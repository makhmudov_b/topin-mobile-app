import React, { useState, useEffect, useRef } from "react";
import AppContainer from "./src/navigation";
import * as Font from "expo-font";
import Context from "./src/components/Context";
import LoadingScreen from "./src/screens/LoadingScreen";

import ApiService from "./src/services/api";
import ErrorBoundary from "./src/components/ErrorBoundary";
import * as Location from "expo-location";
import * as Notifications from "expo-notifications";
import * as Permissions from "expo-permissions";
import {
  AsyncStorage,
  Platform,
  YellowBox,
  Text,
  TextInput,
} from "react-native";
import Constants from "expo-constants";

const apiService = new ApiService();

Notifications.setNotificationHandler({
  handleNotification: async () => ({
    shouldShowAlert: true,
    shouldPlaySound: false,
    shouldSetBadge: false,
  }),
});
export default function App() {
  const [loading, setLoading] = useState(true);
  const [token, setTokenizer] = useState(null);
  const [phone, setUserPhone] = useState(null);
  const [regions, setRegions] = useState(-1);
  const [registered, setRegistered] = useState(false);
  const [currentLang, setLang] = useState(1);
  const [location, setLocation] = useState(null);
  const [categories, setCategories] = useState([]);
  const [user, setUser] = useState(null);
  const [categoryId, setCategoryId] = useState(0);
  const [priceFrom, setPriceFrom] = useState("");
  const [priceTo, setPriceTo] = useState("");
  const [regionId, setRegionId] = useState(0);
  const [chosenTypes, setChosenTypes] = useState([]);
  const [typesChildren, setTypesChildren] = useState([]);
  const [childTypes, setChildTypes] = useState([]);

  /// notification
  const [expoPushToken, setExpoPushToken] = useState("");
  const [notification, setNotification] = useState(false);
  const notificationListener = useRef();
  const responseListener = useRef();

  const setCurrentLang = (value) => {
    if (value != currentLang) {
      storeData("currentLang", value);
      setLang(value);
    }
  };
  const setData = (name, currentValue, value, method) => {
    if (value != currentValue) {
      storeData(name, value);
      method(value);
    }
  };
  const setPhone = (value) => {
    setData("phone", phone, value, setUserPhone);
  };
  const setToken = (value) => {
    storeData("token", value);
    setTokenizer(value);
  };
  Text.defaultProps = {};
  Text.defaultProps.allowFontScaling = false;
  TextInput.defaultProps.allowFontScaling = false;
  YellowBox.ignoreWarnings([
    "Non-serializable values were found in the navigation state",
  ]);

  const registerForPushNotificationsAsync = async (userToken, settedToken) => {
    if (Constants.isDevice) {
      const { status: existingStatus } = await Permissions.getAsync(
        Permissions.NOTIFICATIONS
      );
      let finalStatus = existingStatus;
      if (existingStatus !== "granted") {
        const { status } = await Permissions.askAsync(
          Permissions.NOTIFICATIONS
        );
        finalStatus = status;
      }
      if (finalStatus !== "granted") {
        alert("Failed to get push token for push notification!");
        return;
      }
      const notificationToken = await Notifications.getExpoPushTokenAsync();
      if (userToken) {
        if (notificationToken != settedToken) {
          const body = {
            notification_token: notificationToken.data,
          };
          setExpoPushToken(notificationToken.data);
          apiService.updateForm("/set/token", userToken, body);
        }
      }
      if (Platform.OS === "android") {
        Notifications.setNotificationChannelAsync("default", {
          name: "default",
          importance: Notifications.AndroidImportance.MAX,
          vibrationPattern: [0, 250, 250, 250],
          lightColor: "#FF231F7C",
        });
      }
      setExpoPushToken(notificationToken);
    }
  };
  useEffect(() => {
    Font.loadAsync({
      "Rubik-Light": require("./assets/fonts/Rubik-Light.ttf"),
      "Rubik-Regular": require("./assets/fonts/Rubik-Regular.ttf"),
      "Rubik-Bold": require("./assets/fonts/Rubik-Bold.ttf"),
      "Rubik-Medium": require("./assets/fonts/Rubik-Medium.ttf"),
    }).then(() => {
      getData("currentLang").then((value) => {
        const data = JSON.parse(value);
        if (data) {
          if (currentLang != data) setCurrentLang(data);
        }
      });
      getLocation();
      getRegions();
      getUserData();
    });
    notificationListener.current = Notifications.addNotificationReceivedListener(
      (notification) => {
        setNotification(notification);
      }
    );
    // responseListener.current = Notifications.addNotificationResponseReceivedListener(
    //   (response) => {
    //     console.warn(response);
    //   }
    // );
    return () => {
      Notifications.removeNotificationSubscription(notificationListener);
      Notifications.removeNotificationSubscription(responseListener);
    };
  }, []);
  const getRegions = () => {
    apiService.getResources("/regions").then((value) => {
      setRegions(value.data);
    });
  };
  const getLocation = async () => {
    let { status } = await Location.requestPermissionsAsync();
    const location = await Location.getCurrentPositionAsync({
      enableHighAccuracy: false,
    });
    setLocation(location);
  };
  const getUserData = () => {
    if (!loading) setLoading(true);
    getData("token").then((value) => {
      const getToken = !value ? null : JSON.parse(value);
      setToken(getToken);
      if (getToken) {
        apiService
          .getResources("/user", getToken)
          .then((value) => {
            setUser(value);
            registerForPushNotificationsAsync(
              getToken,
              value.notification_token
            );
            setData("registered", registered, value.registered, setRegistered);
            setData("phone", phone, value.phone, setUserPhone);
            if (loading) setLoading(false);
          })
          .catch((e) => {
            if (loading) setLoading(false);
          });
      } else {
        if (loading) setLoading(false);
      }
    });
  };
  const removeItemValue = async (key) => {
    try {
      await AsyncStorage.removeItem(key);
      return true;
    } catch (exception) {
      return false;
    }
  };
  const logout = () => {
    return apiService.getResources("/logout", token).then((value) => {
      if (value.statusCode === 200) {
        setTokenizer(null);
        setUserPhone(null);
        setUser(null);
        setRegistered(false);
        removeItemValue("token");
        removeItemValue("phone");
        removeItemValue("registered");
        return true;
      }
    });
  };
  const getUser = (getToken) => {
    const sendToken = getToken ? getToken : token;
    return apiService.getResources("/user", sendToken).then((value) => {
      setUser(value);
      setData("registered", registered, true, setRegistered);
      setData("phone", phone, value.phone, setUserPhone);
      return true;
    });
  };
  const getData = async (name) => {
    try {
      const data = await AsyncStorage.getItem(name);
      return data;
    } catch (e) {}
  };
  const storeData = async (name, data) => {
    try {
      await AsyncStorage.setItem(name, JSON.stringify(data));
    } catch (e) {}
  };

  if (loading) {
    return <LoadingScreen />;
  }
  return (
    <ErrorBoundary>
      <Context.Provider
        value={{
          apiService,
          token,
          setToken,
          setPhone,
          phone,
          location,
          getUser,
          setUser,
          getLocation,
          setRegistered,
          registered,
          categories,
          setCategories,
          categoryId,
          setCategoryId,
          regions,
          setRegions,
          currentLang,
          setCurrentLang,
          user,
          logout,
          priceFrom,
          setPriceFrom,
          priceTo,
          setPriceTo,
          regionId,
          setRegionId,
          chosenTypes,
          setChosenTypes,
          typesChildren,
          setTypesChildren,
          childTypes,
          setChildTypes,
        }}
      >
        <AppContainer />
      </Context.Provider>
    </ErrorBoundary>
  );
}
