class ApiService {
  _host = "http://topin-system.indev.uz/api";
  _apiBase = this._host + "";
  _headers = {
    Accept: "application/json",
    "Content-Type": "application/json",
  };

  // Authorization
  verify = (phone) => {
    return fetch(this._apiBase + "/verification", {
      method: "POST",
      headers: this._headers,
      body: JSON.stringify({ phone: phone }),
    }).then((res) => {
      if (res.status != 200) {
        throw new Error("Введены неправильные данные. Повторите заново!");
      }
      return res.status == 200;
    });
  };
  authorize = async ({ code, phone }) => {
    const body = {
      verify_code: code,
      phone: phone,
    };
    const res = await fetch(this._apiBase + "/auth", {
      method: "POST",
      headers: this._headers,
      body: JSON.stringify(body),
    });
    return res.json();
  };
  account = async (token = null, form) => {
    let headers = {};
    if (token) {
      headers = {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
      };
    }
    return fetch(this._apiBase + "/account", {
      method: "PUT",
      headers: headers,
      body: JSON.stringify(form),
    }).then((res) => {
      if (res.status > 201) throw "Login";
      return res.json();
    });
  };

  postEvent = async (url, token = null, body) => {
    let headers = {};
    if (token) {
      headers = {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
      };
    }
    return fetch(this._apiBase + url, {
      method: "POST",
      headers: headers,
      body: JSON.stringify(body),
    }).then((res) => {
      if (res.status > 201) throw "Login";
      return res.json();
    });
  };
  addAdvert = async (token = null, body) => {
    const {
      advert_type,
      type_id,
      category_id,
      region_id,
      name,
      description,
      price,
      phone,
      files,
      lat,
      long,
      address_info,
    } = body;
    const data = new FormData();
    data.append("category_id", category_id);
    data.append("region_id", region_id);
    data.append("name", name);
    data.append("description", description);
    data.append("price", price);
    data.append("phone", phone);
    data.append("lat", lat);
    data.append("long", long);
    data.append("address_info", address_info);

    if (files.length) {
      files.forEach((f, index) => {
        const getType = f.indexOf("png");
        data.append("images[]", {
          uri: f,
          name: index + (getType > 0 ? ".png" : ".jpg"),
          type: getType > 0 ? "image/png" : "image/jpeg",
        });
      });
    }
    type_id.forEach((item) => data.append("type_id[]", item));
    advert_type.forEach((item) => data.append("advert_type[]", item));
    const res = await fetch("http://topin-system.indev.uz/api/add/advert", {
      method: "POST",
      headers: {
        Authorization: `Bearer ${token}`,
      },
      body: data,
    });
    if (res.status !== 200) throw new Error("error");
    return res.status === 200;
  };
  updateAdvert = async (token, body) => {
    const {
      advert_type,
      type_id,
      category_id,
      region_id,
      name,
      description,
      price,
      files,
      lat,
      long,
      address_info,
      id,
    } = body;
    const data = new FormData();
    data.append("category_id", category_id);
    data.append("region_id", region_id);
    data.append("name", name);
    data.append("description", description);
    data.append("price", price);
    data.append("lat", lat);
    data.append("long", long);
    data.append("address_info", address_info);
    data.append("_method", "PUT");
    if (files.length) {
      files.forEach((f, index) => {
        const getType = f.indexOf("png");
        data.append("images[]", {
          uri: f,
          name: index + (getType > 0 ? ".png" : ".jpg"),
          type: getType > 0 ? "image/png" : "image/jpeg",
        });
      });
    }
    type_id.forEach((item) => data.append("type_id[]", item));
    advert_type.forEach((item) => data.append("advert_type[]", item));
    try {
      const res = await fetch(
        "http://topin-system.indev.uz/api/update/advert/" + id,
        {
          method: "POST",
          headers: {
            Authorization: `Bearer ${token}`,
          },
          body: data,
        }
      );
      const value = await res.json();
      return value.statusCode == 200;
    } catch (e) {
      throw Error("error");
    }
  };
  patchEvent = async (url, token = null, body) => {
    let headers = {};
    if (token) {
      headers = {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
      };
    }
    const res = await fetch(this._apiBase + url, {
      method: "PATCH",
      headers: headers,
      body: JSON.stringify(body),
    });
    if (res.status > 201) throw "Login";
    return res.json();
  };

  updateForm = async (url, token = null, form) => {
    let headers = {};
    if (token) {
      headers = {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
      };
    }
    return fetch(this._apiBase + url, {
      method: "PUT",
      headers: headers,
      body: JSON.stringify(form),
    }).then((res) => {
      if (res.status > 201) throw "Login";
      return res.json();
    });
  };
  searchAdvert = async (body) => {
    const res = await fetch(
      `${this._apiBase}/search?` + new URLSearchParams(body),
      { headers: this._headers }
    );
    return await res.json();
  };
  getResources = async (url, token = null) => {
    let headers = [];

    if (token) {
      headers = {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
      };
    }

    const res = await fetch(this._apiBase + url, { headers });
    if (res.status > 201) throw "Login";
    return await res.json();
  };
}

export default ApiService;
