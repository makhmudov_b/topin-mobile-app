import React from "react";
import { View } from "react-native";
import Arrow from "../../assets/arrow-left.svg";
import TouchableScale from "react-native-touchable-scale";
import { useNavigation } from "@react-navigation/native";

const HeaderLeft = () => {
  const navigation = useNavigation();
  return (
    <TouchableScale onPress={() => navigation.goBack()}>
      <View style={{ alignItems: "center", marginLeft: 10 }}>
        <Arrow />
      </View>
    </TouchableScale>
  );
};

export default HeaderLeft;
