import React from "react";
import { View, Text, StatusBar } from "react-native";
import global from "../../resources/global";
import Logo from "../../assets/logo.svg";

const Header = () => {
  return (
    <>
      <StatusBar
        backgroundColor={global.colors.main}
        barStyle={`light-content`}
      />
      <View
        style={{
          alignItems: "center",
          justifyContent: "center",
        }}
      >
        <Logo />
      </View>
    </>
  );
};

export default Header;
