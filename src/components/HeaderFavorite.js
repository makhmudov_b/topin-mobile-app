import React, { useContext } from "react";
import { View, Text } from "react-native";
import Favorite from "../../assets/favorite.svg";
import TouchableScale from "react-native-touchable-scale";
import styled from "styled-components";
import global from "../../resources/global";
import language from "../constants/language";
import Context from "./Context";

const HeaderFavorite = ({ active, method }) => {
  const { currentLang } = useContext(Context);
  return (
    <TouchableScale onPress={() => method()}>
      <View style={{ alignItems: "center", marginRight: 10 }}>
        <FavoriteButton active={active}>
          <FavoriteText>{language[currentLang].toFavorite}</FavoriteText>
          <Favorite color={active ? global.colors.orange : "#999999"} />
        </FavoriteButton>
      </View>
    </TouchableScale>
  );
};
const FavoriteButton = styled.View`
  background: ${({ active }) => (active ? global.colors.orange : "#999999")};
  height: 40px;
  padding: 0 8px;
  flex-direction: row;
  align-items: center;
  justify-content: space-around;
  border-radius: 10px;
`;
const FavoriteText = styled.Text`
  font-size: 14px;
  color: white;
  padding-right: 10px;
  font-family: ${global.fonts.regular};
`;
export default HeaderFavorite;
