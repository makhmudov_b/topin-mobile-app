import React, { useContext } from "react";
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  TouchableNativeFeedback,
  TouchableHighlight,
  Touchable,
} from "react-native";
import TouchableScale from "react-native-touchable-scale";
import styled from "styled-components";
import AdLeft from "../../assets/advert.svg";
import global from "../../resources/global";
import language from "../constants/language";
import Context from "../components/Context";

const generateButton = (title, color = 0, left = 0, right = 0, method) => {
  return (
    <TouchableScale
      style={{ flex: 1 }}
      activeScale={0.97}
      onPress={() => method()}
    >
      <ChoiceButton
        left={left}
        right={right}
        color={!color ? "#F5F5F5" : global.colors.orange}
      >
        <ChoiceButtonText color={!color ? "#333333" : "white"}>
          {title}
        </ChoiceButtonText>
      </ChoiceButton>
    </TouchableScale>
  );
};
const AdvertHorizontalFavorite = ({ favorite, remove, navigation }) => {
  const { apiService, token, currentLang } = useContext(Context);
  const { advert } = favorite;
  const { name, region, price } = advert;

  return (
    <AdvertWrap>
      <Advert>
        <AdvertLeft>
          <View style={{ backgroundColor: "#AEAEAE", flex: 1 }}>
            <Image
              source={{ uri: advert.image[0] }}
              style={{ width: "100%", height: "100%" }}
              resizeMode="cover"
            />
          </View>
        </AdvertLeft>
        <AdverRight>
          <AdvertText>{name}</AdvertText>
          <AdvertBottom>
            <Region>
              {currentLang == 1 && region.name_ru}
              {currentLang == 2 && region.name_uz}
              {currentLang == 3 && region.name_kr}
            </Region>
            <Price>{price} UZS</Price>
          </AdvertBottom>
        </AdverRight>
      </Advert>
      <ChoiceWrap>
        {generateButton(language[currentLang].see, 0, 1, 0, () =>
          navigation.push("ShowAdvert", { advertId: advert.id })
        )}
        {generateButton(language[currentLang].write, 0, 0, 0, () =>
          navigation.navigate("MessageShow", { advertId: advert.id })
        )}
        {generateButton(language[currentLang].remove, 1, 0, 1, () =>
          remove(advert.id)
        )}
      </ChoiceWrap>
    </AdvertWrap>
  );
};
const AdvertWrap = styled.View`
  margin-bottom: 15px;
  width: 100%;
`;
const ChoiceButtonText = styled.Text`
  font-size: 10px;
  color: ${({ color }) => color};
  font-family: ${global.fonts.regular};
`;
const ChoiceButton = styled.View`
  /* width: 100%; */
  flex: 1;
  background: ${({ color }) => color};
  height: 30px;
  align-items: center;
  justify-content: center;
  border-left-width: ${({ left, right }) => (!left && !right ? "1px" : 0)};
  border-left-color: rgba(174, 174, 174, 0.5);
  /* border-bottom-left-radius: ${({ left }) => (left ? "10px" : 0)};
  border-bottom-right-radius: ${({ right }) => (right ? "10px" : 0)}; */
`;
const ChoiceWrap = styled.View`
  width: 100%;
  background: #f5f5f5;
  /* border-bottom-left-radius: 10px;
  border-bottom-right-radius: 10px; */
  flex-direction: row;
  border-bottom-left-radius: 10px;
  border-bottom-right-radius: 10px;
  overflow: hidden;
`;
const Advert = styled.View`
  flex: 1;
  /* margin: 10px 10px 0px; */
  height: 70px;
  /* border-radius: 10px; */
  overflow: hidden;
  background-color: white;
  flex-direction: row;
  justify-content: space-between;
  border-top-left-radius: 10px;
`;
const AdvertLeft = styled.View`
  flex-basis: 100px;
  background-color: whitesmoke;
`;
const AdverRight = styled.View`
  padding: 10px;
  flex: 1;
  border-top-right-radius: 10px;
  /* border-bottom-right-radius: 10px; */
  border: 1px solid #f1f1f1;
`;
const AdvertText = styled.Text`
  color: ${global.colors.secondary};
  font-size: 12px;
  line-height: 18px;
  font-family: ${global.fonts.regular};
`;
const AdvertBottom = styled.View`
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
`;
const Region = styled.Text`
  color: #999999;
  font-size: 10px;
  font-family: ${global.fonts.regular};
`;
const Price = styled.Text`
  color: ${global.colors.secondary};
  font-size: 12px;
  font-family: ${global.fonts.medium};
`;
export default AdvertHorizontalFavorite;
