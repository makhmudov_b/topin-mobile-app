import React from "react";
import { View } from "react-native";
import Close from "../../assets/header-close.svg";
import TouchableScale from "react-native-touchable-scale";
import { useNavigation } from "@react-navigation/native";

const HeaderClose = () => {
  const navigation = useNavigation();
  return (
    <TouchableScale onPress={() => navigation.goBack()}>
      <View style={{ alignItems: "center", marginRight: 10 }}>
        <Close />
      </View>
    </TouchableScale>
  );
};

export default HeaderClose;
