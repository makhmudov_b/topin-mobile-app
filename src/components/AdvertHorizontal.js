import React, { useContext } from "react";
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  TouchableNativeFeedback,
  TouchableHighlight,
  Touchable,
} from "react-native";
import TouchableScale from "react-native-touchable-scale";
import styled from "styled-components";
import AdLeft from "../../assets/advert.svg";
import AdRight from "../../assets/advert-right.svg";
import AdTop from "../../assets/advert-top.svg";
import global from "../../resources/global";
import { useNavigation } from "@react-navigation/native";
import Context from "../components/Context";

const AdvertHorizontal = ({ advert }) => {
  const navigation = useNavigation();
  const { currentLang } = useContext(Context);
  return (
    <TouchableScale
      style={{ width: "100%", marginTop: 10 }}
      activeScale={0.97}
      onPress={() => navigation.push("ShowAdvert", { advertId: advert.id })}
    >
      <Advert>
        <AdvertLeft>
          <Image
            source={{ uri: advert.image[0] }}
            style={{ width: "100%", height: "100%" }}
            resizeMode="cover"
          />
          {advert.is_premium && (
            <AbsoluteLeft>
              <AdTop />
            </AbsoluteLeft>
          )}
          <AbsoluteRight>
            <AdRight />
          </AbsoluteRight>
        </AdvertLeft>
        <AdverRight>
          <AdvertText>{advert.name}</AdvertText>
          <AdvertBottom>
            <Region>
              {currentLang == 1 && advert.region.name_ru}
              {currentLang == 2 && advert.region.name_uz}
              {currentLang == 3 && advert.region.name_kr}
            </Region>
            <Price>{advert.price} UZS</Price>
          </AdvertBottom>
        </AdverRight>
      </Advert>
    </TouchableScale>
  );
};

const AbsoluteLeft = styled.View`
  width: 100%;
  height: 100%;
  position: absolute;
  top: 0;
  left: 0;
  z-index: 20;
  /* right: 0; */
  /* bottom: 0; */
  align-items: flex-start;
`;
const AbsoluteRight = styled.View`
  width: 100%;
  height: 100%;
  position: absolute;
  top: 0;
  right: 0;
  /* right: 0; */
  /* bottom: 0; */
  align-items: flex-start;
`;
const Advert = styled.View`
  flex: 1;
  margin: 0 10px 0;
  height: 70px;
  border-radius: 10px;
  shadow-color: #000;
  shadow-opacity: 0.12;
  elevation: 3;
  overflow: hidden;
  background-color: white;
  flex-direction: row;
  justify-content: space-between;
`;
const AdvertLeft = styled.View`
  flex-basis: 100px;
  position: relative;
  background-color: whitesmoke;
`;
const AdverRight = styled.View`
  padding: 8px;
  flex: 1;
  border-top-right-radius: 10px;
  border-bottom-right-radius: 10px;
  border: 1px solid #f1f1f1;
`;
const AdvertText = styled.Text`
  color: ${global.colors.secondary};
  font-size: 12px;
  line-height: 18px;
  font-family: ${global.fonts.regular};
  height: 40px;
  overflow: hidden;
`;
const AdvertBottom = styled.View`
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
`;
const Region = styled.Text`
  color: #999999;
  font-size: 10px;
  font-family: ${global.fonts.regular};
`;
const Price = styled.Text`
  color: ${global.colors.secondary};
  font-size: 12px;
  font-family: ${global.fonts.medium};
`;
export default AdvertHorizontal;
