import React from "react";
import { View, Text } from "react-native";
import ContentLoader, { Rect } from "react-content-loader/native";
import global from "../../resources/global";

export const CategoryLoadWrap = (props) => {
  const fourItems = global.strings.width / 4;
  const categorySize = fourItems > 80 ? 80 : fourItems;
  return (
    <ContentLoader
      speed={3}
      width={categorySize}
      height={categorySize}
      viewBox={`0 0 ${categorySize + 10} ${categorySize + 20}`}
      backgroundColor="#5a5a5a"
      foregroundColor="#ff7800"
    >
      <Rect
        x="5"
        y="0"
        rx="10"
        ry="10"
        width={categorySize}
        height={categorySize}
      />
      <Rect x="5" y="85" rx="5" ry="5" width={categorySize} height="10" />
    </ContentLoader>
  );
};
export const SliderLoader = () => {
  return (
    <ContentLoader
      speed={3}
      width={global.strings.width}
      height={170}
      viewBox={`0 0 ${global.strings.width} 170`}
      backgroundColor="#5a5a5a"
      foregroundColor="#ff7800"
    >
      <Rect
        x="0"
        y="0"
        rx="0"
        ry="0"
        width={global.strings.width}
        height="170"
      />
    </ContentLoader>
  );
};
export const CategoryLoader = () => (
  <>
    <CategoryLoadWrap />
    <CategoryLoadWrap />
    <CategoryLoadWrap />
    <CategoryLoadWrap />
    <CategoryLoadWrap />
  </>
);

const ChoiceLoadWrap = () => {
  const innerSize = global.strings.width - 20;
  return (
    <ContentLoader
      speed={3}
      width={innerSize}
      height={40}
      viewBox={`0 0 ${innerSize} 40`}
      // backgroundColor="#5a5a5a"
      foregroundColor="#ff7800"
      style={{ marginBottom: 15 }}
    >
      <Rect x="0" y="0" rx="10" ry="10" width={innerSize} height="40" />
    </ContentLoader>
  );
};
export const ChoiceLoader = () => (
  <View>
    <ChoiceLoadWrap />
    <ChoiceLoadWrap />
    <ChoiceLoadWrap />
    <ChoiceLoadWrap />
  </View>
);
