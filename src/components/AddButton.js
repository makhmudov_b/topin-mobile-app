import React, { useContext } from "react";
import { View, Text } from "react-native";
import Plus from "../../assets/plus.svg";
import TouchableScale from "react-native-touchable-scale";
import { useNavigation } from "@react-navigation/native";
import styled from "styled-components";
import global from "../../resources/global";
import Context from "./Context";
import language from "../constants/language";

const AddButton = () => {
  const { registered, currentLang } = useContext(Context);
  const navigation = useNavigation();
  return (
    <Centered>
      <TouchableScale
        onPress={() =>
          registered
            ? navigation.navigate("AddAdvert")
            : navigation.jumpTo("Profile")
        }
      >
        <Button>
          <Plus />
          <ButtonText>{language[currentLang].addAdvert}</ButtonText>
        </Button>
      </TouchableScale>
    </Centered>
  );
};
const Centered = styled.View`
  position: absolute;
  bottom: 10px;
  width: 100%;
  justify-content: center;
  align-items: center;
`;
const ButtonText = styled.Text`
  font-size: 14px;
  font-family: ${global.fonts.medium};
  color: white;
  padding-left: 5px;
`;
const Button = styled.View`
  height: 40px;
  padding: 0 10px;
  flex-direction: row;
  align-items: center;
  justify-content: center;
  border-radius: 10px;
  background: ${global.colors.orange};
`;
export default AddButton;
