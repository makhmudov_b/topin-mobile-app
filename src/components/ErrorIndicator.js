import React from "react";
import { View, Text } from "react-native";
import Logo from "../../assets/logo.svg";

const ErrorIndicator = () => {
  return (
    <View
      style={{
        flex: 1,
        alignItems: "center",
        justifyContent: "center",
        backgroundColor: "#5A5A5A",
        zIndex: 99,
      }}
    >
      <View style={{ alignItems: "center", justifyContent: "center" }}>
        <Logo />
        <Text style={{ color: "white" }}>Ошибка! Что-то пошло не так.</Text>
      </View>
    </View>
  );
};
export default ErrorIndicator;
