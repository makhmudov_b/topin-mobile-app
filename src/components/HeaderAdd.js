import React from "react";
import { View } from "react-native";
import Add from "../../assets/add-button.svg";
import TouchableScale from "react-native-touchable-scale";
import { useNavigation } from "@react-navigation/native";

const HeaderLeft = () => {
  const navigation = useNavigation();
  return (
    <TouchableScale onPress={() => navigation.navigate("AddAdvert")}>
      <View style={{ alignItems: "center", marginRight: 10 }}>
        <Add />
      </View>
    </TouchableScale>
  );
};

export default HeaderLeft;
