import React from "react";
import {
  View,
  Text,
  Modal,
  TouchableWithoutFeedback,
  StyleSheet,
} from "react-native";
import global from "../../resources/global";
import Success from "../../assets/success-icon.svg";

const SuccessModal = ({ showModal, setShowModal }) => {
  return (
    <Modal animationType="fade" transparent={true} visible={showModal}>
      <TouchableWithoutFeedback
        onPress={() => setShowModal(false)}
        style={{
          position: "absolute",
          top: 0,
          left: 0,
          right: 0,
          bottom: 0,
        }}
      >
        <View style={{ flex: 1, backgroundColor: "rgba(0,0,0,.6)" }} />
      </TouchableWithoutFeedback>
      <View
        style={{
          flexDirection: "row",
          position: "absolute",
          marginTop: 60,
          alignSelf: "center",
        }}
      >
        <View
          style={[
            styles.authorizationBlock,
            {
              borderRadius: 20,
              marginHorizontal: 20,
              backgroundColor: "white",
            },
          ]}
        >
          <Text
            style={{
              fontSize: 24,
              textAlign: "center",
              marginBottom: 15,
              fontFamily: global.fonts.medium,
              color: "#444444",
            }}
          >
            Заявка принята
          </Text>
          <Text
            style={{
              fontSize: 14,
              fontFamily: global.fonts.medium,
              textAlign: "center",
              color: "#444444",
            }}
          >
            Василий Петрович
          </Text>
          <Text style={{ fontSize: 9, textAlign: "center", color: "#999999" }}>
            Поставшик услуг может вам позвонить по указанному номеру для
            подтверждения заявки
          </Text>
          <View
            style={{
              alignItems: "center",
              justifyContent: "center",
              marginTop: 20,
            }}
          >
            <Success />
            <Text
              style={{
                marginTop: 20,
                fontSize: 14,
                textAlign: "center",
                color: "#444444",
              }}
            >
              Мин. стоимость:
              <Text style={{ fontWeight: "bold" }}>25 000 UZS</Text>
            </Text>
          </View>
        </View>
      </View>
    </Modal>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#F6F6F6",
    justifyContent: "space-between",
  },
  authorization: {
    backgroundColor: "white",
    width: `90%`,
    borderRadius: 20,
    shadowColor: "rgba(0,0,0,.15)",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  block: {
    flex: 1,
    height: 40,
    alignItems: "center",
    justifyContent: "center",
  },
  leftBlock: {
    borderTopLeftRadius: 60,
    borderBottomLeftRadius: 60,
  },
  rightBlock: {
    borderTopRightRadius: 60,
    borderBottomRightRadius: 60,
  },
  activeBlock: {
    backgroundColor: global.colors.mainColor,
  },
  inactiveBlock: {
    backgroundColor: "white",
    borderWidth: 1,
    borderColor: "#E9E9E9",
  },
  activeText: {
    color: "white",
  },
  inactiveText: {
    color: "#999999",
  },
  authorizationBlock: {
    padding: 20,
    justifyContent: "center",
  },
  authorizationInput: {
    fontSize: 18,
    paddingHorizontal: 15,
    height: 50,
    fontFamily: global.fonts.regular,
    borderWidth: 1,
    borderRadius: 50,
    backgroundColor: "white",
    borderColor: "#E9E9E9",
    marginBottom: 10,
    lineHeight: 50,
  },
  button: {
    height: 50,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 50,
    marginTop: 20,
    marginBottom: 10,
    width: "90%",
  },
  icon: {
    position: "absolute",
    height: 24,
    width: 24,
    justifyContent: "center",
    alignItems: "center",
    right: 20,
    top: 13,
  },
});

export default SuccessModal;
