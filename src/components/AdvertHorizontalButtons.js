import React, { useContext, useEffect } from "react";
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  TouchableNativeFeedback,
  TouchableHighlight,
  Touchable,
} from "react-native";
import TouchableScale from "react-native-touchable-scale";
import styled from "styled-components";
import AdLeft from "../../assets/advert.svg";
import global from "../../resources/global";
import language from "../constants/language";
import Context from "./Context";

const generateButton = (title, color, left, right, method) => {
  return (
    <TouchableScale
      onPress={() => method()}
      style={{ flex: 1 }}
      activeScale={0.97}
    >
      <ChoiceButton
        left={left}
        right={right}
        color={!color ? "#F5F5F5" : global.colors.orange}
      >
        <ChoiceButtonText color={!color ? "#333333" : "white"}>
          {title}
        </ChoiceButtonText>
      </ChoiceButton>
    </TouchableScale>
  );
};
const AdvertHorizontalButtons = ({ advert, navigation, switcher }) => {
  const { currentLang } = useContext(Context);
  const { name, region, price } = advert;
  return (
    <AdvertWrap>
      <Advert>
        <AdvertLeft>
          <View style={{ backgroundColor: "#AEAEAE", flex: 1 }}>
            <Image
              source={{ uri: advert.image[0] }}
              style={{ width: "100%", height: "100%" }}
            />
          </View>
        </AdvertLeft>
        <AdverRight>
          <AdvertText>{name}</AdvertText>
          <Region>
            {language[currentLang].promoCount} {advert.promoted_count}
          </Region>
          <AdvertBottom>
            <Region>
              {currentLang == 1 && region.name_ru}
              {currentLang == 2 && region.name_uz}
              {currentLang == 3 && region.name_kr}
            </Region>
            <Price>{price} UZS</Price>
          </AdvertBottom>
        </AdverRight>
      </Advert>
      <ChoiceWrap>
        {generateButton(language[currentLang].see, 0, 1, 0, () =>
          navigation.navigate("ShowAdvert", { advertId: advert.id })
        )}
        {generateButton(language[currentLang].edit, 0, 0, 0, () =>
          navigation.navigate("EditAdvert", { advertId: advert.id })
        )}
        {generateButton(
          advert.status != "DISABLED"
            ? language[currentLang].off
            : language[currentLang].enable,
          0,
          0,
          0,
          () => switcher(advert.id)
        )}
        {generateButton(
          advert.promoted_count == 0
            ? language[currentLang].promote
            : `${language[currentLang].promoted}`,
          1,
          0,
          1,
          () => navigation.navigate("Promote", { advertId: advert.id })
        )}
      </ChoiceWrap>
    </AdvertWrap>
  );
};
const AdvertWrap = styled.View`
  margin-bottom: 15px;
  width: 100%;
`;
const ChoiceButtonText = styled.Text`
  font-size: 10px;
  color: ${({ color }) => color};
  font-family: ${global.fonts.regular};
`;
const ChoiceButton = styled.View`
  /* width: 100%; */
  flex: 1;
  background: ${({ color }) => color};
  height: 30px;
  align-items: center;
  justify-content: center;
  border-left-width: ${({ left, right }) => (!left && !right ? "1px" : 0)};
  border-left-color: rgba(174, 174, 174, 0.5);
  /* border-bottom-left-radius: ${({ left }) => (left ? "10px" : 0)};
  border-bottom-right-radius: ${({ right }) => (right ? "10px" : 0)}; */
`;
const ChoiceWrap = styled.View`
  width: 100%;
  background: #f5f5f5;
  /* border-bottom-left-radius: 10px;
  border-bottom-right-radius: 10px; */
  flex-direction: row;
  border-bottom-left-radius: 10px;
  border-bottom-right-radius: 10px;
  overflow: hidden;
  shadow-color: #000;
  shadow-opacity: 0.12;
  elevation: 3;
`;
const Advert = styled.View`
  flex: 1;
  /* margin: 10px 10px 0px; */
  height: 70px;
  /* border-radius: 10px; */
  overflow: hidden;
  background-color: white;
  flex-direction: row;
  justify-content: space-between;
  border-top-left-radius: 10px;
`;
const AdvertLeft = styled.View`
  flex-basis: 100px;
  background-color: whitesmoke;
`;
const AdverRight = styled.View`
  padding: 10px;
  flex: 1;
  border-top-right-radius: 10px;
  /* border-bottom-right-radius: 10px; */
  border: 1px solid #f1f1f1;
`;
const AdvertText = styled.Text`
  color: ${global.colors.secondary};
  font-size: 12px;
  line-height: 18px;
  font-family: ${global.fonts.regular};
`;
const AdvertBottom = styled.View`
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
`;
const Region = styled.Text`
  color: #999999;
  font-size: 10px;
  font-family: ${global.fonts.regular};
`;
const Price = styled.Text`
  color: ${global.colors.secondary};
  font-size: 12px;
  font-family: ${global.fonts.medium};
`;
export default AdvertHorizontalButtons;
