import React, { useEffect, useContext } from "react";
import { View, Text } from "react-native";
import styled from "styled-components";

const TextChoice = ({ title, id }) => {
  const [from, setFrom] = useState("");
  const [to, setTo] = useState("");
  const changeValues = (value) => {
    const getOldValue = chosenTypes.filter((chosen) => chosen.id != id);
    const newValue = [
      id,
      [
        from.length ? parseInt(from) : 0,
        to.length ? parseInt(this.to) : 999999999,
      ],
    ];
  };
  return (
    <InputGroup>
      <InputTitle>{title} </InputTitle>
      <Between>
        <InputBlock
          keyboardType={`numeric`}
          onChangeText={(text) => setFrom(text)}
          placeholder={language[currentLang].from}
          value={from}
        />
        <InputBlock
          keyboardType={`numeric`}
          left={1}
          placeholder={language[currentLang].to}
          value={to}
          onChangeText={(text) => setTo(text)}
        />
      </Between>
    </InputGroup>
  );
};
const Between = styled.View`
  flex-direction: row;
  align-items: center;
  width: 100%;
  justify-content: space-between;
  margin-top: ${({ margin }) => (margin ? margin + "px" : 0)};
`;
const InputBlock = styled.TextInput`
  margin-top: 5px;
  background: white;
  shadow-color: #000;
  shadow-opacity: 0.12;
  shadow-radius: 2px;
  elevation: 3;
  flex: 1;
  border-width: 1px;
  border-color: #efefef;
  border-left-width: ${({ left }) => (left ? "1px" : 0)};
  border-left-color: ${({ left }) => (left ? "#EFEFEF" : "white")};
  border-top-left-radius: ${({ left }) => (!left ? 10 + "px" : 0)};
  border-bottom-left-radius: ${({ left }) => (!left ? 10 + "px" : 0)};
  border-top-right-radius: ${({ left }) => (left ? 10 + "px" : 0)};
  border-bottom-right-radius: ${({ left }) => (left ? 10 + "px" : 0)};
  height: 40px;
  /* border-radius:10px; */
  font-family: ${global.fonts.regular};
  padding: 10px 15px;
`;
const InputTitle = styled.Text`
  color: #999999;
  font-family: ${global.fonts.regular};
  font-size: 14px;
`;
const InputGroup = styled.View`
  width: 100%;
  margin-bottom: 10px;
`;
export default TextChoice;
