import React, { useState, useEffect, useContext } from "react";
import {
  View,
  ScrollView,
  TextInput,
  Text,
  Image,
  TouchableOpacity,
  KeyboardAvoidingView,
} from "react-native";
import styled from "styled-components";
import global from "../../resources/global";
import Profile from "../../assets/profile.svg";
import TouchableScale from "react-native-touchable-scale";
import Phone from "../../assets/phone-auth.svg";
import language from "../constants/language";
import Context from "../components/Context";
import BlackLoadingScreen from "./BlackLoadingScreen";

const SendMessageScreen = ({ navigation }) => {
  const { apiService, currentLang, setPhone } = useContext(Context);
  const [phone, setPhones] = useState("");
  const [loading, setLoading] = useState(false);
  const confirmPhone = () => {
    if (phone.length === 9) {
      setLoading(true);
      apiService
        .verify(phone)
        .then((value) => {
          if (value) {
            setPhone(phone);
            navigation.navigate("Confirm");
          }
          setLoading(false);
        })
        .catch((e) => setLoading(false));
    }
  };
  if (loading) return <BlackLoadingScreen />;
  return (
    <KeyboardAvoidingView
      style={{
        backgroundColor: "#999",
        flex: 1,
      }}
      // behavior={"padding"}
    >
      <Wrap>
        <Phone />
        <View height={20} />
        <Title>{language[currentLang].login}</Title>
        <InputWrap>
          <InputLeft>
            <Profile color="#999999" />
          </InputLeft>
          <LoginInput
            placeholder={language[currentLang].phone}
            placeholderTextColor="#999"
            keyboardType={`phone-pad`}
            returnKeyType={"done"}
            value={phone}
            onChangeText={(text) => setPhones(text)}
            maxLength={9}
          />
        </InputWrap>
        <InfoText>
          {language[currentLang].agreement} {"\n"}
          <OrangeText>{language[currentLang].agree}</OrangeText>
        </InfoText>
        <TouchableScale
          activeScale={0.96}
          onPress={() => confirmPhone()}
          style={{ width: "100%" }}
        >
          <LoginButton>
            <LoginText>{language[currentLang].getCode}</LoginText>
          </LoginButton>
        </TouchableScale>
        {Platform.OS == "ios" ? <View height={100} /> : null}
      </Wrap>
    </KeyboardAvoidingView>
  );
};
const LoginText = styled.Text`
  color: white;
  font-size: 14px;
  text-align: center;
  font-family: ${global.fonts.regular};
`;
const LoginButton = styled.View`
  width: 100%;
  background: #2dba49;
  border-radius: 10px;
  height: 40px;
  align-items: center;
  justify-content: center;
`;
const Wrap = styled.View`
  margin: 5px 0 5px;
  background: ${global.colors.main};
  padding: 10px;
  flex: 1;
  justify-content: center;
  align-items: center;
`;
const Title = styled.Text`
  font-family: ${global.fonts.medium};
  font-size: 24px;
  line-height: 36px;
  color: white;
  padding-bottom: 20px;
`;
const InputWrap = styled.View`
  background: white;
  border-radius: 10px;
  height: 40px;
  width: 100%;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  margin-bottom: 10px;
`;
const InputLeft = styled.View`
  height: 40px;
  width: 55px;
  border-right-width: 1px;
  border-right-color: #efefef;
  align-items: center;
  justify-content: center;
  /* border: solid #efefef; */
`;
const InfoText = styled.Text`
  color: #aeaeae;
  font-size: 14px;
  font-family: ${global.fonts.regular};
  text-align: center;
  margin: 20px 0;
`;
const OrangeText = styled.Text`
  color: ${global.colors.orange};
`;
const LoginInput = styled.TextInput`
  flex: 1;
  height: 40px;
  color: #333333;
  font-size: 14px;
  font-family: ${global.fonts.regular};
  padding: 0 10px;
`;
const PaddingHorizontal = styled.View`
  padding: 0 5px;
`;
const Between = styled.View`
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
`;
export default SendMessageScreen;
