import React, { useState, useEffect, useContext } from "react";
import {
  View,
  ScrollView,
  TextInput,
  Text,
  Image,
  TouchableOpacity,
  StatusBar,
} from "react-native";
import styled from "styled-components";
import global from "../../resources/global";
import Move from "../../assets/move.svg";
import User from "../../assets/user.svg";
import Message from "../../assets/message.svg";
import Phone from "../../assets/phone.svg";

import TouchableScale from "react-native-touchable-scale";
import { Linking } from "expo";
import Context from "../components/Context";
import LoadingScreen from "./LoadingScreen";
import language from "../constants/language";
import Swiper from "react-native-swiper";
import SliderLeft from "../../assets/slider-left.svg";
import SliderRight from "../../assets/slider-right.svg";
import MapMarker from "../../assets/marker.svg";
import MapView, { Marker } from "react-native-maps";
import HeaderFavorite from "../components/HeaderFavorite";

const generateInfo = (left, right) => {
  return (
    <Between margin={5}>
      <Information color={1}>{left}</Information>
      <Information>{right}</Information>
    </Between>
  );
};
const generateTitle = (title) => {
  return (
    <Between margin={5}>
      <Title>{title}</Title>
    </Between>
  );
};
const generateDescription = (title) => {
  return (
    <Between margin={5}>
      <Description>{title}</Description>
    </Between>
  );
};
const generateButton = (type, icon, title, method) => {
  return (
    <Flexer>
      <TouchableScale onPress={() => method()}>
        <BottomButton type={type}>
          <Icon left={0} right={5}>
            {icon}
          </Icon>
          <ButtonText>{title}</ButtonText>
        </BottomButton>
      </TouchableScale>
    </Flexer>
  );
};
const ShowAdvertScreen = ({ navigation, route }) => {
  const {
    apiService,
    currentLang,
    categories,
    user,
    token,
    registered,
  } = useContext(Context);
  const { advertId } = route.params;
  const [advert, setAdvert] = useState(null);
  const [loading, setLoading] = useState(true);
  const [favorite, setFavorite] = useState(false);
  const switchFavorite = () => {
    const body = {
      advert_id: advert.id,
    };
    if (favorite) {
      apiService.patchEvent("/favorite/remove", token, body).then((value) => {
        if (value.statusCode == 200) {
          setFavorite(false);
        }
      });
    } else {
      apiService.patchEvent("/favorite/set", token, body).then((value) => {
        if (value.statusCode == 200) {
          setFavorite(true);
        }
      });
    }
  };
  if (advert) {
    if (token && registered && user.id != advert.user_id) {
      navigation.setOptions({
        headerRight: () => (
          <HeaderFavorite method={switchFavorite} active={favorite} />
        ),
      });
    }
  }

  useEffect(() => {
    getAdvert(advertId);
  }, []);
  const findCategory = (id) => {
    const found = categories.find((data) => data.id == id);
    if (found) {
      if (currentLang == 1) return found.name_ru;
      if (currentLang == 2) return found.name_uz;
      if (currentLang == 3) return found.name_kr;
    }
    return "";
  };
  function getAdvert(id) {
    if (!loading) setLoading(true);
    apiService.getResources("/advert/" + id, token).then((value) => {
      setAdvert(value);
      setFavorite(value.is_favorite);
      if (loading) setLoading(false);
    });
  }
  if (loading) {
    return <LoadingScreen />;
  }
  return (
    <>
      <StatusBar backgroundColor={`white`} barStyle={`dark-content`} />
      <ScrollView
        contentContainerStyle={{
          backgroundColor: "#f5f5f5",
        }}
      >
        <Swiper
          showsButtons={advert.image.length > 1}
          nextButton={<SliderRight />}
          prevButton={<SliderLeft />}
          activeDotColor={"#FFFFFF"}
          dotColor={"#80807F"}
          buttonColor={"#FFFFFF"}
          height={170}
          width={global.strings.width}
        >
          {advert.image.map((photo, key) => (
            <View key={key}>
              <Image
                resizeMode={"cover"}
                style={{ width: "100%", height: 170 }}
                source={{ uri: photo }}
              />
            </View>
          ))}
        </Swiper>
        <Wrap>
          {generateTitle(advert.name)}
          {generateInfo(
            language[currentLang].price + ":",
            advert.price + " UZS"
          )}
        </Wrap>
        <Wrap>
          {generateTitle(language[currentLang].addDetails + ":")}
          {generateInfo(language[currentLang].id, "№ " + advert.id)}
          {generateInfo(
            language[currentLang].addedTime + ":",
            advert.created_at.slice(0, 10)
          )}
          {generateInfo(
            language[currentLang].addRazdel + ":",
            findCategory(advert.category_id)
          )}
          {advert.advert_type.map((info, key) => {
            return (
              <React.Fragment key={key}>
                {generateInfo(
                  currentLang == 1
                    ? info.name_ru
                    : currentLang == 2
                    ? info.name_uz
                    : info.name_kr,
                  info.pivot.value
                )}
              </React.Fragment>
            );
          })}
        </Wrap>
        <Wrap>
          {generateTitle(language[currentLang].addDescr)}
          {generateDescription(advert.description)}
        </Wrap>
        {advert.lat != null && (
          <Wrap>
            {generateTitle(language[currentLang].address)}
            {generateDescription(advert.address_info)}

            <MapView
              initialRegion={{
                latitude: Number(advert.lat),
                longitude: Number(advert.long),
                latitudeDelta: 0.04,
                longitudeDelta: 0.05,
              }}
              zoomControlEnabled={false}
              rotateEnabled={false}
              scrollEnabled={false}
              style={{
                width: "100%",
                height: 170,
                borderRadius: 10,
              }}
            >
              <Marker
                coordinate={{
                  latitude: Number(advert.lat),
                  longitude: Number(advert.long),
                }}
                style={{ width: 40, height: 40 }}
              >
                <MapMarker />
              </Marker>
            </MapView>
          </Wrap>
        )}
        <Wrap>
          <Authorization>
            <Row>
              <User />
              <View width={15} />
              <Title>{advert.user.name}</Title>
            </Row>
            <View>
              <TouchableScale
                onPress={() =>
                  navigation.navigate("User", { user: advert.user })
                }
              >
                <MoveButton>
                  <Move width={24} height={24} />
                </MoveButton>
              </TouchableScale>
            </View>
          </Authorization>
          {generateInfo(
            language[currentLang].inSystem + ":",
            advert.user.created_at.slice(0, 10)
          )}
          {generateInfo(
            language[currentLang].advertsCount + ":",
            advert.user.advert_count
          )}
          {generateInfo(
            language[currentLang].accType + ":",
            !parseInt(advert.user.is_business)
              ? language[currentLang].simple
              : language[currentLang].bussinessAcc
          )}
        </Wrap>
      </ScrollView>
      <Wrap>
        <Row>
          {token &&
            registered &&
            user.id != advert.user_id &&
            generateButton(
              1,
              <Message />,
              language[currentLang].writeMess,
              () => navigation.navigate("MessageShow", { advertId: advert.id })
            )}
          <View width={10} />
          {generateButton(0, <Phone />, language[currentLang].call, () =>
            Linking.openURL("tel://" + advert.user.phone)
          )}
        </Row>
      </Wrap>
    </>
  );
};
const Flexer = styled.View`
  flex: 1;
`;
const ButtonText = styled.Text`
  font-size: 14px;
  color: white;
  font-family: ${global.fonts.regular};
`;
const BottomButton = styled.View`
  width: 100%;
  background: ${({ type }) => (type ? "#999999" : "#2DBA49")};
  border-radius: 10px;
  height: 40px;
  flex-direction: row;
  align-items: center;
  justify-content: center;
`;
const MoveButton = styled.View`
  width: 40px;
  height: 40px;
  background: ${global.colors.main};
  border-radius: 10px;
  align-items: center;
  justify-content: center;
`;
const Authorization = styled.View`
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  margin-bottom: 5px;
`;
const MapImage = styled.Image`
  width: 100%;
  height: 170px;
  border-radius: 10px;
`;
const Description = styled.Text`
  color: #333333;
  font-size: 14px;
  font-family: ${global.fonts.regular};
  padding-bottom: 5px;
`;
const Row = styled.View`
  flex-direction: row;
  align-items: center;
`;

const Information = styled.Text`
  font-size: 14px;
  color: ${({ color }) => (color ? "#333" : "#999")};
  font-family: ${global.fonts.regular};
`;
const Title = styled.Text`
  color: #333333;
  font-size: 14px;
  font-family: ${global.fonts.medium};
`;
const Icon = styled.View`
  margin-left: ${({ left }) => left + "px"};
  margin-right: ${({ right }) => right + "px"};
  align-items: center;
  justify-content: center;
`;
const CoverImage = styled.Image`
  width: 100%;
  height: 170px;
`;
const Wrap = styled.View`
  margin: 5px 0 5px;
  background: white;
  padding: 10px;
  width: 100%;
  shadow-color: #000;
  shadow-opacity: 0.12;
  elevation: 3;
`;
const PaddingHorizontal = styled.View`
  padding: 0 5px;
`;
const Between = styled.View`
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
`;
export default ShowAdvertScreen;
