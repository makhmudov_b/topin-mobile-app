import React, { useState, useEffect } from "react";
import { View, ScrollView, Text, StatusBar } from "react-native";
import styled from "styled-components";
import global from "../../resources/global";
import Arrow from "../../assets/arrow-right.svg";
import Chosed from "../../assets/chosen.svg";
import NotChosed from "../../assets/not-chosen.svg";

import TouchableScale from "react-native-touchable-scale";

const MultiChoiceScreen = ({ navigation }) => {
  useEffect(() => {
    navigation.setParams({ title: "Сортировка" });
  }, []);
  const generateChoice = (choic, key) => {
    return (
      <TouchableScale
        style={{ width: "100%", marginBottom: 10 }}
        activeScale={0.96}
        onPress={() => handleCheckBox(key)}
        key={key}
      >
        <AdvertButton>
          <Icon left={5} right={10}>
            {choic.checked ? <Chosed /> : <NotChosed />}
          </Icon>
          <Information color={1}>{choic.title}</Information>
        </AdvertButton>
      </TouchableScale>
    );
  };
  function handleCheckBox(key) {
    let handler = choice.map((item, index) => {
      if (index === key) item.checked = !item.checked;
      return item;
    });
    setChoice(handler);
  }
  const [choice, setChoice] = useState([
    {
      title: "Сигнализация (Пожар)",
      checked: false,
    },
    {
      title: "Американская Кухня",
      checked: false,
    },
    {
      title: "Сигнализация (Пожар)",
      checked: false,
    },
    {
      title: "Американская Кухня",
      checked: false,
    },
    {
      title: "Сигнализация (Пожар)",
      checked: false,
    },
    {
      title: "Американская Кухня",
      checked: false,
    },
  ]);
  return (
    <>
      <StatusBar barStyle={`dark-content`} backgroundColor={`white`} />
      <ScrollView
        contentContainerStyle={{
          backgroundColor: "#f5f5f5",
          // flex: 1,
        }}
      >
        <Wrap>
          {choice.map((choic, key) => {
            return generateChoice(choic, key);
          })}
        </Wrap>
      </ScrollView>
      <Wrap>
        <Row>
          <Flexer>
            <TouchableScale onPress={() => navigation.goBack()}>
              <BottomButton>
                <ButtonText>Выбрать</ButtonText>
              </BottomButton>
            </TouchableScale>
          </Flexer>
        </Row>
      </Wrap>
    </>
  );
};
const Flexer = styled.View`
  flex: 1;
  padding: 10px 10px 0;
`;
const ButtonText = styled.Text`
  font-size: 14px;
  color: white;
  font-family: ${global.fonts.regular};
`;
const BottomButton = styled.View`
  width: 100%;
  background: ${global.colors.orange};
  border-radius: 10px;
  height: 40px;
  flex-direction: row;
  align-items: center;
  justify-content: center;
`;
const Row = styled.View`
  flex-direction: row;
  align-items: center;
`;
const AdvertButton = styled.View`
  flex-direction: row;
  align-items: center;
  height: 40px;
  background: white;
  /* width: 100%; */
  border-radius: 10px;
`;
const Information = styled.Text`
  font-size: 14px;
  color: ${({ color }) => (color ? "#333" : "#999")};
  font-family: ${global.fonts.regular};
`;
const Icon = styled.View`
  margin-left: ${({ left }) => left + "px"};
  margin-right: ${({ right }) => right + "px"};
  align-items: center;
  justify-content: center;
`;
const Wrap = styled.View`
  margin: 8px 0;
  background: white;
  /* padding: 10px; */
  width: 100%;
  align-items: center;
`;
export default MultiChoiceScreen;
