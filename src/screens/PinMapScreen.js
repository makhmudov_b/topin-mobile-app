import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  TouchableOpacity,
  TextInput,
  View,
  StatusBar,
  KeyboardAvoidingView,
} from "react-native";
import MapView, { Marker, PROVIDER_GOOGLE } from "react-native-maps";
import global from "../../resources/global";
// import AdvertHorizontal from "../components/AdvertHorizontal";
import AdvertHorizontal from "../components/AdvertHorizontal";
import styled from "styled-components";
import TouchableScale from "react-native-touchable-scale";
import MapMarker from "../../assets/marker.svg";
import Arrow from "../../assets/arrow-left.svg";
import Target from "../../assets/target.svg";
import Constants from "expo-constants";
import Context from "../components/Context";
import language from "../constants/language";

export default class PinMapScreen extends Component {
  static contextType = Context;
  constructor(props) {
    super(props);
    const { coordinates } = props.route.params;
    const data = coordinates
      ? {
          latitude: coordinates.latitude,
          longitude: coordinates.longitude,
          latitudeDelta: 0.04,
          longitudeDelta: 0.05,
        }
      : {
          latitude: 41.26465,
          longitude: 69.21627,
          latitudeDelta: 0.04,
          longitudeDelta: 0.05,
        };
    this.state = {
      region: data,
    };
  }
  componentDidMount() {
    if (!this.context.location) {
      this.context.getLocation();
    } else {
      this.goToUserLocation();
    }
  }
  onRegionChange(region) {
    this.setState({
      region: {
        latitude: region.latitude,
        longitude: region.longitude,
        latitudeDelta: region.latitudeDelta,
        longitudeDelta: region.longitudeDelta,
      },
    });
  }
  goToUserLocation() {
    if (!this.context.location) {
      this.context.getLocation();
    } else {
      const { coords } = this.context.location;
      const { latitude, longitude, altitude, heading } = coords;
      const temp_cordinate = { latitude: latitude, longitude: longitude };
      this.map.animateCamera({
        center: temp_cordinate,
        pitch: 2,
        heading: heading,
        altitude: altitude,
        zoom: 14,
      });
    }
  }
  render() {
    const { navigation, route } = this.props;
    const { method, coordinates } = route.params;
    const setMethod = () => {
      method(this.state.region);
      navigation.goBack();
    };
    return (
      <>
        <StatusBar backgroundColor={`white`} barStyle={`dark-content`} />
        <Container>
          <Between>
            <TouchableScale onPress={() => navigation.goBack()}>
              <View>
                <Arrow />
              </View>
            </TouchableScale>
          </Between>
          <MapView
            initialRegion={
              coordinates
                ? coordinates
                : {
                    latitude: 41.26465,
                    longitude: 69.21627,
                    latitudeDelta: 0.04,
                    longitudeDelta: 0.05,
                  }
            }
            ref={(map) => {
              this.map = map;
            }}
            onRegionChangeComplete={(region) => this.onRegionChange(region)}
            style={styles.mapStyle}
            loadingEnabled={true}
            showsUserLocation={true}
          ></MapView>
          <MarkerWrap pointerEvents={"none"}>
            <MapMarker style={{ zIndex: 999 }} pointerEvents={"none"} />
          </MarkerWrap>
          <UserWrap>
            <TouchableScale
              style={{ zIndex: 999 }}
              onPress={() => this.goToUserLocation()}
            >
              <TargetWrap>
                <Target />
              </TargetWrap>
            </TouchableScale>
          </UserWrap>
          <Bottom>
            <TouchableScale activeScale={0.95} onPress={() => setMethod()}>
              <BottomButton>
                <BottomText>
                  {language[this.context.currentLang].apply}
                </BottomText>
              </BottomButton>
            </TouchableScale>
          </Bottom>
        </Container>
      </>
    );
  }
}
const TargetWrap = styled.View`
  width: 40px;
  height: 40px;
  border-radius: 40px;
  background: ${global.colors.orange};
  align-items: center;
  justify-content: center;
`;
const BottomText = styled.Text`
  font-size: 12px;
  color: white;
  font-family: ${global.fonts.regular};
`;
const BottomButton = styled.View`
  padding: 0 20px;
  height: 50px;
  border-radius: 10px;
  align-items: center;
  justify-content: center;
  background: ${global.colors.orange};
`;
const Bottom = styled.View`
  width: 100%;
  align-items: center;
  position: absolute;
  bottom: 20px;
  width: ${global.strings.width + "px"};
  z-index: 20;
`;
const Between = styled.View`
  flex-direction: row;
  align-items: center;
  width: 100%;
  justify-content: space-between;
  padding: 0 10px;
  position: absolute;
  z-index: 20;
  top: ${Constants.statusBarHeight - 10 + "px"};
`;
const MarkerWrap = styled.View`
  position: absolute;
  width: 100%;
  height: 100%;
  z-index: 99;
  align-items: center;
  justify-content: center;
`;
const UserWrap = styled.View`
  position: absolute;
  width: 100%;
  height: 100%;
  align-items: flex-end;
  padding-right: 20px;
  justify-content: center;
`;

const Container = styled.View`
  flex: 1;
`;
const styles = StyleSheet.create({
  mapStyle: {
    flex: 1,
    // position: "absolute",
    // width: global.strings.width,
    // height: global.strings.height,
  },
});
