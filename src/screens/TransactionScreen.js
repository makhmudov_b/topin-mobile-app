import React, { useState, useEffect, useContext } from "react";
import {
  View,
  ScrollView,
  TextInput,
  Text,
  Image,
  TouchableOpacity,
  StatusBar,
} from "react-native";
import styled from "styled-components";
import global from "../../resources/global";
import Arrow from "../../assets/arrow-down.svg";
import TouchableScale from "react-native-touchable-scale";
import language from "../constants/language";
import Context from "../components/Context";
import LoadingScreen from "./LoadingScreen";
import Empty from "../../assets/no-operations.svg";

const TransactionScreen = ({ navigation, route }) => {
  const { apiService, token, currentLang } = useContext(Context);
  const [transactions, setTransactions] = useState([]);
  const [loading, setLoading] = useState(true);
  const [type, setType] = useState(1);
  useEffect(() => {
    apiService
      .getResources("/balance/history", token)
      .then((value) => {
        setTransactions(value.data);
        setLoading(false);
      })
      .catch((e) => setLoading(false));
  }, []);
  const generateTransaction = (transaction) => {
    return (
      <Transaction>
        <Between>
          <LightTitle>№ {transaction.id}</LightTitle>
          <LightTitle>{transaction.created_at.slice(0, 10)}</LightTitle>
        </Between>
        <Between padding={5}>
          <Title>Topin System</Title>
        </Between>
        <Between>
          <LightTitle>
            {parseInt(transaction.amount) > 0
              ? language[currentLang].moneyPlus
              : language[currentLang].moneyMinus}
          </LightTitle>
          <Price>{parseInt(transaction.amount)} UZS</Price>
        </Between>
      </Transaction>
    );
  };
  if (loading) return <LoadingScreen />;
  return (
    <>
      <StatusBar backgroundColor={"white"} barStyle={`dark-content`} />
      <ScrollView
        contentContainerStyle={{
          backgroundColor: "#f5f5f5",
        }}
      >
        <Wrap>
          {transactions.map((transaction, key) => {
            return (
              <React.Fragment key={key}>
                {generateTransaction(transaction)}
              </React.Fragment>
            );
          })}
          {!transactions.length && (
            <View style={{ alignItems: "center" }}>
              <Empty />
              <DropdownTitle>{language[currentLang].notExist}</DropdownTitle>
            </View>
          )}
        </Wrap>
      </ScrollView>
    </>
  );
};
const LightTitle = styled.Text`
  font-size: 12px;
  color: #999999;
`;
const Title = styled.Text`
  font-family: ${global.fonts.regular};
  font-size: 14px;
  color: #333;
  max-width: ${global.strings.width / 1.4 + "px"};
`;
const Price = styled.Text`
  font-family: ${global.fonts.medium};
  font-size: 12px;
  color: #333;
`;
const Transaction = styled.View`
  width: 100%;
  padding: 10px;
  min-height: 70px;
  /* shadow-offset: {width: 0, height: 2px}; */
  /* shadow-radius: 2; */
  shadow-color: #000;
  shadow-opacity: 0.12;
  elevation: 2;
  background: white;
  border-radius: 10px;
  margin-bottom: 10px;
`;
const MarginBottom = styled.View`
  margin-bottom: 10px;
  width: 100%;
`;
const DropdownTitle = styled.Text`
  color: #333333;
  font-size: 14px;
`;
const Dropdown = styled.View`
  background: #efefef;
  border-radius: 10px;
  padding: 6px 10px;
  flex-direction: row;
  align-items: center;
`;
const Wrap = styled.View`
  margin: 5px 0 5px;
  background: white;
  padding: 10px;
  flex: 1;
  box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.12);
  align-items: center;
`;
const Between = styled.View`
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  width: 100%;
  padding: ${({ padding }) => (padding ? padding + "px" : 0)} 0;
`;
export default TransactionScreen;
