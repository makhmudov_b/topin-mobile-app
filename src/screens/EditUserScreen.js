import React, { useState, useEffect, useContext } from "react";
import {
  View,
  ScrollView,
  TextInput,
  Text,
  Image,
  TouchableOpacity,
  Alert,
} from "react-native";
import styled from "styled-components";
import global from "../../resources/global";
import Arrow from "../../assets/arrow-right.svg";
import Profile from "../../assets/profile.svg";
import TouchableScale from "react-native-touchable-scale";
import Context from "../components/Context";
import language from "../constants/language";
import DateTimePicker from "@react-native-community/datetimepicker";
import { TabActions } from "@react-navigation/native";
import LoadingScreen from "./LoadingScreen";

const generateBlock = (title, value, method) => {
  return (
    <InputGroup>
      <InputTitle>{title} </InputTitle>
      <InputBlock onChangeText={(text) => method(text)} value={value} />
    </InputGroup>
  );
};
const generateChoice = (title, value, method) => {
  return (
    <InputGroup>
      <InputTitle>{title} </InputTitle>
      <TouchableScale activeScale={0.96} onPress={() => method()}>
        <View style={{ position: "relative" }}>
          <InputChoice>{value}</InputChoice>
          {/* <Icon>
            <Arrow />
          </Icon> */}
        </View>
      </TouchableScale>
    </InputGroup>
  );
};
const EditUserScreen = ({ navigation, route }) => {
  const { params } = route;
  const {
    apiService,
    token,
    phone,
    regions,
    user,
    regionId,
    categoryId,
    setCategoryId,
    setRegionId,
    setUser,
    setRegistered,
    categories,
    currentLang,
  } = useContext(Context);
  const type = user.is_business;
  useEffect(() => {
    if (user.category_id) setCategoryId(user.category_id);
  }, []);
  const [loading, setLoading] = useState(false);
  const [name, setName] = useState(user.name);
  const [inn, setINN] = useState(user.inn);
  const [workdays, setWorkdays] = useState(user.work_time);
  const [description, setDescription] = useState(user.description);
  const [address, setAddress] = useState(user.address);
  const [gender, setGender] = useState(user.gender == "male" ? 1 : 2);
  const [site, setSite] = useState(user.website);
  const [mail, setMail] = useState(user.email);
  const [date, setDate] = useState(
    user.birthday ? new Date(user.birthday) : new Date()
  );
  const [show, setShow] = useState(false);
  const [dateChosen, setDateChosen] = useState(true);
  const onChange = (event, selectedDate) => {
    setShow(false);
    if (selectedDate) {
      setDate(selectedDate);
      setDateChosen(true);
    }
  };
  const year = date.getFullYear();
  const month = date.getMonth() + 1;
  const day = date.getDate();
  const chosenDate = `${year}-${month < 10 ? "0" + month : month}-${day}`;
  const findRegion = (id) => {
    const found = regions.find((cat) => cat.id == id);
    if (currentLang == 1) return found.name_ru;
    if (currentLang == 2) return found.name_uz;
    if (currentLang == 3) return found.name_kr;
  };
  const findCategory = (id) => {
    const category = categories.find((cat) => cat.id == id);
    if (currentLang == 1) return category.name_ru;
    if (currentLang == 2) return category.name_uz;
    if (currentLang == 3) return category.name_kr;
  };
  const chooseGender = (id) => {
    setGender(id);
  };
  const getGender = !gender
    ? ""
    : gender == 1
    ? language[currentLang].male
    : language[currentLang].female;
  const genderList = [
    {
      id: 1,
      name_ru: "Мужчина",
      name_uz: "Erkak",
      name_kr: "Эркак",
    },
    {
      id: 2,
      name_ru: "Женщина",
      name_uz: "Ayol",
      name_kr: "Аёл",
    },
  ];
  const Bussiness = () => {
    return (
      <>
        <Between margin={10}>
          <Logo>
            <Profile color={"white"} width={40} height={40} />
          </Logo>
          <Type>
            <TypeInfo type={global.colors.orange}>
              <TypeInfoText type={"white"}>
                {language[currentLang].bussinessAcc}
              </TypeInfoText>
            </TypeInfo>
            <TypeInfo type={"white"}>
              <TypeInfoText type={"#999999"}>
                {language[currentLang].businessType}
              </TypeInfoText>
            </TypeInfo>
          </Type>
        </Between>
        {generateBlock(language[currentLang].bussinessName + "", name, setName)}
        {generateChoice(
          language[currentLang].sphere + ":",
          !categoryId ? "" : findCategory(categoryId),
          () =>
            navigation.navigate("CategoryChoice", {
              headerTitle: language[currentLang].chooseList,
            })
        )}
        {generateBlock(language[currentLang].inn + ":", inn, setINN)}
        {generateBlock(
          language[currentLang].workdays + ":",
          workdays,
          setWorkdays
        )}
        {generateBlock(
          language[currentLang].addDescr + ":",
          description,
          setDescription
        )}
        <Title>{language[currentLang].location}</Title>
        {generateChoice(
          language[currentLang].region + ":",
          user.country,
          () => {
            return;
          }
        )}
        {generateBlock(
          language[currentLang].address + ":",
          address,
          setAddress
        )}
        <Title>{language[currentLang].contactInfo}</Title>
        {generateBlock(language[currentLang].website + ":", site, setSite)}
        {generateBlock(language[currentLang].email + ":", mail, setMail)}
        {generateChoice(language[currentLang].phone + ":", phone, () => {
          return;
        })}
      </>
    );
  };
  const Simple = () => {
    return (
      <>
        <Between margin={10}>
          <Logo>
            <Profile color={"white"} width={40} height={40} />
          </Logo>
          <Type>
            <TypeInfo type={global.colors.orange}>
              <TypeInfoText type={"white"}>
                {language[currentLang].simple}
              </TypeInfoText>
            </TypeInfo>
            <TypeInfo type={"white"}>
              <TypeInfoText type={"#999999"}>
                {language[currentLang].simpleType}
              </TypeInfoText>
            </TypeInfo>
          </Type>
        </Between>
        {generateBlock(language[currentLang].fio + ":", name, setName)}
        {generateChoice(
          language[currentLang].birthday + ":",
          dateChosen ? chosenDate : "",
          () => setShow(true)
        )}
        {generateChoice(language[currentLang].sex + ":", getGender, () =>
          navigation.navigate("Choice", {
            choices: genderList,
            method: chooseGender,
            headerTitle: language[currentLang].chooseList,
          })
        )}
        <Title>{language[currentLang].location}</Title>
        {generateChoice(
          language[currentLang].region + ":",
          !regionId ? "" : findRegion(regionId),
          () =>
            navigation.navigate("RegionChoice", {
              headerTitle: language[currentLang].chooseList,
            })
        )}
        <Title>{language[currentLang].contactInfo}</Title>
        {generateBlock(language[currentLang].email + ":", mail, setMail)}
        {generateChoice(language[currentLang].phone + ":", phone, () => {
          return;
        })}
      </>
    );
  };
  const register = () => {
    if (!name.length && !dateChosen && gender == null && !mail.length) {
      Alert.alert("Нужно заполнить всё");
      return;
    }
    const body = {
      name: name,
      birthday: date,
      sex: gender == 1 ? 1 : 0,
      email: mail,
      is_business: 0,
      country: user.country,
    };
    if (!loading) setLoading(true);
    apiService
      .updateForm("/account", token, body)
      .then((value) => {
        setUser(value.data);
        setLoading(false);
        navigation.goBack();
      })
      .catch((e) => {
        setLoading(false);
      });
  };
  const registerBusiness = () => {
    if (
      !name.length &&
      !mail.length &&
      categoryId == 0 &&
      !description.length &&
      !address.length &&
      !site.length &&
      !workdays.length &&
      !inn.length
    ) {
      Alert.alert("Нужно заполнить всё");
      return;
    }
    const body = {
      email: mail,
      name: name,
      work_time: workdays,
      inn: inn,
      description: description,
      address: address,
      website: site,
      category_id: categoryId,
      country: user.country,
      is_business: 1,
    };
    if (!loading) setLoading(true);
    apiService
      .updateForm("/account", token, body)
      .then((value) => {
        setUser(value.data);
        setLoading(false);
        navigation.goBack();
      })
      .catch((e) => {
        setLoading(false);
      });
  };
  if (loading || !user) return <LoadingScreen />;
  return (
    <ScrollView
      contentContainerStyle={{
        backgroundColor: "#f5f5f5",
        // flex: 1,
      }}
    >
      <Wrap>
        {show && (
          <DateTimePicker
            timeZoneOffsetInMinutes={0}
            value={date}
            mode={`date`}
            display="default"
            onChange={onChange}
          />
        )}
        {type ? Bussiness() : Simple()}
        <TouchableScale
          style={{ width: "100%" }}
          onPress={type ? () => registerBusiness() : () => register()}
          activeScale={0.96}
        >
          <SaveButton>{language[currentLang].save}</SaveButton>
        </TouchableScale>
      </Wrap>
    </ScrollView>
  );
};
const SaveButton = styled.Text`
  background: ${global.colors.orange};
  text-align: center;
  line-height: 40px;
  color: white;
  height: 40px;
  border-radius: 10px;
  margin-bottom: 5px;
  margin-top: 10px;
`;
const Title = styled.Text`
  color: #333333;
  font-size: 14px;
  font-family: ${global.fonts.medium};
  width: 100%;
  line-height: 22px;
  padding-bottom: 10px;
`;
const Icon = styled.View`
  position: absolute;
  top: -5px;
  /* right: -10px; */
  width: 20px;
  height: 20px;
  z-index: 90;
  background: blue;
`;
const InputChoice = styled.Text`
  position: relative;
  z-index: -1;
  margin-top: 5px;
  background: white;
  /* shadow-radius: 2; */
  shadow-color: #000;
  shadow-opacity: 0.12;
  elevation: 3;
  width: 100%;
  height: 40px;
  border-radius: 10px;
  font-family: ${global.fonts.regular};
  padding: 10px 15px;
`;
const InputBlock = styled.TextInput`
  margin-top: 5px;
  background: white;
  shadow-color: #000;
  shadow-opacity: 0.12;
  /* shadow-radius: 2; */
  elevation: 3;
  width: 100%;
  height: 40px;
  border-radius: 10px;
  font-family: ${global.fonts.regular};
  padding: 10px 15px;
`;
const InputTitle = styled.Text`
  color: #999999;
  font-family: ${global.fonts.regular};
  font-size: 14px;
`;
const InputGroup = styled.View`
  width: 100%;
  margin-bottom: 10px;
`;
const TypeInfoText = styled.Text`
  font-family: ${global.fonts.regular};
  font-size: 14px;
  color: ${({ type }) => type};
`;
const TypeInfo = styled.View`
  height: 40px;
  align-items: center;
  border-radius: 10px;
  /* box-shadow: 0px 0px 2px rgba(0, 0, 0, 0.12); */
  shadow-color: #000;
  shadow-opacity: 0.12;
  /* shadow-radius: 2; */
  elevation: 3;
  justify-content: center;
  background: ${({ type }) => type};
  margin-bottom: 10px;
`;
const Type = styled.View`
  flex: 1;
  padding-left: 10px;
`;
const LogoText = styled.Text`
  color: #ffffff;
  font-size: 16px;
  font-family: ${global.fonts.medium};
`;
const Logo = styled.View`
  align-items: center;
  justify-content: center;
  background: #999999;
  height: 90px;
  width: 90px;
  border-radius: 10px;
`;
const CoverImage = styled.Image`
  width: 100%;
  height: 170px;
  border-radius: 10px;
`;
const Wrap = styled.View`
  margin: 5px 0 5px;
  background: white;
  padding: 10px;
  shadow-color: #000;
  shadow-opacity: 0.12;
  elevation: 3;
  align-items: center;
`;
// const Title = styled.Text`
//   font-family: ${global.fonts.medium};
//   font-size: 24px;
//   line-height: 36px;
//   color: white;
//   padding-bottom: 20px;
// `;
const InputWrap = styled.View`
  background: white;
  border-radius: 10px;
  height: 40px;
  width: 100%;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
`;
const InputLeft = styled.View`
  height: 40px;
  width: 55px;
  border-right-width: 1px;
  border-right-color: #efefef;
  align-items: center;
  justify-content: center;
  /* border: solid #efefef; */
`;
const InfoText = styled.Text`
  color: #aeaeae;
  font-size: 14px;
  font-family: ${global.fonts.regular};
  text-align: center;
  margin: 20px 0;
`;
const OrangeText = styled.Text`
  color: ${global.colors.orange};
`;
const LoginInput = styled.TextInput`
  flex: 1;
  height: 40px;
  color: #333333;
  font-size: 14px;
  font-family: ${global.fonts.regular};
  padding: 0 10px;
`;
const PaddingHorizontal = styled.View`
  padding: 0 5px;
`;
const Between = styled.View`
  flex-direction: row;
  align-items: center;
  width: 100%;
  justify-content: space-between;
  margin-top: ${({ margin }) => margin + "px" ?? 0};
`;
export default EditUserScreen;
