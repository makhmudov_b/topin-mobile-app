import React, { useState, useContext } from "react";
import {
  View,
  ScrollView,
  TextInput,
  Text,
  Image,
  TouchableOpacity,
  KeyboardAvoidingView,
} from "react-native";
import styled from "styled-components";
import global from "../../resources/global";
import TouchableScale from "react-native-touchable-scale";
import Profile from "../../assets/type.svg";
import language from "../constants/language";
import Context from "../components/Context";

const TypeScreen = ({ navigation }) => {
  const { currentLang } = useContext(Context);
  const [type, setType] = useState();
  return (
    <KeyboardAvoidingView
      style={{
        backgroundColor: "#999",
        flex: 1,
      }}
    >
      <Wrap>
        <Profile />
        <View height={20} />
        <Title>{language[currentLang].chooseTypeAcc}</Title>
        <Between>
          <TouchableScale
            style={{ flex: 1 }}
            activeScale={0.96}
            onPress={() => setType(0)}
          >
            <Choice
              active={!type ? global.colors.orange : "white"}
              type={"left"}
            >
              <ChoiceText active={!type ? "white" : "#999999"}>
                {language[currentLang].simple}
              </ChoiceText>
            </Choice>
          </TouchableScale>
          <TouchableScale
            style={{ flex: 1 }}
            activeScale={0.96}
            onPress={() => setType(1)}
          >
            <Choice
              activeScale={0.96}
              // onPress={() => setType(1)}
              active={type ? global.colors.orange : "white"}
              type={"right"}
            >
              <ChoiceText active={type ? "white" : "#999999"}>
                {language[currentLang].bussinessAcc}
              </ChoiceText>
            </Choice>
          </TouchableScale>
        </Between>
        <View height={40} />
        <TouchableScale
          activeScale={0.96}
          onPress={() => navigation.navigate("Register", { type: type })}
          style={{ width: "100%" }}
        >
          <LoginButton>
            <LoginText>{language[currentLang].verification}</LoginText>
          </LoginButton>
        </TouchableScale>
      </Wrap>
    </KeyboardAvoidingView>
  );
};
const LoginText = styled.Text`
  color: white;
  font-size: 14px;
  text-align: center;
  font-family: ${global.fonts.regular};
`;
const LoginButton = styled.View`
  width: 100%;
  background: #2dba49;
  border-radius: 10px;
  height: 40px;
  align-items: center;
  justify-content: center;
`;
const Wrap = styled.View`
  margin: 5px 0 5px;
  background: ${global.colors.main};
  padding: 10px;
  flex: 1;
  /* box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.12); */
  justify-content: center;
  align-items: center;
`;
const Title = styled.Text`
  font-family: ${global.fonts.medium};
  font-size: 24px;
  line-height: 36px;
  color: white;
  padding-bottom: 20px;
`;
const InputWrap = styled.View`
  background: white;
  border-radius: 10px;
  height: 40px;
  flex: 1;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
`;
const InputRight = styled.Text`
  height: 40px;
  width: 40px;
  background: white;
  border-radius: 10px;
  text-align: center;
  line-height: 40px;
  font-family: ${global.fonts.regular};
  color: ${global.colors.orange};
  margin-left: 10px;
`;
const InfoText = styled.Text`
  color: #aeaeae;
  font-size: 14px;
  font-family: ${global.fonts.regular};
  text-align: center;
  margin: 20px 0;
`;
const OrangeText = styled.Text`
  color: ${global.colors.orange};
`;
const LoginInput = styled.TextInput`
  flex: 1;
  height: 40px;
  color: #333333;
  font-size: 14px;
  font-family: ${global.fonts.regular};
  padding: 0 10px;
  text-align: center;
`;
const PaddingHorizontal = styled.View`
  padding: 0 5px;
`;
const Between = styled.View`
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
`;
const Choice = styled.View`
  /* flex: 1; */
  width: 100%;
  height: 40px;
  /* background:blue; */
  background: ${({ active }) => active};
  border-top-left-radius: ${({ type }) => (type === "left" ? "10px" : 0)};
  border-bottom-left-radius: ${({ type }) => (type === "left" ? "10px" : 0)};
  border-top-right-radius: ${({ type }) => (type === "right" ? "10px" : 0)};
  border-bottom-right-radius: ${({ type }) => (type === "right" ? "10px" : 0)};
  align-items: center;
  justify-content: center;
`;
const ChoiceText = styled.Text`
  font-family: ${global.fonts.regular};
  font-size: 14px;
  color: ${({ active }) => active};
`;
export default TypeScreen;
