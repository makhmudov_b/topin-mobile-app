import React, { useState, useEffect, useContext } from "react";
import {
  View,
  ScrollView,
  TextInput,
  Text,
  Image,
  TouchableOpacity,
  StatusBar,
} from "react-native";
import styled from "styled-components";
import global from "../../resources/global";
import Phone from "../../assets/phone.svg";
import TouchableScale from "react-native-touchable-scale";
import * as Linking from "expo-linking";
import Context from "../components/Context";
import language from "../constants/language";
import { TabActions } from "@react-navigation/native";
import LoadingScreen from "./LoadingScreen";

const SettingScreen = ({ navigation }) => {
  const { currentLang, setCurrentLang, logout } = useContext(Context);
  const [loading, setLoading] = useState(false);
  const callPhone = () => {
    Linking.openURL("tel://+998 99 948-20-00");
  };
  const generateChoice = (title, value, radius) => {
    return (
      <TouchableScale
        style={{ flex: 1 }}
        activeScale={0.96}
        onPress={() => setCurrentLang(value)}
      >
        <Choice active={currentLang == value} type={radius}>
          <ChoiceText active={currentLang == value ? "white" : "#999999"}>
            {title}
          </ChoiceText>
        </Choice>
      </TouchableScale>
    );
  };
  const logoutAction = () => {
    setLoading(true);
    logout()
      .then((value) => {
        if (value) {
          setLoading(false);
          const jumpToAction = TabActions.jumpTo("Profile");
          navigation.dispatch(jumpToAction);
          navigation.navigate("Home");
        }
      })
      .catch((e) => setLoading(false));
  };
  if (loading) return <LoadingScreen />;
  return (
    <>
      <StatusBar backgroundColor={"white"} barStyle={`dark-content`} />
      <ScrollView
        contentContainerStyle={{
          backgroundColor: "#f5f5f5",
          // flex: 1,
        }}
      >
        <Wrap>
          <Between>
            <View>
              <Title>{language[currentLang].support}</Title>
              <PhoneText>998 99 948-20-00</PhoneText>
            </View>
            <View>
              <TouchableScale onPress={() => callPhone()}>
                <PhoneButton>
                  <Phone width={24} height={24} />
                </PhoneButton>
              </TouchableScale>
            </View>
          </Between>
        </Wrap>
        <Wrap>
          <View>
            <Lang>{language[currentLang].lang}: </Lang>
          </View>
          <Between>
            {generateChoice("РУ", 1, "left")}
            {generateChoice("UZ", 2)}
            {generateChoice("УЗ", 3, "right")}
          </Between>
          {/* <Flexer>
            <TouchableScale>
              <BottomButton type={1}>
                <ButtonText>Удалить аккаунт</ButtonText>
              </BottomButton>
            </TouchableScale>
          </Flexer> */}
        </Wrap>
      </ScrollView>
      <Wrap>
        <TouchableScale onPress={() => logoutAction()}>
          <BottomButton type={0}>
            <ButtonText>{language[currentLang].logout}</ButtonText>
          </BottomButton>
        </TouchableScale>
      </Wrap>
    </>
  );
};
const Flexer = styled.View`
  margin-top: 20px;
`;
const ButtonText = styled.Text`
  font-size: 14px;
  color: white;
  font-family: ${global.fonts.regular};
`;
const BottomButton = styled.View`
  width: 100%;
  background: ${({ type }) => (type ? "#FC574D" : global.colors.orange)};
  border-radius: 10px;
  height: 40px;
  flex-direction: row;
  align-items: center;
  justify-content: center;
`;
const PhoneButton = styled.View`
  width: 40px;
  height: 40px;
  background: #2dba49;
  align-items: center;
  justify-content: center;
  border-radius: 10px;
`;
const PhoneText = styled.Text`
  font-size: 12px;
  color: #333333;
  padding-top: 5px;
  font-family: ${global.fonts.regular};
`;
const Lang = styled.Text`
  font-size: 10px;
  color: #999999;
  font-family: ${global.fonts.regular};
  padding-bottom: 5px;
  padding-left: 3px;
`;
const Wrap = styled.View`
  margin: 5px 0 5px;
  background: white;
  padding: 10px;
`;
const Title = styled.Text`
  font-family: ${global.fonts.medium};
  font-size: 14px;
  color: #333;
  /* padding-bottom: 10px; */
`;
const Between = styled.View`
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  width: 100%;
`;
const Choice = styled.View`
  /* flex: 1; */
  width: 100%;
  height: 50px;
  shadow-color: #000;
  shadow-opacity: 0.12;
  elevation: 3;
  background: ${({ active }) => (active ? global.colors.orange : "white")};
  border-bottom-width: 1px;
  border-bottom-color: ${({ active }) =>
    active ? global.colors.orange : "white"};
  border-top-left-radius: ${({ type }) => (type === "left" ? 10 + "px" : 0)};
  border-bottom-left-radius: ${({ type }) => (type === "left" ? 10 + "px" : 0)};
  border-top-right-radius: ${({ type }) => (type === "right" ? 10 + "px" : 0)};
  border-bottom-right-radius: ${({ type }) =>
    type === "right" ? 10 + "px" : 0};
  border-left-width: ${({ type }) => (!type ? 2 + "px" : 0)};
  border-right-width: ${({ type }) => (!type ? 2 + "px" : 0)};
  border-left-color: ${({ type }) => (!type ? "#F5F5F5" : 0)};
  border-right-color: ${({ type }) => (!type ? "#F5F5F5" : 0)};
  align-items: center;
  justify-content: center;
`;
const ChoiceText = styled.Text`
  font-family: ${global.fonts.medium};
  font-size: 14px;
  color: ${({ active }) => active};
`;
export default SettingScreen;
