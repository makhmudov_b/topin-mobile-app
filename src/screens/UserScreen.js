import React, { useState, useEffect, useContext } from "react";
import {
  View,
  ScrollView,
  TextInput,
  Text,
  Image,
  TouchableOpacity,
} from "react-native";
import styled from "styled-components";
import global from "../../resources/global";
import Arrow from "../../assets/arrow-right.svg";

import Profile from "../../assets/person.svg";
import TouchableScale from "react-native-touchable-scale";
import AdvertHorizontal from "../components/AdvertHorizontal";
import language from "../constants/language";
import Context from "../components/Context";

const generateInfo = (left, right, colored) => {
  return (
    <Between margin={5}>
      <Information color={1}>{left}</Information>
      <Information colored={colored}>{right}</Information>
    </Between>
  );
};
const UserScreen = ({ route }) => {
  const { apiService, currentLang, categories } = useContext(Context);
  const { user } = route.params;
  const [adverts, setAdverts] = useState([]);
  const type = parseInt(user.is_business);
  useEffect(() => {
    const body = {
      user_id: user.id,
    };
    apiService.searchAdvert(body).then((value) => {
      setAdverts(value.data);
    });
  }, []);
  const Bussiness = () => {
    const getCategory = () => {
      const found = categories.find(
        (category) => category.id == user.category_id
      );
      if (found) {
        if (currentLang == 1) return found.name_ru;
        if (currentLang == 2) return found.name_uz;
        if (currentLang == 3) return found.name_kr;
      }
      return "";
    };
    return (
      <>
        <Wrap>
          <UserWrap>
            <CoverImage resizeMode={`cover`} source={{ uri: user.banner }} />
            <UserImage>
              {!user.image ? (
                <Profile width={100} height={100} />
              ) : (
                <Image
                  resizeMode={`cover`}
                  style={{ width: "100%", height: "100%" }}
                  source={{ uri: user.image }}
                />
              )}
            </UserImage>
          </UserWrap>
          <View height={60} />
          <Between>
            <View>
              <Title>{user.name}</Title>
            </View>
            <View>
              <Dropdown>
                <DropdownTitle> {user.website}</DropdownTitle>
              </Dropdown>
            </View>
          </Between>
          <View height={10} />
          {generateInfo(
            language[currentLang].inSystem + ":",
            user.created_at.slice(0, 10)
          )}
          {generateInfo(
            language[currentLang].advertsCount + ":",
            user.advert_count
          )}
          {generateInfo(language[currentLang].sphere, getCategory())}
          {generateInfo(language[currentLang].address + ":", user.address)}
          {generateInfo(language[currentLang].workdays + ":", user.work_time)}
          {generateInfo(
            language[currentLang].accType + ":",
            language[currentLang].bussinessAcc,
            1
          )}
        </Wrap>
        <Wrap>
          <Between>
            <Title>{language[currentLang].addDescr}</Title>
          </Between>
          <View height={10} />
          <Between>
            <Description>{user.description}</Description>
          </Between>
        </Wrap>
      </>
    );
  };
  const Simple = () => {
    return (
      <>
        <UserLine />
        <Wrap>
          <UserWrap>
            <UserImage>
              <Profile width={100} height={100} />
            </UserImage>
          </UserWrap>
          <View height={60} />
          <Title>{user.name}</Title>
          <View height={10} />
          {generateInfo(
            language[currentLang].inSystem + ":",
            user.created_at.slice(0, 10)
          )}
          {generateInfo(
            language[currentLang].advertsCount + ":",
            user.advert_count
          )}
          {generateInfo(
            language[currentLang].accType + ":",
            !parseInt(user.is_business)
              ? language[currentLang].simple
              : language[currentLang].bussinessAcc
          )}
        </Wrap>
      </>
    );
  };
  return (
    <ScrollView
      contentContainerStyle={{
        backgroundColor: "#f5f5f5",
        // flex: 1,
      }}
    >
      {type ? Bussiness() : Simple()}
      <Wrap>
        <Between>
          <View>
            <Title>{language[currentLang].adverts}</Title>
          </View>
          <View>
            {/* <TouchableScale activeScale={0.96}>
              <Dropdown>
                <DropdownTitle>Сначало новые</DropdownTitle>
                <View>
                  <Arrow />
                </View>
              </Dropdown>
            </TouchableScale> */}
          </View>
        </Between>
        {adverts.map((advert, key) => (
          <AdvertHorizontal key={key} advert={advert} />
        ))}
        {!adverts.length && (
          <Image
            source={global.images.loading}
            style={{ height: 100 }}
            resizeMode={"contain"}
          />
        )}
      </Wrap>
    </ScrollView>
  );
};
const UserLine = styled.View`
  width: 100%;
  height: 60px;
  background: ${global.colors.orange};
`;
const UserWrap = styled.View`
  position: relative;
  width: 100%;
`;
const UserImage = styled.View`
  width: 100px;
  height: 100px;
  border-radius: 100px;
  background: #c4c4c4;
  position: absolute;
  bottom: -50px;
  left: ${global.strings.width / 2 - 50 + "px"};
  border-width: 5px;
  border-color: white;
  overflow: hidden;
  align-items: center;
  justify-content: center;
`;
const DropdownTitle = styled.Text`
  color: #333333;
  font-size: 10px;
  margin-right: 5px;
`;
const Description = styled.Text`
  color: #333333;
  font-size: 14px;
  font-family: ${global.fonts.regular};
  padding-bottom: 5px;
`;
const Dropdown = styled.View`
  background: #efefef;
  border-radius: 10px;
  padding: 6px 10px;
  flex-direction: row;
  align-items: center;
`;
const Row = styled.View`
  flex-direction: row;
  align-items: center;
  /* flex: 1; */
`;
const AdvertButton = styled.View`
  flex-direction: row;
  justify-content: space-between;
  padding: 10px 15px;
  height: 40px;
  shadow-color: #000;
  shadow-opacity: 0.12;
  elevation: 3;
  background: white;
  width: 100%;
  border-radius: 10px;
`;
const Information = styled.Text`
  font-size: 14px;
  color: ${({ color, colored }) =>
    colored ? global.colors.orange : color ? "#333" : "#999"};
  font-family: ${global.fonts.regular};
`;
const SaveButton = styled.Text`
  background: ${global.colors.orange};
  text-align: center;
  line-height: 40px;
  color: white;
  height: 40px;
  border-radius: 10px;
  margin-bottom: 5px;
  margin-top: 10px;
`;
const Title = styled.Text`
  color: #333333;
  font-size: 14px;
  font-family: ${global.fonts.medium};
  text-align: center;
`;

const CoverImage = styled.Image`
  width: ${global.strings.width + "px"};
  height: 170px;
`;
const Wrap = styled.View`
  margin: 0px 0 5px;
  background: white;
  padding: 10px 0;
  box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.12);
  align-items: center;
`;
// const Title = styled.Text`
//   font-family: ${global.fonts.medium};
//   font-size: 24px;
//   line-height: 36px;
//   color: white;
//   padding-bottom: 20px;
// `;

const Between = styled.View`
  flex-direction: row;
  align-items: center;
  width: 100%;
  padding: 0 10px;
  justify-content: space-between;
  margin-top: ${({ margin }) => (margin ? margin + "px" : 0)};
`;
export default UserScreen;
