import React, { useState, useEffect, useContext } from "react";
import {
  View,
  ScrollView,
  TextInput,
  Text,
  Image,
  TouchableOpacity,
  StatusBar,
} from "react-native";
import styled from "styled-components";
import global from "../../resources/global";
import TouchableScale from "react-native-touchable-scale";
import Context from "../components/Context";
import LoadingScreen from "./LoadingScreen";
import Empty from "../../assets/no-messages.svg";
import language from "../constants/language";

const MessagesScreen = ({ navigation, route }) => {
  const [loading, setLoading] = useState(true);
  const [messages, setMessages] = useState([]);
  const { apiService, token, currentLang } = useContext(Context);
  useEffect(() => {
    apiService
      .getResources("/messages", token)
      .then((value) => {
        setMessages(value.data);
        setLoading(false);
      })
      .catch((e) => setLoading(false));
  }, []);
  const generateMessage = (message) => {
    return (
      <TouchableScale
        style={{ marginBottom: 10 }}
        activeScale={0.97}
        onPress={() =>
          navigation.navigate("MessageShow", { advertId: message.advert.id })
        }
      >
        <Transaction>
          <Row>
            <View>
              <MessageImage>
                <Image
                  source={{ uri: message.advert.image[0] }}
                  style={{ width: "100%", height: "100%" }}
                />
              </MessageImage>
            </View>
            <View style={{ flex: 1 }}>
              <Between>
                <Title>{message.advert.name}</Title>
                <LightTitle>{message.created_at.slice(0, 10)}</LightTitle>
              </Between>
              <Between>
                <LightTitle active={message.count > 0}>
                  {message.body}
                </LightTitle>
                <Count active={message.count > 0}>
                  {message.count > 0 ? message.count : null}
                </Count>
              </Between>
            </View>
          </Row>
        </Transaction>
      </TouchableScale>
    );
  };
  if (loading) return <LoadingScreen />;
  return (
    <>
      <StatusBar backgroundColor={"white"} barStyle={`dark-content`} />
      <ScrollView
        contentContainerStyle={{
          backgroundColor: "#f5f5f5",
        }}
      >
        <Wrap>
          <FullWidth>
            {messages.map((message, key) => {
              return (
                <React.Fragment key={key}>
                  {generateMessage(message)}
                </React.Fragment>
              );
            })}
            {!messages.length && (
              <View style={{ alignItems: "center" }}>
                <Empty />
                <DropdownTitle>{language[currentLang].notExist}</DropdownTitle>
              </View>
            )}
          </FullWidth>
        </Wrap>
      </ScrollView>
    </>
  );
};
const FullWidth = styled.View`
  width: 100%;
`;
const DropdownTitle = styled.Text`
  color: #333333;
  font-size: 14px;
  text-align: center;
`;
const MessageImage = styled.View`
  width: 40px;
  height: 40px;
  align-items: center;
  justify-content: center;
  border-radius: 10px;
  background: #aeaeae;
  margin-right: 10px;
  overflow: hidden;
`;
const LightTitle = styled.Text`
  font-size: 12px;
  color: ${({ active }) => (active ? global.colors.orange : "#999999")};
`;
const Title = styled.Text`
  font-family: ${global.fonts.regular};
  font-size: 12px;
  color: #333;
  max-width: ${global.strings.width / 1.4 + "px"};
`;
const Count = styled.Text`
  font-family: ${global.fonts.medium};
  font-size: 12px;
  color: ${({ active }) => (active ? "white" : "#333")};
  background: ${({ active }) => (active ? global.colors.orange : "white")};
  width: 20px;
  height: 20px;
  text-align: center;
  line-height: 20px;
  border-radius: 10px;
  margin-top: 5px;
`;
const Transaction = styled.View`
  width: 100%;
  padding: 10px;
  min-height: 60px;
  /* shadow-offset: {width: 0, height: 2px}; */
  /* shadow-radius: 2; */
  shadow-color: #000;
  shadow-opacity: 0.12;
  elevation: 2;
  background: white;
  border-radius: 10px;
`;
const Wrap = styled.View`
  margin: 5px 0 5px;
  background: white;
  padding: 10px;
  flex: 1;
  box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.12);
  align-items: center;
`;
const Row = styled.View`
  flex-direction: row;
  align-items: center;
`;
const Between = styled.View`
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  /* width: 100%; */
  padding: ${({ padding }) => (padding ? padding + "px" : 0)} 0;
`;
export default MessagesScreen;
