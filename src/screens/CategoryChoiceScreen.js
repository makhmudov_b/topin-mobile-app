import React, { useState, useEffect, useContext } from "react";
import { View, ScrollView, Text, StatusBar } from "react-native";
import styled from "styled-components";
import global from "../../resources/global";
import Arrow from "../../assets/arrow-right.svg";
// import { useNavigation } from "@react-navigation/native";
import Context from "../components/Context";
import language from "../constants/language";
import { TabActions } from "@react-navigation/native";

import TouchableScale from "react-native-touchable-scale";

const CategoryChoiceScreen = ({ navigation, route }) => {
  const { categories, currentLang, setCategoryId } = useContext(Context);
  const parentCategory = route.params.categoryId;
  const method = route.params.method;
  const [data, setData] = useState([]);
  const [chosen, setChosen] = useState(-1);
  // const navigation = useNavigation();
  useEffect(() => {
    const childCats = categories.filter(
      (category) => category.parent_id == parentCategory
    );
    setData(childCats);
  }, []);
  const searchByCategory = (value) => {
    const childCats = categories.filter(
      (category) => category.parent_id == value
    );
    setChosen(value);
    setData(childCats);
  };
  const findCategory = (id) => {
    const category = categories.find((cats) => cats.id == id);
    if (currentLang == 1) return category.name_ru;
    if (currentLang == 2) return category.name_uz;
    if (currentLang == 3) return category.name_kr;
  };
  const setCategory = (value) => {
    navigation.goBack();
    if (method) {
      method(value);
    } else {
      // setCategoryId(value);
      const jumpToAction = TabActions.jumpTo("Search", { categoryId: value });
      navigation.dispatch(jumpToAction);
    }
  };
  const generateButton = (name, id, method) => {
    return (
      <TouchableScale
        style={{ width: "100%", marginBottom: 15 }}
        activeScale={0.96}
        onPress={() => method(id)}
        key={id}
      >
        <AdvertButton>
          <Row>
            <Information color={1}>{name}</Information>
          </Row>
          <Row>
            <Icon left={5} right={5}>
              <Arrow width={20} height={20} />
            </Icon>
          </Row>
        </AdvertButton>
      </TouchableScale>
    );
  };
  return (
    <>
      <StatusBar barStyle={`dark-content`} backgroundColor={`white`} />
      <ScrollView
        contentContainerStyle={{
          backgroundColor: "#f5f5f5",
        }}
      >
        <Wrap>
          {(parentCategory || chosen > 0) &&
            generateButton(
              `${language[currentLang].chooseAll} ${
                // parentCategory
                //   ? "- " + findCategory(parentCategory)
                // :
                chosen > 0 ? "- " + findCategory(chosen) : ""
              }`,
              chosen,
              () => setCategory(chosen > 0 ? chosen : parentCategory)
            )}
          {data.map((category) => {
            return generateButton(
              currentLang == 1
                ? category.name_ru
                : currentLang == 2
                ? category.name_uz
                : category.name_kr,
              category.id,
              () => searchByCategory(category.id)
            );
          })}
        </Wrap>
      </ScrollView>
    </>
  );
};
const Row = styled.View`
  flex-direction: row;
  align-items: center;
`;
const AdvertButton = styled.View`
  flex-direction: row;
  justify-content: space-between;
  padding: 10px 15px;
  height: 40px;
  shadow-color: #000;
  shadow-opacity: 0.2;
  elevation: 5;
  shadow-radius: 5px;
  background: white;
  width: 100%;
  border-radius: 10px;
`;
const Information = styled.Text`
  font-size: 14px;
  color: ${({ color }) => (color ? "#333" : "#999")};
  font-family: ${global.fonts.regular};
`;
const Icon = styled.View`
  margin-left: ${({ left }) => left + "px"};
  margin-right: ${({ right }) => right + "px"};
  align-items: center;
  justify-content: center;
`;
const Wrap = styled.View`
  margin: 5px 0 5px;
  background: white;
  padding: 10px;
  flex: 1;
  shadow-color: #000;
  shadow-opacity: 0.12;
  elevation: 3;
  align-items: center;
`;
const PaddingHorizontal = styled.View`
  padding: 0 5px;
`;
const Between = styled.View`
  flex-direction: row;
  align-items: center;
  width: 100%;
  justify-content: space-between;
  margin-top: ${({ margin }) => (margin ? margin + "px" : 0)};
`;
export default CategoryChoiceScreen;
