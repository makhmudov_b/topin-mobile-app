import React, { useContext } from "react";
import { WebView } from "react-native-webview";
import { TabActions, useNavigation } from "@react-navigation/native";
import Context from "../components/Context";
export default function WebScreen({ route }) {
  const { getUser } = useContext(Context);
  const { url } = route.params;
  const navigation = useNavigation();
  const navigateToProfile = () => {
    const jumpToAction = TabActions.jumpTo("Profile");
    navigation.dispatch(jumpToAction);
    navigation.navigate("Home");
    getUser();
  };
  return (
    <WebView
      source={{ uri: url }}
      onNavigationStateChange={(navState) => {
        if (navState.url == "http://topin.indev.uz/operations") {
          navigateToProfile();
        }
      }}
    />
  );
}
