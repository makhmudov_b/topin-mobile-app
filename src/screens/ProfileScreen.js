import React, { useState, useEffect, useContext } from "react";
import {
  View,
  ScrollView,
  TextInput,
  Text,
  Image,
  TouchableOpacity,
} from "react-native";
import styled from "styled-components";
import global from "../../resources/global";
import Arrow from "../../assets/arrow-right.svg";
import Favorite from "../../assets/fav-icon.svg";
import Message from "../../assets/mess-icon.svg";
import Advert from "../../assets/advert-icon.svg";
import Transaction from "../../assets/trans-icon.svg";
import Setting from "../../assets/setting-icon.svg";
import Profile from "../../assets/profile.svg";
import Person from "../../assets/person.svg";

import Edit from "../../assets/edit-button.svg";
import TouchableScale from "react-native-touchable-scale";
import Context from "../components/Context";
import language from "../constants/language";
import LoadingScreen from "./LoadingScreen";

const generateInfo = (left, right) => {
  return (
    <Between margin={5}>
      <Information color={1}>{left}</Information>
      <Information>{right}</Information>
    </Between>
  );
};
const GenerateButton = ({ icon, title, count, method }) => {
  return (
    <TouchableScale
      onPress={() => method()}
      style={{ width: "100%", marginBottom: 10 }}
      activeScale={0.96}
    >
      <AdvertButton>
        <Row>
          <Icon right={10} left={0}>
            {icon}
          </Icon>
          <Information color={1}>{title}</Information>
        </Row>
        <Row>
          <Information>{count}</Information>
          <Icon left={5} right={5}>
            <Arrow width={20} height={20} />
          </Icon>
        </Row>
      </AdvertButton>
    </TouchableScale>
  );
};
const ProfileScreen = ({ navigation }) => {
  const {
    apiService,
    token,
    phone,
    categories,
    currentLang,
    user,
  } = useContext(Context);
  const Bussiness = () => {
    const getCategory = () => {
      const found = categories.find(
        (category) => category.id == user.category_id
      );
      if (found) {
        if (currentLang == 1) return found.name_ru;
        if (currentLang == 2) return found.name_uz;
        if (currentLang == 3) return found.name_kr;
      }
      return "";
    };
    return (
      <>
        <TouchableScale style={{ width: "100%" }} activeScale={0.96}>
          <CoverImage resizeMode={`cover`} source={{ uri: user.banner }} />
        </TouchableScale>
        <Between margin={0}>
          <TouchableScale activeScale={0.96}>
            <Logo>
              {!user.image ? (
                <Person width={90} height={90} />
              ) : (
                <Image
                  resizeMode={`cover`}
                  style={{ width: "100%", height: "100%", borderRadius: 10 }}
                  source={{ uri: user.image }}
                />
              )}
            </Logo>
          </TouchableScale>
          <Type>
            <Between>
              <Title>{user.name}</Title>
              <View>
                <TouchableScale
                  activeScale={0.96}
                  onPress={() => navigation.navigate("EditUser")}
                >
                  <EditButton>
                    <Edit />
                  </EditButton>
                </TouchableScale>
              </View>
            </Between>
            <TypeInfo type={global.colors.orange}>
              <TypeInfoText type={"white"}>
                {!type
                  ? language[currentLang].simple
                  : language[currentLang].bussinessAcc}
              </TypeInfoText>
            </TypeInfo>
          </Type>
        </Between>
        {generateInfo(
          language[currentLang].inSystem + ":",
          user.created_at.slice(0, 10)
        )}
        {generateInfo(
          language[currentLang].advertsCount + ":",
          user.advert_count
        )}
        {generateInfo(language[currentLang].sphere, getCategory())}
        {generateInfo(language[currentLang].address + ":", user.address)}
        {generateInfo(
          language[currentLang].accType + ":",
          language[currentLang].bussinessAcc,
          1
        )}
        {generateInfo(language[currentLang].inn + ":", user.inn)}
        {generateInfo(language[currentLang].workdays + ":", user.work_time)}
        {generateInfo(language[currentLang].website + ":", user.website)}
        {generateInfo(language[currentLang].email + ":", user.email)}
        {generateInfo(language[currentLang].phone + ":", user.phone)}
        {generateInfo(language[currentLang].addDescr + ":", user.description)}
      </>
    );
  };
  const Simple = () => {
    return (
      <>
        <Between margin={0}>
          <TouchableScale activeScale={0.96}>
            <Logo>
              <Profile color={"white"} width={40} height={40} />
            </Logo>
          </TouchableScale>
          <Type>
            <Between>
              <Title>{user.name}</Title>
              <View>
                <TouchableScale
                  activeScale={0.96}
                  onPress={() => navigation.navigate("EditUser")}
                >
                  <EditButton>
                    <Edit />
                  </EditButton>
                </TouchableScale>
              </View>
            </Between>
            <TypeInfo type={global.colors.orange}>
              <TypeInfoText type={"white"}>
                {language[currentLang].simple}
              </TypeInfoText>
            </TypeInfo>
          </Type>
        </Between>
        {generateInfo(
          language[currentLang].inSystem + ":",
          user.created_at.slice(0, 10)
        )}
        {generateInfo(
          language[currentLang].advertsCount + ":",
          user.advert_count
        )}
        {generateInfo(language[currentLang].birthday + ":", user.birthday)}
        {generateInfo(
          language[currentLang].sex + ":",
          user.sex ? language[currentLang].male : language[currentLang].female
        )}
        {generateInfo(language[currentLang].region + ":", user.country)}
        {generateInfo(language[currentLang].email + ":", user.email)}
        {generateInfo(language[currentLang].phone + ":", user.phone)}

        <TouchableScale
          style={{ width: "100%", marginTop: 10 }}
          activeScale={0.96}
        >
          {/* <TypeInfo type={"#999999"}>
            <TypeInfoText type={"white"}>Изменить данные</TypeInfoText>
          </TypeInfo> */}
        </TouchableScale>
      </>
    );
  };
  if (!user) return <LoadingScreen />;
  const type = parseInt(user.is_business);
  const navigateToPath = (path) => {
    navigation.navigate(path);
  };
  return (
    <ScrollView
      contentContainerStyle={{
        backgroundColor: "#f5f5f5",
        // flex: 1,
      }}
    >
      <TopWrap>
        <Between>
          <View>
            <Balance>{language[currentLang].balance}:</Balance>
            <Amount>{user.balance.amount} UZS</Amount>
          </View>
          <View>
            <TouchableScale
              activeScale={0.96}
              onPress={() => navigation.navigate("Balance")}
            >
              <AddBalance type={global.colors.orange}>
                <TypeInfoText type={"white"}>
                  {language[currentLang].addMoney}
                </TypeInfoText>
              </AddBalance>
            </TouchableScale>
          </View>
        </Between>
      </TopWrap>
      <Wrap>{type ? Bussiness() : Simple()}</Wrap>
      <Wrap>
        <GenerateButton
          icon={<Advert />}
          title={language[currentLang].adverts}
          count={user.advert_count}
          method={() => navigateToPath("MyAdvert")}
        />
        <GenerateButton
          icon={<Transaction />}
          title={language[currentLang].operations}
          count={""}
          method={() => navigateToPath("Transaction")}
        />
        <GenerateButton
          icon={<Message />}
          title={language[currentLang].messages}
          count={""}
          method={() => navigateToPath("Messages")}
        />
        <GenerateButton
          icon={<Favorite />}
          title={language[currentLang].favorites}
          count={""}
          method={() => navigateToPath("Favorite")}
        />
        <GenerateButton
          icon={<Setting />}
          title={language[currentLang].settings}
          count={""}
          method={() => navigateToPath("Setting")}
        />
      </Wrap>
    </ScrollView>
  );
};
const EditButton = styled.View`
  width: 40px;
  height: 40px;
  align-items: center;
  justify-content: center;
  border-radius: 10px;
  background: #f5f5f5;
`;
const AddBalance = styled.View`
  height: 40px;
  align-items: center;
  border-radius: 10px;
  justify-content: center;
  background: ${({ type }) => type};
  width: 100%;
  padding: 10px 15px;
`;
const Balance = styled.Text`
  font-size: 14px;
  color: white;
  font-family: ${global.fonts.medium};
  line-height: 18px;
`;
const Amount = styled.Text`
  font-size: 14px;
  color: white;
  font-family: ${global.fonts.regular};
  line-height: 18px;
`;
const TopWrap = styled.View`
  background: #333;
  padding: 10px;
`;
const Row = styled.View`
  flex-direction: row;
  align-items: center;
  /* flex: 1; */
`;
const AdvertButton = styled.View`
  flex-direction: row;
  justify-content: space-between;
  padding: 10px 15px;
  height: 40px;
  shadow-color: #000;
  shadow-opacity: 0.12;
  elevation: 3;
  background: white;
  width: 100%;
  border-radius: 10px;
`;
const Information = styled.Text`
  font-size: 14px;
  color: ${({ color }) => (color ? "#333" : "#999")};
  font-family: ${global.fonts.regular};
`;
const SaveButton = styled.Text`
  background: ${global.colors.orange};
  text-align: center;
  line-height: 40px;
  color: white;
  height: 40px;
  border-radius: 10px;
  margin-bottom: 5px;
  margin-top: 10px;
`;
const Title = styled.Text`
  color: #333333;
  font-size: 14px;
  font-family: ${global.fonts.medium};
  line-height: 60px;
  height: 50px;
  margin-bottom: 10px;
  text-align: center;
`;
const Icon = styled.View`
  margin-left: ${({ left }) => left + "px"};
  margin-right: ${({ right }) => right + "px"};
  align-items: center;
  justify-content: center;
`;
const TypeInfoText = styled.Text`
  font-family: ${global.fonts.regular};
  font-size: 14px;
  color: ${({ type }) => type};
`;
const TypeInfo = styled.View`
  height: 40px;
  align-items: center;
  border-radius: 10px;
  /* box-shadow: 0px 0px 2px rgba(0, 0, 0, 0.12); */
  /* shadow-offset: {width: 0, height: 2px}; */
  /* shadow-radius: 2; */
  shadow-color: #000;
  shadow-opacity: 0.12;
  elevation: 3;
  justify-content: center;
  background: ${({ type }) => type};
  margin-bottom: 10px;
  width: 100%;
`;
const Type = styled.View`
  flex: 1;
  padding-left: 10px;
`;
const LogoText = styled.Text`
  color: #ffffff;
  font-size: 16px;
  font-family: ${global.fonts.medium};
`;
const Logo = styled.View`
  align-items: center;
  justify-content: center;
  background: #aeaeae;
  height: 90px;
  width: 90px;
  border-radius: 10px;
`;
const CoverImage = styled.Image`
  width: 100%;
  height: 170px;
  border-radius: 10px;
`;
const Wrap = styled.View`
  margin: 5px 0 5px;
  background: white;
  padding: 10px;
  flex: 1;
  shadow-color: #000;
  shadow-opacity: 0.12;
  elevation: 3;
  align-items: center;
`;
// const Title = styled.Text`
//   font-family: ${global.fonts.medium};
//   font-size: 24px;
//   line-height: 36px;
//   color: white;
//   padding-bottom: 20px;
// `;
const PaddingHorizontal = styled.View`
  padding: 0 5px;
`;
const Between = styled.View`
  flex-direction: row;
  align-items: center;
  width: 100%;
  justify-content: space-between;
  margin-top: ${({ margin }) => (margin ? margin + "px" : 0)};
`;
export default ProfileScreen;
