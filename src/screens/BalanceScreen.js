import React, { useState, useEffect, useContext } from "react";
import {
  View,
  ScrollView,
  TextInput,
  Text,
  Image,
  TouchableOpacity,
  StatusBar,
} from "react-native";
import styled from "styled-components";
import global from "../../resources/global";
import Coins from "../../assets/coins.svg";
import TouchableScale from "react-native-touchable-scale";
import Context from "../components/Context";
import language from "../constants/language";
const generateButton = (type, disabled, title, method) => {
  return (
    <Flexer>
      <TouchableScale disabled={disabled} onPress={() => method()}>
        <BottomButton type={type}>
          <ButtonText>{title}</ButtonText>
        </BottomButton>
      </TouchableScale>
    </Flexer>
  );
};
const BalanceScreen = ({ navigation }) => {
  const { apiService, currentLang, user, token } = useContext(Context);

  const [promotions, setPromotions] = useState([
    {
      id: 1,
      price: 5000,
    },
    {
      id: 2,
      price: 10000,
    },
    {
      id: 3,
      price: 20000,
    },
    {
      id: 4,
      price: 30000,
    },
    {
      id: 5,
      price: 40000,
    },
    {
      id: 6,
      price: 50000,
    },
    {
      id: 7,
      price: 60000,
    },
    {
      id: 8,
      price: 70000,
    },
  ]);
  // useEffect(() => {}, []);
  const [chosen, setChosen] = useState(-1);
  const makeChoice = (value) => {
    const setValue = value === chosen ? -1 : value;
    setChosen(setValue);
  };
  const generateBalance = (promotion, method) => {
    return (
      <TouchableScale
        style={{ marginBottom: 10, width: "100%" }}
        onPress={() => method()}
        activeScale={0.96}
      >
        <Promotion active={promotion.id === chosen ? 1 : 0}>
          <PromoTop>
            <Icon left={0} right={0}>
              <Coins color={"white"} />
            </Icon>
            <RightAlign>
              <Title active={promotion.id === chosen ? 1 : 0}>
                {promotion.price} UZS
              </Title>
            </RightAlign>
          </PromoTop>
        </Promotion>
      </TouchableScale>
    );
  };
  return (
    <>
      <StatusBar backgroundColor={`white`} barStyle={`dark-content`} />
      <TopWrap>
        <Between>
          <View>
            <Balance>{language[currentLang].balance}:</Balance>
          </View>
          <View>
            <Amount>{user.balance.amount} UZS</Amount>
          </View>
        </Between>
      </TopWrap>
      <ScrollView
        contentContainerStyle={{
          backgroundColor: "#f5f5f5",
        }}
      >
        <Wrap>
          {promotions.map((promo, key) => {
            return (
              <React.Fragment key={key}>
                {generateBalance(promo, () => makeChoice(promo.id))}
              </React.Fragment>
            );
          })}
        </Wrap>
      </ScrollView>
      <Wrap>
        <Row>
          {generateButton(
            chosen < 0,
            chosen < 0,
            language[currentLang].choose,
            () =>
              navigation.navigate("Payment", {
                price: promotions.find((promo) => promo.id == chosen).price,
              })
          )}
          <View width={10} />
        </Row>
      </Wrap>
    </>
  );
};
const PromoTop = styled.View`
  padding: 10px 0;
  flex-direction: row;
`;
const RightAlign = styled.View`
  align-items: flex-end;
  justify-content: center;
  flex: 1;
`;
const Promotion = styled.View`
  width: 100%;
  background: white;
  border-radius: 10px;
  shadow-color: #000;
  /* shadow-offset: {width: 0, height: 2px}; */
  shadow-opacity: 0.12;
  /* shadow-radius: 2; */
  elevation: ${({ active }) => (active ? 10 : 3)};
  border-width: 2px;
  border-color: ${({ active }) => (active ? global.colors.orange : "white")};
`;
const AddBalance = styled.View`
  height: 40px;
  align-items: center;
  border-radius: 10px;
  justify-content: center;
  background: ${({ type }) => type};
  width: 100%;
  padding: 10px 15px;
`;
const Balance = styled.Text`
  font-size: 14px;
  color: #333333;
  font-family: ${global.fonts.medium};
  line-height: 18px;
`;
const Amount = styled.Text`
  font-size: 14px;
  color: #333333;
  font-family: ${global.fonts.regular};
  line-height: 18px;
`;
const TopWrap = styled.View`
  background: white;
  padding: 10px;
`;
const TypeInfoText = styled.Text`
  font-family: ${global.fonts.regular};
  font-size: 14px;
  color: ${({ type }) => type};
`;
const Flexer = styled.View`
  flex: 1;
`;
const ButtonText = styled.Text`
  font-size: 14px;
  color: white;
  font-family: ${global.fonts.regular};
`;
const BottomButton = styled.View`
  width: 100%;
  background: ${({ type }) => (type ? "#999999" : global.colors.orange)};
  border-radius: 10px;
  height: 40px;
  flex-direction: row;
  align-items: center;
  justify-content: center;
`;
const Row = styled.View`
  flex-direction: row;
  align-items: center;
`;
const Title = styled.Text`
  color: ${({ active }) => (active ? "#333" : "#999")};
  font-size: 14px;
  font-family: ${global.fonts.medium};
  padding-right: 10px;
`;
const Icon = styled.View`
  align-items: center;
  justify-content: center;
  width: 80px;
`;

const Wrap = styled.View`
  margin: 5px 0 5px;
  background: white;
  padding: 10px;
  width: 100%;
  box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.12);
`;
const Between = styled.View`
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
`;
export default BalanceScreen;
