import React, { useContext, useEffect, useState } from "react";
import { Image, ScrollView, View, Text } from "react-native";
import Swiper from "react-native-swiper";
import TouchableScale from "react-native-touchable-scale";
import styled from "styled-components";
import Arrow from "../../assets/arrow.svg";
import SliderLeft from "../../assets/slider-left.svg";
import SliderRight from "../../assets/slider-right.svg";
import global from "../../resources/global";
import AddButton from "../components/AddButton";
import AdvertHorizontal from "../components/AdvertHorizontal";
import Context from "../components/Context";
import { CategoryLoader, SliderLoader } from "../components/Preloaders";
import language from "../constants/language";

const fourItems = global.strings.width / 4;
const categorySize = fourItems > 80 ? 80 : fourItems;

const HomeScreen = ({ navigation }) => {
  const {
    apiService,
    token,
    phone,
    currentLang,
    setCurrentLang,
    categories,
    setCategories,
    user,
  } = useContext(Context);
  const [slider, setSlider] = useState([]);
  const [adverts, setAdvert] = useState([]);
  useEffect(() => {
    getSlider();
    getCategories();
    getAdverts();
  }, []);
  function getCategories() {
    apiService.getResources("/category").then((value) => {
      setCategories(value);
    });
  }
  function getSlider() {
    apiService.getResources("/slider").then((value) => {
      setSlider(value.data);
    });
  }
  function getAdverts() {
    apiService.getResources("/main").then((value) => {
      setAdvert(value.data);
    });
  }
  return (
    <>
      <ScrollView style={{ backgroundColor: "#F0F0F0" }}>
        {slider.length > 0 ? (
          <Swiper
            showsButtons={true}
            nextButton={<SliderRight />}
            prevButton={<SliderLeft />}
            activeDotColor={"#FFFFFF"}
            dotColor={"#80807F"}
            buttonColor={"#FFFFFF"}
            height={170}
            width={global.strings.width}
            // containerStyle={{ height: 170 }}
            // style={{ height: 170 }}
            // loadMinimal
            // loadMinimalSize={1}
          >
            {slider.map((slide, key) => (
              <View key={key}>
                <Image
                  resizeMode={"cover"}
                  style={{ width: "100%", height: 170 }}
                  source={{ uri: slide.image }}
                />
              </View>
            ))}
          </Swiper>
        ) : (
          <SliderLoader />
        )}
        <WhiteWrap>
          <Between>
            <Title>{language[currentLang].category}</Title>
            <TouchableScale onPress={() => navigation.jumpTo("Category")}>
              <GoButton>
                <ButtonText>{language[currentLang].all}</ButtonText>
                <Arrow />
              </GoButton>
            </TouchableScale>
          </Between>
          <ScrollView
            horizontal={true}
            contentContainerStyle={{ height: 110 }}
            showsHorizontalScrollIndicator={false}
          >
            {categories.length > 0 ? (
              categories.map((category, key) => {
                if (!category.parent_id) {
                  return (
                    <TouchableScale
                      key={key}
                      onPress={() =>
                        navigation.navigate("CategoryChoice", {
                          headerTitle:
                            currentLang == 1
                              ? category.name_ru
                              : currentLang == 2
                              ? category.name_uz
                              : category.name_kr,
                          categoryId: category.id,
                        })
                      }
                      activeScale={0.94}
                    >
                      <Category>
                        <CategoryTop color={category.color}>
                          <CategoryImage source={{ uri: category.image }} />
                        </CategoryTop>
                        <CategoryTitle>
                          {currentLang == 1 && category.name_ru}
                          {currentLang == 2 && category.name_uz}
                          {currentLang == 3 && category.name_kr}
                        </CategoryTitle>
                      </Category>
                    </TouchableScale>
                  );
                }
              })
            ) : (
              <Between>
                <CategoryLoader />
              </Between>
            )}
          </ScrollView>
        </WhiteWrap>
        <WhiteWrap>
          <Title>{language[currentLang].topAdverts}</Title>
          {adverts.length ? (
            adverts.map((advert, key) => (
              <AdvertHorizontal
                navigation={navigation}
                key={key}
                advert={advert}
              />
            ))
          ) : (
            <Image
              source={global.images.loading}
              style={{ height: 150, marginTop: 60, alignSelf: "center" }}
              resizeMode="contain"
            />
          )}
          <View height={50} />
        </WhiteWrap>
      </ScrollView>
      <AddButton />
    </>
  );
};
const categorySizePixel = categorySize + "px";
const Title = styled.Text`
  color: black;
  font-size: 18px;
  line-height: 22px;
  padding: 0 10px;
  font-family: ${global.fonts.medium};
  color: ${global.colors.secondary};
`;
const WhiteWrap = styled.View`
  margin-top: 5px;
  background: ${global.colors.background};
  shadow-color: #000;
  shadow-opacity: 0.12;
  elevation: 3;
  padding: 10px 0;
`;
const Category = styled.View`
  margin-top: 10px;
  width: ${categorySizePixel};
  margin-left: 10px;
  margin-right: 10px;
`;
const CategoryTop = styled.View`
  width: ${categorySizePixel};
  height: ${categorySizePixel};
  background: ${({ color }) => "#" + color};
  align-items: flex-end;
  border-radius: 10px;
  overflow: hidden;
`;
const CategoryImage = styled.Image`
  height: ${categorySizePixel};
  width: 60px;
`;

const CategoryTitle = styled.Text`
  font-size: 10px;
  font-family: ${global.fonts.regular};
  line-height: 14px;
  margin-top: 5px;
  text-align: center;
  width: ${categorySizePixel};
`;
const Between = styled.View`
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
`;
const GoButton = styled.View`
  flex-direction: row;
  align-items: center;
  justify-content: center;
  background: ${global.colors.orange};
  border-radius: 10px;
  padding: 0 10px;
  height: 20px;
  margin-right: 10px;
`;
const ButtonText = styled.Text`
  color: white;
  font-family: ${global.fonts.medium};
  font-size: 10px;
  margin-right: 5px;
`;
export default HomeScreen;
