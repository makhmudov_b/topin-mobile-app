import React, { useState, useEffect, useContext } from "react";
import {
  View,
  ScrollView,
  TextInput,
  Text,
  Image,
  TouchableOpacity,
  StatusBar,
} from "react-native";
import styled from "styled-components";
import global from "../../resources/global";
import AdvertHorizontalFavorite from "../components/AdvertHorizontalFavorite";
import language from "../constants/language";
import Context from "../components/Context";
import Empty from "../../assets/no-favorites.svg";
import LoadingScreen from "./LoadingScreen";

const FavoriteScreen = ({ navigation, route }) => {
  const { apiService, token, currentLang } = useContext(Context);
  const [favorites, setFavorites] = useState([]);
  const [loading, setLoading] = useState(true);
  useEffect(() => {
    apiService
      .getResources("/favorite", token)
      .then((value) => {
        setFavorites(value.data);
        setLoading(false);
      })
      .catch((e) => setLoading(false));
  }, []);
  const removeItem = (id) => {
    const body = {
      advert_id: id,
    };
    apiService.patchEvent("/favorite/remove", token, body).then((value) => {
      if (value.statusCode == 200) {
        setFavorites(favorites.filter((favorite) => favorite.advert_id != id));
      }
    });
  };
  if (loading) return <LoadingScreen />;
  return (
    <>
      <StatusBar backgroundColor={"white"} barStyle={`dark-content`} />
      <ScrollView
        contentContainerStyle={{
          backgroundColor: "#f5f5f5",
          // flex: 1,
        }}
      >
        <Wrap>
          {/* <MarginBottom>
            <Between>
              <View>
                <Title>Результаты</Title>
              </View>
              <View>
                <TouchableScale activeScale={0.96}>
                  <Dropdown>
                    <DropdownTitle>Сначало новые</DropdownTitle>
                    <View>
                      <Arrow />
                    </View>
                  </Dropdown>
                </TouchableScale>
              </View>
            </Between>
          </MarginBottom> */}
          <MarginBottom>
            <FullWidth>
              {favorites.map((favorite, key) => (
                <AdvertHorizontalFavorite
                  navigation={navigation}
                  key={`${key} ${favorite.id}`}
                  favorite={favorite}
                  remove={removeItem}
                />
              ))}
              {!favorites.length && (
                <View>
                  <Empty />
                  <DropdownTitle>
                    {language[currentLang].notExist}
                  </DropdownTitle>
                </View>
              )}
            </FullWidth>
          </MarginBottom>
        </Wrap>
      </ScrollView>
    </>
  );
};
const MarginBottom = styled.View`
  margin-bottom: 10px;
  width: 100%;
`;
const FullWidth = styled.View`
  width: 100%;
  align-items: center;
`;
const WhiteWrap = styled.View`
  margin-top: 5px;
  width: 100%;
  background: ${global.colors.background};
  box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.12);
  padding: 10px 0;
`;
const DropdownTitle = styled.Text`
  color: #333333;
  font-size: 14px;
  text-align: center;
`;
const Dropdown = styled.View`
  background: #efefef;
  border-radius: 10px;
  padding: 6px 10px;
  flex-direction: row;
  align-items: center;
`;
const Wrap = styled.View`
  margin: 5px 0 5px;
  background: white;
  padding: 10px;
  flex: 1;
  shadow-color: #000;
  shadow-opacity: 0.12;
  elevation: 3;
  align-items: center;
`;
const Title = styled.Text`
  font-family: ${global.fonts.medium};
  font-size: 14px;
  color: #333;
  /* padding-bottom: 10px; */
`;
const Between = styled.View`
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  width: 100%;
`;
export default FavoriteScreen;
