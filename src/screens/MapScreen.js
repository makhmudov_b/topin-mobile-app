import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  TouchableOpacity,
  TextInput,
  View,
  StatusBar,
  KeyboardAvoidingView,
} from "react-native";
import MapView, { Marker } from "react-native-maps";
import global from "../../resources/global";
// import AdvertHorizontal from "../components/AdvertHorizontal";
import AdvertHorizontal from "../components/AdvertHorizontal";
import styled from "styled-components";
import TouchableScale from "react-native-touchable-scale";
import Close from "../../assets/header-close.svg";
import MapMarker from "../../assets/map-marker.svg";
import Arrow from "../../assets/arrow-left.svg";
import Filter from "../../assets/filter.svg";
import Constants from "expo-constants";
import language from "../constants/language";
import Context from "../components/Context";

export default class MapScreen extends Component {
  static contextType = Context;
  constructor(props) {
    super(props);
    this.state = {
      region: {
        latitude: 41.26465,
        longitude: 69.21627,
        latitudeDelta: 0.04,
        longitudeDelta: 0.05,
      },
      adverts: props.route.params ? props.route.params.adverts : [],
      chosen: -1,
      loading: true,
    };
  }
  onRegionChange(region) {
    this.setState({
      region: {
        latitude: region.latitude,
        longitude: region.longitude,
      },
    });
  }
  setCamerView(markerPosition, key) {
    this.setState({
      region: {
        latitude: markerPosition.latitude,
        longitude: markerPosition.longitude,
        latitudeDelta: this.state.region.latitudeDelta,
        longitudeDelta: this.state.region.longitudeDelta,
      },
      chosen: key,
      show: true,
    });
  }
  render() {
    // if (this.state.loading) {
    //   return (
    //     <View
    //       style={{ flex: 1, alignItems: "center", justifyContent: "center" }}
    //     >
    //       <Text>Loading</Text>
    //     </View>
    //   );
    // }
    const { navigation } = this.props;
    return (
      <>
        <StatusBar backgroundColor={`white`} barStyle={`dark-content`} />
        <Container>
          <Between>
            <TouchableScale onPress={() => navigation.goBack()}>
              <View>
                <Arrow />
              </View>
            </TouchableScale>
            {/* <TouchableScale onPress={() => navigation.navigate("Filter")}>
              <View>
                <FilterButton>
                  <Filter style={{ marginRight: 10 }} />
                  <FilterText>
                    {language[this.context.currentLang].filter}
                  </FilterText>
                </FilterButton>
              </View>
            </TouchableScale> */}
          </Between>
          {true && (
            <MapView
              initialRegion={{
                latitude: 41.26465,
                longitude: 69.21627,
                latitudeDelta: 0.04,
                longitudeDelta: 0.05,
              }}
              onRegionChangeComplete={(region) => this.onRegionChange(region)}
              style={styles.mapStyle}
            >
              {this.state.adverts.map((marker, key) => {
                if (marker.lat && marker.long) {
                  return (
                    <Marker
                      coordinate={{
                        latitude: Number(marker.lat),
                        longitude: Number(marker.long),
                      }}
                      key={key}
                      onPress={() => this.setCamerView(marker, key)}
                    >
                      <Relative>
                        <MarkerItem>
                          <MapMarker />
                        </MarkerItem>
                        <MarkerWrap>
                          {this.state.chosen === key && (
                            <Relative>
                              <Price>
                                <PriceText>{marker.price} UZS</PriceText>
                              </Price>
                              <AdvertImage
                                resizeMode={`cover`}
                                source={{ uri: marker.image[0] }}
                              />
                            </Relative>
                          )}
                          <MarkerText>{marker.name.slice(0, 20)}...</MarkerText>
                        </MarkerWrap>
                      </Relative>
                    </Marker>
                  );
                }
              })}
            </MapView>
          )}
          {this.state.chosen >= 0 && (
            <Bottom>
              <Relative>
                <CloseButton>
                  <TouchableScale onPress={() => this.setState({ chosen: -1 })}>
                    <Close />
                  </TouchableScale>
                </CloseButton>
                {
                  <AdvertHorizontal
                    advert={this.state.adverts[this.state.chosen]}
                  />
                }
              </Relative>
            </Bottom>
          )}
        </Container>
      </>
    );
  }
}
const Between = styled.View`
  flex-direction: row;
  align-items: center;
  width: 100%;
  justify-content: space-between;
  padding: 0 10px;
  position: absolute;
  z-index: 20;
  top: ${Constants.statusBarHeight - 10 + "px"};
`;
const PriceText = styled.Text`
  font-family: ${global.fonts.medium};
  font-size: 10px;
  color: white;
  text-align: center;
`;
const Price = styled.View`
  background: ${global.colors.orange};
  position: absolute;
  top: -12px;
  z-index: 10;
  padding: 5px;
  border-radius: 5px;
  flex-direction: row;
  align-items: center;
  justify-content: center;
`;
const FilterButton = styled.View`
  background: ${global.colors.orange};
  padding: 0 10px;
  border-radius: 10px;
  flex-direction: row;
  align-items: center;
  height: 40px;
  justify-content: center;
`;
const FilterText = styled.Text`
  font-family: ${global.fonts.medium};
  font-size: 14px;
  color: white;
  text-align: center;
`;
const AdvertImage = styled.Image`
  width: 100%;
  height: 85px;
  border-radius: 10px;
  margin-bottom: 5px;
`;
const MarkerItem = styled.View`
  position: absolute;
  bottom: 2px;
  left: 40%;
`;
const MarkerText = styled.Text`
  font-size: 10px;
  line-height: 14px;
  color: white;
  font-family: ${global.fonts.regular};
  text-align: center;
`;
const MarkerWrap = styled.View`
  background: ${global.colors.main};
  padding: 5px;
  /* position: relative; */
  border-radius: 10px;
  z-index: 10;
  margin-top: 10px;
  margin-bottom: 10px;
  width: 150px;
`;
const CloseButton = styled.View`
  align-self: flex-end;
  margin-right: 10px;
`;
const Relative = styled.View`
  position: relative;
  align-items: center;
  justify-content: center;
`;
const WhiteWrap = styled.View`
  padding: 0 10px;
  flex: 1;
`;
const Bottom = styled.View`
  flex: 1;
  position: absolute;
  bottom: 20px;
  width: ${global.strings.width + "px"};
  z-index: 20;
`;
const Container = styled.View`
  flex: 1;
`;
const styles = StyleSheet.create({
  mapStyle: {
    flex: 1,
    // position: "absolute",
    // width: global.strings.width,
    // height: global.strings.height,
  },
});
