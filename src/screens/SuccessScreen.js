import React, { useState, useEffect } from "react";
import {
  View,
  ScrollView,
  TextInput,
  Text,
  Image,
  TouchableOpacity,
  StatusBar,
} from "react-native";
import styled from "styled-components";
import global from "../../resources/global";
import Success from "../../assets/success.svg";

const SuccessScreen = () => {
  return (
    <>
      <StatusBar backgroundColor={"white"} barStyle={`dark-content`} />
      <Wrap>
        <Success />
      </Wrap>
    </>
  );
};
const Wrap = styled.View`
  margin: 5px 0 5px;
  flex: 1;
  background: white;
  align-items: center;
  justify-content: center;
`;
export default SuccessScreen;
