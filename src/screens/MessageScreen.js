import React, { useState, useEffect, useContext } from "react";
import {
  View,
  ScrollView,
  TextInput,
  Text,
  Image,
  TouchableOpacity,
  StatusBar,
  Keyboard,
} from "react-native";
import styled from "styled-components";
import global from "../../resources/global";
import Arrow from "../../assets/arrow-down.svg";
import SendMessage from "../../assets/send.svg";
import TouchableScale from "react-native-touchable-scale";
import AdRight from "../../assets/advert-right.svg";
import Context from "../components/Context";
import LoadingScreen from "./LoadingScreen";
import language from "../constants/language";

const MessageScreen = ({ navigation, route }) => {
  const [loading, setLoading] = useState(true);
  const { advertId } = route.params;
  const { apiService, user, token, currentLang } = useContext(Context);
  const [messages, setMessages] = useState([]);
  const [innerAdvert, setAdvert] = useState(null);
  const [body, setBody] = useState("");
  useEffect(() => {
    getMessages();
  }, []);
  const getMessages = () => {
    apiService
      .getResources("/messages/advert/" + advertId, token)
      .then((value) => {
        setMessages(value.data.messages);
        setAdvert(value.data.advert);
        setLoading(false);
      });
  };
  const sendMess = () => {
    if (body.length > 3) {
      Keyboard.dismiss();
      const receiver_id =
        innerAdvert.user_id == user.id
          ? messages.filter((mess) => mess.user_id != user.id)[0].user_id
          : innerAdvert.user_id;
      const formBody = {
        advert_id: innerAdvert.id,
        receiver_id: receiver_id,
        body: body,
      };
      setLoading(true);
      apiService
        .postEvent("/send/message", token, formBody)
        .then((value) => {
          getMessages();
          setBody("");
          setLoading(false);
        })
        .catch((e) => setLoading(false));
    }
  };
  const generateUserMessage = (title, date) => {
    return (
      <MarginHorizontal>
        <Between>
          <UserMessage>
            <UserText>{title}</UserText>
            <UserDate>{date}</UserDate>
          </UserMessage>
        </Between>
      </MarginHorizontal>
    );
  };
  const generateOwnMessage = (title, date) => {
    return (
      <MarginHorizontal>
        <Between>
          <View />
          <OwnMessage>
            <OwnText>{title}</OwnText>
            <OwnDate>{date}</OwnDate>
          </OwnMessage>
        </Between>
      </MarginHorizontal>
    );
  };
  if (loading) return <LoadingScreen />;
  return (
    <>
      <StatusBar backgroundColor={"white"} barStyle={`dark-content`} />
      <TouchableScale
        activeScale={0.97}
        onPress={() =>
          navigation.navigate("ShowAdvert", { advertId: innerAdvert.id })
        }
      >
        <Advert>
          <AdvertLeft>
            <Image
              source={{ uri: innerAdvert.image[0] }}
              style={{ width: "100%", height: "100%" }}
              resizeMode="cover"
            />
            <AbsoluteRight>
              <AdRight />
            </AbsoluteRight>
          </AdvertLeft>
          <AdverRight>
            <AdvertText>{innerAdvert.name}</AdvertText>
            <AdvertBottom>
              <Region>
                {currentLang == 1 && innerAdvert.region.name_ru}
                {currentLang == 2 && innerAdvert.region.name_uz}
                {currentLang == 3 && innerAdvert.region.name_kr}
              </Region>
              <Price>{innerAdvert.price} UZS</Price>
            </AdvertBottom>
          </AdverRight>
        </Advert>
      </TouchableScale>
      <ScrollView
        contentContainerStyle={{
          backgroundColor: "#f5f5f5",
        }}
        keyboardShouldPersistTaps="handled"
      >
        {messages.map((message, key) => {
          if (message.user_id != user.id) {
            return (
              <React.Fragment key={key}>
                {generateUserMessage(
                  message.body,
                  message.created_at.slice(0, 10)
                )}
              </React.Fragment>
            );
          } else {
            return (
              <React.Fragment key={key}>
                {generateOwnMessage(
                  message.body,
                  message.created_at.slice(0, 10)
                )}
              </React.Fragment>
            );
          }
        })}
        <View height={20} />
      </ScrollView>
      <Wrap>
        <Between>
          <MessageInput
            returnKeyType="send"
            value={body}
            spellCheck={false}
            onSubmitEditing={() => sendMess()}
            onChangeText={(text) => setBody(text)}
            placeholder={language[currentLang].write + "..."}
          />
          <TouchableScale onPress={() => sendMess()}>
            <SendWrap>
              <SendMessage />
            </SendWrap>
          </TouchableScale>
        </Between>
      </Wrap>
    </>
  );
};
const OwnDate = styled.Text`
  font-family: ${global.fonts.regular};
  color: white;
  font-size: 8px;
`;
const OwnText = styled.Text`
  font-size: 12px;
  font-family: ${global.fonts.regular};
  color: white;
  margin-right: 10px;
  max-width: ${global.strings.width / 1.3 + "px"};
`;
const OwnMessage = styled.View`
  margin-top: 10px;
  background: ${global.colors.orange};
  border-top-right-radius: 10px;
  border-top-left-radius: 10px;
  border-bottom-left-radius: 10px;
  shadow-color: #000;
  shadow-opacity: 0.12;
  elevation: 2;
  flex-direction: row;
  align-items: flex-end;
  padding: 5px 10px;
  flex-wrap: wrap;
`;
const MarginHorizontal = styled.View`
  margin: 0 10px;
`;
const UserDate = styled.Text`
  font-family: ${global.fonts.regular};
  color: #999999;
  font-size: 8px;
`;
const UserText = styled.Text`
  font-size: 12px;
  font-family: ${global.fonts.regular};
  color: #333333;
  margin-right: 10px;
  max-width: ${global.strings.width / 1.7 + "px"};
`;
const UserMessage = styled.View`
  margin-top: 10px;
  background: white;
  border-top-right-radius: 10px;
  border-top-left-radius: 10px;
  border-bottom-right-radius: 10px;
  shadow-color: #000;
  shadow-opacity: 0.12;
  elevation: 2;
  flex-direction: row;
  align-items: flex-end;
  padding: 5px 10px;
  flex-wrap: wrap;
`;
const SendWrap = styled.View`
  align-items: center;
  justify-content: center;
  height: 40px;
  width: 40px;
  background: #f5f5f5;
  border-radius: 10px;
`;
const MessageInput = styled.TextInput`
  font-size: 14px;
  color: #333;
  flex: 1;
  font-family: ${global.fonts.regular};
`;

const MessageImage = styled.View`
  width: 40px;
  height: 40px;
  align-items: center;
  justify-content: center;
  border-radius: 10px;
  background: #aeaeae;
  margin-right: 10px;
`;
const LightTitle = styled.Text`
  font-size: 12px;
  color: ${({ active }) => (active ? global.colors.orange : "#999999")};
`;
const Title = styled.Text`
  font-family: ${global.fonts.regular};
  font-size: 12px;
  color: #333;
  max-width: ${global.strings.width / 1.4 + "px"};
`;
const Count = styled.Text`
  font-family: ${global.fonts.medium};
  font-size: 12px;
  color: ${({ active }) => (active ? "white" : "#333")};
  background: ${({ active }) => (active ? global.colors.orange : "white")};
  width: 20px;
  height: 20px;
  text-align: center;
  line-height: 20px;
  border-radius: 10px;
  margin-top: 5px;
`;
const Transaction = styled.View`
  width: 100%;
  padding: 10px;
  min-height: 60px;
  /* shadow-offset: {width: 0, height: 2px}; */
  /* shadow-radius: 2; */
  shadow-color: #000;
  shadow-opacity: 0.12;
  elevation: 2;
  background: white;
  border-radius: 10px;
`;
const Wrap = styled.View`
  background: white;
  padding: 8px 20px;
  box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.12);
`;
const Row = styled.View`
  flex-direction: row;
  align-items: center;
`;
const Between = styled.View`
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  padding: ${({ padding }) => (padding ? padding + "px" : 0)} 0;
`;
const Advert = styled.View`
  width: 100%;
  height: 70px;
  overflow: hidden;
  background-color: white;
  flex-direction: row;
  justify-content: space-between;
`;
const AdvertLeft = styled.View`
  flex-basis: 100px;
  position: relative;
  background-color: whitesmoke;
`;
const AbsoluteRight = styled.View`
  width: 100%;
  height: 100%;
  position: absolute;
  top: 0;
  right: 0;
  /* right: 0; */
  /* bottom: 0; */
  align-items: flex-start;
`;
const AdverRight = styled.View`
  padding: 10px;
  flex: 1;
  border: 1px solid #f1f1f1;
`;
const AdvertText = styled.Text`
  color: ${global.colors.secondary};
  font-size: 12px;
  line-height: 18px;
  font-family: ${global.fonts.regular};
`;
const AdvertBottom = styled.View`
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
`;
const Region = styled.Text`
  color: #999999;
  font-size: 10px;
  font-family: ${global.fonts.regular};
`;
const Price = styled.Text`
  color: ${global.colors.secondary};
  font-size: 12px;
  font-family: ${global.fonts.medium};
`;
export default MessageScreen;
