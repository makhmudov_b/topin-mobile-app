import React, { useState, useEffect, useContext } from "react";
import {
  View,
  ScrollView,
  TextInput,
  Text,
  Image,
  TouchableOpacity,
  StatusBar,
} from "react-native";
import styled from "styled-components";
import global from "../../resources/global";
import TouchableScale from "react-native-touchable-scale";
import AdvertHorizontalButtons from "../components/AdvertHorizontalButtons";
import Context from "../components/Context";
import language from "../constants/language";
import NoAdvert from "../../assets/no-advert.svg";

const MyAdvertScreen = ({ navigation, route }) => {
  const { apiService, token, phone, currentLang, categories } = useContext(
    Context
  );
  const [type, setTypes] = useState(1);
  const [loading, setLoading] = useState(1);
  const [adverts, setAdvert] = useState([]);
  useEffect(() => {
    getOwnAdverts(type);
  }, []);
  const setType = (value) => {
    setTypes(value);
    if (value != type) {
      getOwnAdverts(value);
    }
  };
  const getOwnAdverts = (value) => {
    setLoading(true);
    const body = {
      status: value == 1 ? "ACTIVE" : value == 2 ? "MODERATION" : "DISABLED",
    };
    apiService
      .getResources("/my/adverts?" + new URLSearchParams(body), token)
      .then((value) => {
        setAdvert(value.data);
        setLoading(false);
      })
      .catch((e) => setLoading(false));
  };
  const generateChoice = (title, value) => {
    return (
      <TouchableScale
        style={{ flex: 1 }}
        activeScale={0.96}
        onPress={() => setType(value)}
      >
        <Choice
          active={type == value ? global.colors.orange : "white"}
          //   type={"left"}
        >
          <ChoiceText active={type == value ? global.colors.orange : "#999999"}>
            {title}
          </ChoiceText>
        </Choice>
      </TouchableScale>
    );
  };
  const switchStatus = (id) => {
    setLoading(true);
    apiService.getResources("/switch/status/" + id, token).then((status) => {
      if (status) {
        const filtered = adverts.filter((item) => item.id != id);
        setAdvert(filtered);
        setLoading(false);
      }
    });
  };
  return (
    <>
      <StatusBar backgroundColor={"white"} barStyle={`dark-content`} />
      <ScrollView
        contentContainerStyle={{
          backgroundColor: "#f5f5f5",
          // flex: 1,
        }}
      >
        <TopWrap>
          <Between>
            {generateChoice(language[currentLang].active, 1)}
            {generateChoice(language[currentLang].moderation, 2)}
            {generateChoice(language[currentLang].disabled, 3)}
          </Between>
        </TopWrap>
        <Wrap>
          {/* <MarginBottom>
            <Between>
              <View>
                <Title>{language[currentLang].adverts}</Title>
              </View>
              <View></View>
            </Between>
          </MarginBottom> */}
          <MarginBottom>
            <FullWidth>
              {!loading ? (
                adverts.map((advert, index) => (
                  <AdvertHorizontalButtons
                    navigation={navigation}
                    key={`${index}${advert.id}`}
                    switcher={switchStatus}
                    advert={advert}
                  />
                ))
              ) : (
                <Image
                  source={global.images.loading}
                  style={{ height: 150, alignSelf: "center" }}
                  resizeMode="contain"
                />
              )}
              {!loading && !adverts.length && (
                <View>
                  <NoAdvert />
                  <DropdownTitle>
                    {language[currentLang].notExist}
                  </DropdownTitle>
                </View>
              )}
            </FullWidth>
          </MarginBottom>
        </Wrap>
      </ScrollView>
    </>
  );
};
const MarginBottom = styled.View`
  margin-bottom: 10px;
  width: 100%;
`;
const FullWidth = styled.View`
  width: 100%;
  align-items: center;
`;
const WhiteWrap = styled.View`
  margin-top: 5px;
  width: 100%;
  background: ${global.colors.background};
  box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.12);
  padding: 10px 0;
`;
const DropdownTitle = styled.Text`
  color: #333333;
  font-size: 14px;
  text-align: center;
`;
const Dropdown = styled.View`
  background: #efefef;
  border-radius: 10px;
  padding: 6px 10px;
  flex-direction: row;
  align-items: center;
`;
const TopWrap = styled.View`
  background: white;
  box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.12);
  justify-content: center;
  align-items: center;
`;
const Wrap = styled.View`
  margin: 5px 0 5px;
  background: white;
  padding: 10px;
  flex: 1;
  shadow-color: #000;
  shadow-opacity: 0.12;
  elevation: 3;
  align-items: center;
`;
const Title = styled.Text`
  font-family: ${global.fonts.medium};
  font-size: 14px;
  color: #333;
  /* padding-bottom: 10px; */
`;
const Between = styled.View`
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  width: 100%;
`;
const Choice = styled.View`
  /* flex: 1; */
  width: 100%;
  height: 50px;
  /* background:blue; */
  /* background: ${({ active }) => active}; */
  border-bottom-width: 1px;
  border-bottom-color: ${({ active }) => active};
  align-items: center;
  justify-content: center;
`;
const ChoiceText = styled.Text`
  font-family: ${global.fonts.regular};
  font-size: 14px;
  color: ${({ active }) => active};
`;
export default MyAdvertScreen;
