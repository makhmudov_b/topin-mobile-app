import React, { useState, useEffect, useContext } from "react";
import {
  View,
  ScrollView,
  TextInput,
  Text,
  Image,
  TouchableOpacity,
  StatusBar,
  Alert,
} from "react-native";
import styled from "styled-components";
import global from "../../resources/global";
import Favorite from "../../assets/in.svg";
import language from "../constants/language";
import Context from "../components/Context";
import BlackLoadingScreen from "./BlackLoadingScreen";
import TouchableScale from "react-native-touchable-scale";
import LoadingScreen from "./LoadingScreen";

const generateButton = (type, disabled, title, method) => {
  return (
    <Flexer>
      <TouchableScale disabled={disabled} onPress={() => method()}>
        <BottomButton type={type}>
          <ButtonText>{title}</ButtonText>
        </BottomButton>
      </TouchableScale>
    </Flexer>
  );
};
const PromoteScreen = ({ navigation, route }) => {
  const { apiService, currentLang, getUser, user, token } = useContext(Context);
  const { advertId } = route.params;
  const [promotions, setPromotions] = useState([]);
  const [loading, setLoading] = useState(true);
  useEffect(() => {
    getPromo();
  }, []);
  const getPromo = () => {
    apiService
      .getResources("/promos", token)
      .then((value) => {
        setPromotions(value.data);
        setLoading(false);
      })
      .catch((e) => setLoading(false));
  };
  const promoteAdvert = (id) => {
    const findPromo = promotions.find((promo) => promo.id == id);
    if (parseInt(user.balance.amount) <= parseInt(findPromo.price)) {
      Alert.alert("Не достаточно средств");
      return 0;
    }
    const body = {
      promo_id: findPromo.id,
    };
    setLoading(true);
    apiService
      .updateForm("/promote/advert/" + advertId, token, body)
      .then((value) => {
        getUser();
        navigation.replace("MyAdvert");
        setLoading(false);
      })
      .catch((e) => {
        setLoading(false);
        Alert.alert("Что-то пошло не так");
      });
  };
  // useEffect(() => {}, []);
  const [chosen, setChosen] = useState(-1);
  const makeChoice = (value) => {
    const setValue = value === chosen ? -1 : value;
    setChosen(setValue);
  };
  const generatePromotion = (promotion, method) => {
    return (
      <TouchableScale
        style={{ marginBottom: 20 }}
        onPress={() => method()}
        activeScale={0.96}
      >
        <Promotion>
          <PromoTop active={promotion.id === chosen ? 1 : 0}>
            <Icon left={0} right={0}>
              <Favorite color={"white"} width={50} />
            </Icon>
            <Centered>
              <Title active={promotion.id === chosen ? 1 : 0}>
                {currentLang == 1
                  ? promotion.name_ru
                  : currentLang == 2
                  ? promotion.name_uz
                  : promotion.name_kr}
              </Title>
              <Description active={promotion.id === chosen ? 1 : 0}>
                {currentLang == 1
                  ? promotion.description_ru
                  : currentLang == 2
                  ? promotion.description_uz
                  : promotion.description_kr}
              </Description>
            </Centered>
          </PromoTop>
          <Price active={promotion.id === chosen ? 1 : 0}>
            {promotion.price} UZS
          </Price>
        </Promotion>
      </TouchableScale>
    );
  };
  if (loading) return <LoadingScreen />;
  return (
    <>
      <StatusBar backgroundColor={`white`} barStyle={`dark-content`} />
      <TopWrap>
        <Between>
          <View>
            <Balance>{language[currentLang].balance}:</Balance>
            <Amount>{user.balance.amount} UZS</Amount>
          </View>
          <View>
            <TouchableScale
              activeScale={0.96}
              onPress={() => navigation.navigate("Balance")}
            >
              <AddBalance type={global.colors.orange}>
                <TypeInfoText type={"white"}>
                  {language[currentLang].addMoney}
                </TypeInfoText>
              </AddBalance>
            </TouchableScale>
          </View>
        </Between>
      </TopWrap>
      <ScrollView
        contentContainerStyle={{
          backgroundColor: "#f5f5f5",
        }}
      >
        <Wrap>
          {promotions.map((promo, key) => {
            return (
              <React.Fragment key={key}>
                {generatePromotion(promo, () => makeChoice(promo.id))}
              </React.Fragment>
            );
          })}
        </Wrap>
      </ScrollView>
      <Wrap>
        <Row>
          {/* {generateButton(1, false, "Пропустить")} */}
          {/* <View width={10} /> */}
          {generateButton(chosen < 0 ? 1 : 0, chosen < 0, "Выбрать", () =>
            promoteAdvert(chosen)
          )}
        </Row>
      </Wrap>
    </>
  );
};
const Price = styled.Text`
  background: ${({ active }) => (active ? global.colors.orange : "#999999")};
  color: white;
  text-align: center;
  font-size: 14px;
  font-family: ${global.fonts.medium};
  padding: 10px;
  border-bottom-left-radius: 10px;
  border-bottom-right-radius: 10px;
`;
const PromoTop = styled.View`
  /* height: 50px; */
  padding: 10px 0;
  flex-direction: row;
  border-top-left-radius: 10px;
  border-top-right-radius: 10px;
  background: ${({ active }) => (active ? global.colors.orange : "white")};
`;
const Centered = styled.View`
  /* align-items: center; */
  justify-content: center;
  flex: 1;
`;
const Description = styled.Text`
  color: #333333;
  font-size: 8px;
  font-family: ${global.fonts.regular};
  padding-top: 5px;
  color: ${({ active }) => (active ? "white" : "#333")};
`;
const Promotion = styled.View`
  width: 100%;
  background: white;
  border-radius: 10px;
  shadow-color: #000;
  shadow-opacity: 0.12;
  elevation: 3;
  /* shadow-radius: 2; */
  /* shadow-offset: {width: 0, height: 2px}; */
`;
const AddBalance = styled.View`
  height: 40px;
  align-items: center;
  border-radius: 10px;
  justify-content: center;
  background: ${({ type }) => type};
  width: 100%;
  padding: 10px 15px;
`;
const Balance = styled.Text`
  font-size: 14px;
  color: white;
  font-family: ${global.fonts.medium};
  line-height: 18px;
`;
const Amount = styled.Text`
  font-size: 14px;
  color: white;
  font-family: ${global.fonts.regular};
  line-height: 18px;
`;
const TopWrap = styled.View`
  background: #333;
  padding: 10px;
`;
const TypeInfoText = styled.Text`
  font-family: ${global.fonts.regular};
  font-size: 14px;
  color: ${({ type }) => type};
`;
const Flexer = styled.View`
  flex: 1;
`;
const ButtonText = styled.Text`
  font-size: 14px;
  color: white;
  font-family: ${global.fonts.regular};
`;
const BottomButton = styled.View`
  width: 100%;
  background: ${({ type }) => (type ? "#999999" : global.colors.orange)};
  border-radius: 10px;
  height: 40px;
  flex-direction: row;
  align-items: center;
  justify-content: center;
`;
const Row = styled.View`
  flex-direction: row;
  align-items: center;
`;
const Title = styled.Text`
  color: ${({ active }) => (active ? "white" : "#333")};
  font-size: 14px;
  font-family: ${global.fonts.medium};
`;
const Icon = styled.View`
  align-items: center;
  justify-content: center;
  width: 80px;
`;

const Wrap = styled.View`
  margin: 5px 0 5px;
  background: white;
  padding: 10px;
  width: 100%;
  box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.12);
`;
const Between = styled.View`
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
`;
export default PromoteScreen;
