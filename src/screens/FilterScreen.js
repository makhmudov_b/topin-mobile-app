import React, { useState, useEffect, useContext } from "react";
import {
  View,
  ScrollView,
  Text,
  TextInput,
  StatusBar,
  KeyboardAvoidingView,
} from "react-native";
import styled from "styled-components";
import global from "../../resources/global";
import Arrow from "../../assets/arrow-right.svg";
import TouchableScale from "react-native-touchable-scale";
import ContentLoader, { Rect } from "react-content-loader/native";
import Context from "../components/Context";
import language from "../constants/language";
import LoadingScreen from "./LoadingScreen";

const innerSize = global.strings.width - 20;
const LoadWrap = () => {
  return (
    <ContentLoader
      speed={5}
      width={innerSize}
      height={40}
      viewBox={`0 0 ${innerSize} 40`}
      // backgroundColor="#5a5a5a"
      foregroundColor="#ff7800"
      style={{ marginBottom: 15 }}
    >
      <Rect x="0" y="0" rx="10" ry="10" width={innerSize} height="40" />
    </ContentLoader>
  );
};
const Loader = () => (
  <View>
    <LoadWrap />
    <LoadWrap />
    <LoadWrap />
    <LoadWrap />
  </View>
);
const FilterScreen = ({ navigation, route }) => {
  const {
    apiService,
    currentLang,
    priceFrom,
    setPriceFrom,
    priceTo,
    setPriceTo,
    regionId,
    regions,
    chosenTypes,
    setChosenTypes,
    typesChildren,
    setTypesChildren,
    childTypes,
    setChildTypes,
  } = useContext(Context);
  const [loading, setLoading] = useState(false);
  const [bussiness, setBussiness] = useState(0);
  const category = route.params.categoryId;
  const reloadMethod = route.params.method;
  useEffect(() => {
    const checker = childTypes.find((child) => child.category_id == category);
    if (!checker) {
      if (category > 0) {
        getChildesByCategory();
      }
    }
  }, []);
  const generateRadio = (title, value, radius, chosenValue, method) => {
    return (
      <TouchableScale
        style={{ flex: 1 }}
        activeScale={0.96}
        onPress={() => method(value)}
      >
        <Choice active={chosenValue == value} type={radius}>
          <ChoiceText active={chosenValue == value ? "white" : "#999999"}>
            {title}
          </ChoiceText>
        </Choice>
      </TouchableScale>
    );
  };
  const generateBetween = (title, chosenValue, methodFrom, methodTo) => {
    return (
      <InputGroup>
        <InputTitle>{title} </InputTitle>
        <Between>
          <InputBlock
            keyboardType={`numeric`}
            onChangeText={(text) => methodFrom(text)}
            placeholder={language[currentLang].from}
            value={chosenValue.value[0]}
          />
          <InputBlock
            keyboardType={`numeric`}
            left={1}
            placeholder={language[currentLang].to}
            value={chosenValue.value[1]}
            onChangeText={(text) => methodTo(text)}
          />
        </Between>
      </InputGroup>
    );
  };
  const generateChoice = (title, value, method, children) => {
    return (
      <InputGroup>
        <InputTitle>{title} </InputTitle>
        <TouchableScale activeScale={0.96} onPress={() => method()}>
          <AdvertButton>
            <Row>
              <InputChoice>{value}</InputChoice>
            </Row>
            <Row>
              <Icon left={5} right={5}>
                <Arrow width={20} height={20} />
              </Icon>
            </Row>
          </AdvertButton>
        </TouchableScale>
      </InputGroup>
    );
  };
  const search = () => {
    reloadMethod();
    navigation.goBack();
  };
  const generateTextChoice = (title, id) => {
    const oldValue = chosenTypes.find((chosen) => chosen.id == id);
    const changeValues = (value, type) => {
      const newValue = {
        id: id,
        value: [!type ? value : 0, type ? value : 999999999],
      };
      setChosenTypes(mergeDataById([newValue, ...chosenTypes]));
    };
    const changeFrom = (value) => {
      changeValues(value, id);
    };
    const changeTo = (value) => {
      changeValues(value, id);
    };
    return (
      <InputGroup>
        <InputTitle>{title} </InputTitle>
        <Between>
          <InputBlock
            keyboardType={`numeric`}
            onChangeText={(text) => changeFrom(text)}
            placeholder={language[currentLang].from}
            value={oldValue ? oldValue.value[0].toString() : ""}
          />
          <InputBlock
            keyboardType={`numeric`}
            left={1}
            placeholder={language[currentLang].to}
            value={
              oldValue
                ? oldValue.value[1].toString() == "999999999"
                  ? ""
                  : oldValue.value[1].toString()
                : ""
            }
            onChangeText={(text) => changeTo(text)}
          />
        </Between>
      </InputGroup>
    );
  };
  const generateButton = (getType, title, method) => {
    return (
      <Flexer>
        <TouchableScale onPress={() => method()}>
          <BottomButton type={getType}>
            <ButtonText>{title}</ButtonText>
          </BottomButton>
        </TouchableScale>
      </Flexer>
    );
  };
  const findRegion = (id) => {
    const found = regions.find((cat) => cat.id == id);
    if (currentLang == 1) return found.name_ru;
    if (currentLang == 2) return found.name_uz;
    if (currentLang == 3) return found.name_kr;
  };
  const getChildesByCategory = () => {
    if (!loading) setLoading(true);
    apiService
      .getResources(`/category/${category}/types`)
      .then((value) => {
        setChildTypes(value.data);
        setTypesChildren([]);
        setLoading(false);
      })
      .catch((e) => {
        setLoading(false);
      });
  };
  const getChildTypes = (id) => {
    apiService.getResources("/types/children/" + id).then((value) => {
      setTypesChildren(mergeDataById([...typesChildren, ...value.data]));
    });
  };
  const setChoice = (id, type) => {
    /// chosen_value = [parent_id , chosen_id];
    const getChildren = [];
    childTypes.forEach((inner) => {
      getChildren.push(...inner.children);
    });
    typesChildren.forEach((inner) => {
      getChildren.push(...inner.children);
    });
    const mergedTypes = [...childTypes, ...typesChildren, ...getChildren];
    const chosenParent = mergedTypes.find(
      (inner) => inner.id == type.parent_id
    );
    if (id > 0) {
      let newData = [{ id: chosenParent.id, value: id }];
      if (chosenParent.changeable == 2) {
        getChildTypes(id);
      }
      const toRemove = getAllChildren(chosenParent);
      const merged = mergeDataById([
        ...newData,
        ...chosenTypes.filter((el) => !toRemove.includes(el.id)),
      ]);
      setChosenTypes(merged);
    } else {
      const toRemove = getAllChildren(chosenParent);
      const merged = chosenTypes
        .filter((choice) => choice.id !== chosenParent.id)
        .filter((el) => !toRemove.includes(el.id));
      setChosenTypes(merged);
    }
  };
  const mergeDataById = (data) => {
    return data.filter(function (a) {
      if (!this[a.id]) {
        this[a.id] = true;
        return true;
      }
    }, Object.create(null));
  };
  const getAllChildren = (data) => {
    let childrenIds = [];
    let pusher = data.children.map((child) => child.id);
    while (pusher.length) {
      childrenIds.push(...pusher);
      const children = [];
      typesChildren.forEach((child) => {
        pusher.forEach((item) => {
          if (parseInt(item) === parseInt(child.parent_id))
            children.push(child.id);
        });
      });
      pusher = children;
    }
    return childrenIds;
  };
  const findChosen = (id) => {
    const findChosenType = chosenTypes.filter((chosen) => chosen.id == id);
    if (findChosenType.length > 0) {
      const getChildren = [];
      childTypes.forEach((type) => {
        getChildren.push(...type.children);
      });
      typesChildren.forEach((type) => {
        getChildren.push(...type.children);
      });
      const findType = getChildren.find(
        (type) => type.id == findChosenType[0].value
      );
      if (currentLang == 1) return findType.name_ru;
      if (currentLang == 2) return findType.name_uz;
      if (currentLang == 3) return findType.name_kr;
    }
    return language[currentLang].chooseList;
  };
  const makeChoice = (choices) => {
    navigation.navigate("Choice", {
      choices,
      method: setChoice,
      headerTitle: language[currentLang].chooseList,
    });
  };
  const resetFilter = () => {
    setChosenTypes([]);
  };
  const checkForRelevance = (inner) => {
    return chosenTypes.filter((choice) => choice.value == inner.parent_id)
      .length;
  };
  const showAllChoice = (parentId) => {
    return {
      id: 0,
      parent_id: parentId,
      name_ru: language[1].all,
      name_uz: language[2].all,
      name_kr: language[3].all,
    };
  };
  if (loading) return <LoadingScreen />;
  return (
    <>
      <StatusBar barStyle={`dark-content`} backgroundColor={`white`} />
      <View style={{ flex: 1 }}>
        <ScrollView
          contentContainerStyle={{
            backgroundColor: "#f5f5f5",
          }}
        >
          <Wrap>
            {generateChoice(
              language[currentLang].region + ":",
              !regionId
                ? language[currentLang].uzbekistan
                : findRegion(regionId),
              () =>
                navigation.navigate("RegionChoice", {
                  headerTitle: language[currentLang].chooseList,
                })
            )}
            <Between>
              {generateRadio(
                language[currentLang].simpleFace,
                0,
                "left",
                bussiness,
                setBussiness
              )}
              {generateRadio(
                language[currentLang].businessFace,
                1,
                "right",
                bussiness,
                setBussiness
              )}
            </Between>
            {childTypes.map((inner, index) => {
              if (inner.children.length >= 1) {
                return (
                  <Between key={`${inner.id} ${index}`}>
                    {generateChoice(
                      currentLang == 1
                        ? inner.name_ru
                        : currentLang == 2
                        ? inner.name_uz
                        : inner.name_kr,
                      findChosen(inner.id),
                      () =>
                        makeChoice([showAllChoice(inner.id), ...inner.children])
                    )}
                  </Between>
                );
              } else {
                return (
                  <Between key={`${inner.id} ${index}`}>
                    {generateTextChoice(
                      currentLang == 1
                        ? inner.name_ru
                        : currentLang == 2
                        ? inner.name_uz
                        : inner.name_kr + ":",
                      inner.id
                    )}
                  </Between>
                );
              }
            })}
            {typesChildren.map((inner, index) => {
              if (inner.children.length >= 1 && checkForRelevance(inner)) {
                return (
                  <Between key={`${index} ${inner.id}`}>
                    {generateChoice(
                      currentLang == 1
                        ? inner.name_ru
                        : currentLang == 2
                        ? inner.name_uz
                        : inner.name_kr,
                      findChosen(inner.id),
                      () =>
                        makeChoice([showAllChoice(inner.id), ...inner.children])
                    )}
                  </Between>
                );
              }
            })}
            {generateBetween(
              language[currentLang].price,
              { value: [priceFrom, priceTo] },
              setPriceFrom,
              setPriceTo
            )}
          </Wrap>
        </ScrollView>
        <Wrap>
          <Row>
            {generateButton(1, language[currentLang].clear, () =>
              resetFilter()
            )}
            <View width={10} />
            {generateButton(0, language[currentLang].apply, () => search())}
          </Row>
          <View height={20} />
        </Wrap>
      </View>
    </>
  );
};
const Flexer = styled.View`
  flex: 1;
`;
const ButtonText = styled.Text`
  font-size: 14px;
  color: white;
  font-family: ${global.fonts.regular};
`;
const BottomButton = styled.View`
  width: 100%;
  background: ${({ type }) => (type ? "#999999" : "#2DBA49")};
  border-radius: 10px;
  height: 40px;
  flex-direction: row;
  align-items: center;
  justify-content: center;
`;
const Row = styled.View`
  flex-direction: row;
  align-items: center;
`;
const AdvertButton = styled.View`
  flex-direction: row;
  justify-content: space-between;
  padding: 10px 15px;
  height: 40px;
  shadow-color: #000;
  shadow-opacity: 0.15;
  elevation: 3;
  shadow-radius: 5px;
  margin-top: 5px;
  background: white;
  width: 100%;
  border-radius: 10px;
  border-width: 1px;
  border-color: #efefef;
`;
const Information = styled.Text`
  font-size: 14px;
  color: ${({ color }) => (color ? "#333" : "#999")};
  font-family: ${global.fonts.regular};
`;
const Icon = styled.View`
  margin-left: ${({ left }) => left + "px"};
  margin-right: ${({ right }) => right + "px"};
  align-items: center;
  justify-content: center;
`;
const Wrap = styled.View`
  margin: 5px 0 5px;
  background: white;
  padding: 10px 10px 0;
  shadow-color: #000;
  shadow-opacity: 0.12;
  elevation: 3;
  align-items: center;
`;
const Between = styled.View`
  flex-direction: row;
  align-items: center;
  width: 100%;
  justify-content: space-between;
  margin-top: ${({ margin }) => (margin ? margin + "px" : 0)};
`;
const InputChoice = styled.Text`
  position: relative;
  height: 40px;
  font-family: ${global.fonts.regular};
  line-height: 40px;
`;
const InputBlock = styled.TextInput`
  margin-top: 5px;
  background: white;
  shadow-color: #000;
  shadow-opacity: 0.12;
  shadow-radius: 2px;
  elevation: 3;
  flex: 1;
  border-width: 1px;
  border-color: #efefef;
  border-left-width: ${({ left }) => (left ? "1px" : 0)};
  border-left-color: ${({ left }) => (left ? "#EFEFEF" : "white")};
  border-top-left-radius: ${({ left }) => (!left ? 10 + "px" : 0)};
  border-bottom-left-radius: ${({ left }) => (!left ? 10 + "px" : 0)};
  border-top-right-radius: ${({ left }) => (left ? 10 + "px" : 0)};
  border-bottom-right-radius: ${({ left }) => (left ? 10 + "px" : 0)};
  height: 40px;
  /* border-radius:10px; */
  font-family: ${global.fonts.regular};
  padding: 10px 15px;
`;
const InputTitle = styled.Text`
  color: #999999;
  font-family: ${global.fonts.regular};
  font-size: 14px;
`;
const InputGroup = styled.View`
  width: 100%;
  margin-bottom: 10px;
`;
const Choice = styled.View`
  width: 100%;
  height: 40px;
  margin-bottom: 20px;
  shadow-color: #000;
  shadow-opacity: 0.2;
  shadow-radius: 5px;
  elevation: 3;
  /* border-width: 1px; */
  /* border-color: #efefef; */
  background: ${({ active }) => (active ? "#5A5A5A" : "white")};
  border-bottom-color: ${({ active }) => (active ? "#5A5A5A" : "white")};
  border-top-left-radius: ${({ type }) => (type === "left" ? 10 + "px" : 0)};
  border-bottom-left-radius: ${({ type }) => (type === "left" ? 10 + "px" : 0)};
  border-top-right-radius: ${({ type }) => (type === "right" ? 10 + "px" : 0)};
  border-bottom-right-radius: ${({ type }) =>
    type === "right" ? 10 + "px" : 0};
  /* border-left-width: ${({ type }) => (!type ? 2 + "px" : 0)};
  border-right-width: ${({ type }) => (!type ? 2 + "px" : 0)};
  border-left-color: ${({ type }) => (!type ? "#F5F5F5" : 0)};
  border-right-color: ${({ type }) => (!type ? "#F5F5F5" : 0)}; */
  align-items: center;
  justify-content: center;
`;
const ChoiceText = styled.Text`
  font-family: ${global.fonts.regular};
  font-size: 14px;
  color: ${({ active }) => active};
`;
export default FilterScreen;
