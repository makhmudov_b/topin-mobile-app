import React, { useState, useEffect, useContext } from "react";
import {
  View,
  ScrollView,
  TextInput,
  Text,
  Image,
  TouchableOpacity,
  KeyboardAvoidingView,
  Alert,
} from "react-native";
import styled from "styled-components";
import global from "../../resources/global";
import Profile from "../../assets/profile.svg";
import TouchableScale from "react-native-touchable-scale";
import Confirm from "../../assets/confirm.svg";
import language from "../constants/language";
import Context from "../components/Context";
import BlackLoadingScreen from "./BlackLoadingScreen";

const ConfirmScreen = ({ navigation }) => {
  const [loading, setLoading] = useState(false);
  const [code, setCode] = useState("");
  const {
    apiService,
    currentLang,
    phone,
    setToken,
    setRegistered,
    setUser,
  } = useContext(Context);

  const confirmCode = () => {
    if (code.length === 4) {
      setLoading(true);
      apiService
        .authorize({ code: code, phone: phone })
        .then((value) => {
          if (value) {
            setToken(value.data.token);
            if (value.data.user) {
              setRegistered(value.data.user ? true : false);
              setUser(value.data.user);
              setTimeout(() => navigation.jumpTo("Profile"), 2000);
            } else {
              navigation.replace("Type");
            }
          }
        })
        .catch((e) => {
          Alert.alert("Не правильный код");
          setLoading(false);
        });
    }
  };
  if (loading) return <BlackLoadingScreen />;
  return (
    <KeyboardAvoidingView
      style={{
        backgroundColor: "#999",
        flex: 1,
      }}
      // behavior="padding"
      // keyboardVerticalOffset={200}
    >
      <Wrap>
        <Confirm />
        <View height={20} />
        <Title>{language[currentLang].verification}</Title>
        <Between>
          <InputWrap>
            <LoginInput
              placeholder={language[currentLang].verify_code}
              placeholderTextColor="#999"
              keyboardType="number-pad"
              returnKeyType={"done"}
              value={code}
              onChangeText={(text) => setCode(text)}
              maxLength={4}
            />
          </InputWrap>
          {/* <InputRight>60</InputRight> */}
        </Between>
        <InfoText>
          {language[currentLang].sms} {"\n"}
          +998 {phone}
          {/* <OrangeText>Отправить заного</OrangeText> */}
        </InfoText>
        <TouchableScale
          activeScale={0.96}
          onPress={() => confirmCode()}
          style={{ width: "100%" }}
        >
          <LoginButton>
            <LoginText>{language[currentLang].verification}</LoginText>
          </LoginButton>
        </TouchableScale>
        {Platform.OS == "ios" ? <View height={100} /> : null}
      </Wrap>
    </KeyboardAvoidingView>
  );
};
const LoginText = styled.Text`
  color: white;
  font-size: 14px;
  text-align: center;
  font-family: ${global.fonts.regular};
`;
const LoginButton = styled.View`
  width: 100%;
  background: #2dba49;
  border-radius: 10px;
  height: 40px;
  align-items: center;
  justify-content: center;
`;
const Wrap = styled.View`
  margin: 5px 0 5px;
  background: ${global.colors.main};
  padding: 10px;
  flex: 1;
  box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.12);
  justify-content: center;
  align-items: center;
`;
const Title = styled.Text`
  font-family: ${global.fonts.medium};
  font-size: 24px;
  line-height: 36px;
  color: white;
  padding-bottom: 20px;
`;
const InputWrap = styled.View`
  background: white;
  border-radius: 10px;
  height: 40px;
  flex: 1;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
`;
const InputRight = styled.Text`
  height: 40px;
  width: 40px;
  background: white;
  border-radius: 10px;
  text-align: center;
  line-height: 40px;
  font-family: ${global.fonts.regular};
  color: ${global.colors.orange};
  margin-left: 10px;
`;
const InfoText = styled.Text`
  color: #aeaeae;
  font-size: 14px;
  font-family: ${global.fonts.regular};
  text-align: center;
  margin: 20px 0;
`;
const OrangeText = styled.Text`
  color: ${global.colors.orange};
`;
const LoginInput = styled.TextInput`
  flex: 1;
  height: 40px;
  color: #333333;
  font-size: 14px;
  font-family: ${global.fonts.regular};
  padding: 0 10px;
  text-align: center;
`;
const PaddingHorizontal = styled.View`
  padding: 0 5px;
`;
const Between = styled.View`
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
`;
export default ConfirmScreen;
