import React, { useState, useEffect, useContext } from "react";
import {
  View,
  ScrollView,
  TextInput,
  Text,
  Image,
  TouchableOpacity,
} from "react-native";
import styled from "styled-components";
import global from "../../resources/global";
import AdvertHorizontal from "../components/AdvertHorizontal";
import Arrow from "../../assets/arrow-down.svg";
import ArrowRight from "../../assets/arrow.svg";
import ArrowRightBlack from "../../assets/arrow-right.svg";
import SearchIcon from "../../assets/search.svg";
import Filter from "../../assets/filter.svg";
import Map from "../../assets/map-icon.svg";

import TouchableScale from "react-native-touchable-scale";
import AddButton from "../components/AddButton";
import Context from "../components/Context";
import language from "../constants/language";
import LoadingScreen from "./LoadingScreen";
import Plus from "../../assets/plus.svg";

const SearchScreen = ({ navigation, route }) => {
  const {
    apiService,
    categories,
    chosenTypes,
    currentLang,
    priceFrom,
    priceTo,
    regionId,
    setRegionId,
    regions,
  } = useContext(Context);
  const [loading, setLoading] = useState(false);
  const [adverts, setAdverts] = useState([]);
  const [keyword, setKeyword] = useState("");
  const [total, setTotal] = useState(0);
  const [sort, setSorting] = useState(1);
  const [sorts, setSorts] = useState([
    {
      name_ru: "Сначала новые",
      name_uz: "Eng yangi",
      name_kr: "Энг янги",
      id: 1,
    },
    {
      name_ru: "Сначала старые",
      name_uz: "Eng eski",
      name_kr: "Энг эски",
      id: 2,
    },
    {
      name_ru: "Сначала дорогие",
      name_uz: "Eng qimmat",
      name_kr: "Энг қиммат",
      id: 3,
    },
    {
      name_ru: "Сначала дешёвые",
      name_uz: "Eng arzon",
      name_kr: "Энг арзон",
      id: 4,
    },
  ]);
  const [category, setCategory] = useState(0);
  const [page, setPage] = useState(1);
  const setSort = (id) => {
    setSorting(id);
    getSearchResult();
  };
  useEffect(() => {
    const fallback = navigation.addListener("focus", () => {
      if (route.params) {
        if (category != route.params.categoryId) {
          setCategory(route.params.categoryId);
          getSearchResult(route.params.categoryId);
        }
      }
      // if (category != categoryId) {
      // }
    });
    return fallback;
  });
  const chooseCategory = (value) => {
    if (category != value) {
      setCategory(value);
      setPage(1);
      getSearchResult(value);
    }
  };
  const getSearchResult = (sentCategory, sentRegion, page) => {
    if (!loading) setLoading(true);
    setAdverts([]);
    const infinityNumber = 999999999;
    const price_range = `${priceFrom.length ? parseInt(priceFrom) : 0},${
      priceTo.length ? parseInt(priceTo) : infinityNumber
    }`;
    let type_value = chosenTypes;
    const sortBy = sort == 1 || sort == 3 ? "DESC" : "ASC";
    const orderBy = sort == 1 || sort == 2 ? "created_at" : "price";
    const body = {
      category_id: sentCategory != null ? sentCategory : category,
      region_id: sentRegion != null ? sentRegion : regionId,
      ...(keyword.length && { keyword: keyword }),
      price_range: price_range,
      // sort_by: sortBy,
      // order_by: orderBy,
      ...(type_value.length && { type_value: JSON.stringify(type_value) }),
      page: page != null ? page : 1,
    };
    apiService
      .getResources("/search?" + new URLSearchParams(body))
      .then((value) => {
        if (body.page == 1) {
          setAdverts(value.data);
        } else {
          if (value.data.length) setAdverts([...adverts, ...value.data]);
          else setAdverts([]);
        }
        setTotal(value.last_page);
        setLoading(false);
      })
      .catch((e) => {
        setAdverts([]);
        setTotal(1);
        setPage(1);
        setLoading(false);
      });
  };
  const setRegion = (value) => {
    if (regionId != value) {
      setPage(1);
      setRegionId(value);
      getSearchResult(category, value);
    }
  };
  const handleKeyword = (value) => {
    setKeyword(value);
    // if (categoryId) {
    //   chooseCategory(0);
    // }
  };
  const findCategories = () => {
    const getKeyword = keyword;
    if (!getKeyword.length)
      return categories.filter((category) => !category.parent_id);
    return categories.filter((category) => {
      if (category.name_ru.includes(getKeyword)) return category;
      if (category.name_uz.includes(getKeyword)) return category;
      if (category.name_kr.includes(getKeyword)) return category;
    });
  };
  const findCategory = (id) => {
    const category = categories.find((cat) => cat.id == id);
    if (currentLang == 1) return category.name_ru;
    if (currentLang == 2) return category.name_uz;
    if (currentLang == 3) return category.name_kr;
  };
  const findRegion = (id) => {
    const found = regions.find((cat) => cat.id == id);
    if (currentLang == 1) return found.name_ru;
    if (currentLang == 2) return found.name_uz;
    if (currentLang == 3) return found.name_kr;
  };
  const findSort = () => {
    const found = sorts.find((dat) => dat.id == sort);
    if (currentLang == 1) return found.name_ru;
    if (currentLang == 2) return found.name_uz;
    if (currentLang == 3) return found.name_kr;
  };
  const generateResult = (title, category, method) => {
    return (
      <TouchableScale
        style={{ marginBottom: 10 }}
        activeScale={0.98}
        onPress={() => method()}
      >
        <Result>
          <Between full={1}>
            <ResultLeft>
              <ResultTitle>{title}</ResultTitle>
              <ResultCategory>{category}</ResultCategory>
            </ResultLeft>
            <Icon>
              <ArrowRightBlack width={20} height={20} />
            </Icon>
          </Between>
        </Result>
      </TouchableScale>
    );
  };
  const getMore = (value) => {
    setPage(value);
    getSearchResult(null, null, value);
  };
  const ShowFoundAdverts = () => {
    return (
      <>
        <Wrap>
          <PaddingHorizontal>
            <Between>
              <ChoiceButton
                onPress={() =>
                  navigation.navigate("CategoryChoice", {
                    headerTitle: language[currentLang].chooseList,
                  })
                }
              >
                <ChoiceText>
                  {!category
                    ? language[currentLang].all
                    : findCategory(category)}
                </ChoiceText>
                <ArrowRight />
              </ChoiceButton>
              <ChoiceButton
                onPress={() =>
                  navigation.navigate("RegionChoice", {
                    headerTitle: language[currentLang].chooseList,
                    method: setRegion,
                  })
                }
              >
                <ChoiceText>
                  {!regionId
                    ? language[currentLang].uzbekistan
                    : findRegion(regionId)}
                </ChoiceText>
                <ArrowRight />
              </ChoiceButton>
            </Between>
          </PaddingHorizontal>
        </Wrap>
        {adverts.length > 0 && (
          <BlackWrap>
            <TouchableScale
              onPress={() => navigation.navigate("Map", { adverts: adverts })}
              activeScale={0.98}
            >
              <PaddingHorizontal>
                <Between>
                  <Row>
                    <Map />
                    <MapText>{language[currentLang].mapResult}</MapText>
                  </Row>
                  <ArrowRight width={24} height={24} />
                </Between>
              </PaddingHorizontal>
            </TouchableScale>
          </BlackWrap>
        )}
        <Wrap>
          <View>
            <Between>
              <Title>{language[currentLang].results}</Title>
              <GoButton
                onPress={() =>
                  navigation.navigate("Choice", {
                    headerTitle: language[currentLang].sort,
                    choices: sorts,
                    method: setSort,
                  })
                }
              >
                <ButtonText>{findSort()}</ButtonText>
                <Arrow />
              </GoButton>
            </Between>
            {loading && <LoadingScreen />}
            {!loading &&
              adverts.map((advert, key) => (
                <AdvertHorizontal
                  key={
                    key +
                    `${advert.id} 
                    + ${Math.floor(Math.random() * (100 - 1)) + 1}
                    `
                  }
                  advert={advert}
                />
              ))}
            {!loading && !adverts.length && (
              <View style={{ alignItems: "center", justifyContent: "center" }}>
                <Image
                  source={global.images.notFound}
                  style={{ height: 200, resizeMode: "contain" }}
                />
                <Title>{language[currentLang].notFound}</Title>
                <View style={{ height: 50 }} />
              </View>
            )}
            {/* {adverts.length > 0 && total > 1 && page < total && (
              <Between>
                <TouchableScale onPress={() => getMore(null, null, page + 1)}>
                  <Btn>
                    <BtnText>...</BtnText>
                  </Btn>
                </TouchableScale>
              </Between>
            )} */}
            <View height={50} />
          </View>
        </Wrap>
      </>
    );
  };
  const SearchedCategoryResults = () => (
    <Wrap>
      {findCategories().map((cat, index) => {
        return (
          <React.Fragment key={index + "" + cat.id}>
            {generateResult(
              currentLang == 1
                ? cat.name_ru
                : currentLang == 2
                ? cat.name_uz
                : cat.name_kr,
              cat.parent_id
                ? findCategory(cat.parent_id)
                : findCategory(cat.id),
              () => chooseCategory(cat.id)
            )}
          </React.Fragment>
        );
      })}
      {!categories.length && (
        <Image
          source={global.images.loading}
          style={{
            height: 150,
            marginTop: 60,
            marginBottom: 60,
            alignSelf: "center",
          }}
          resizeMode="contain"
        />
      )}
    </Wrap>
  );
  const isCloseToBottom = ({
    layoutMeasurement,
    contentOffset,
    contentSize,
  }) => {
    const paddingToBottom = 5;
    return (
      layoutMeasurement.height + contentOffset.y >=
      contentSize.height - paddingToBottom
    );
  };
  return (
    <>
      <Search>
        <SearchIcon color={"#999999"} />
        <SearchInput
          value={keyword}
          onChangeText={(text) => handleKeyword(text)}
          onEndEditing={() => getSearchResult()}
        />
        {category > 0 || adverts.length ? (
          <FilterButton
            onPress={() =>
              navigation.navigate("Filter", {
                categoryId: category,
                method: getSearchResult,
              })
            }
          >
            <Between>
              <Filter />
              <FilterText>{language[currentLang].filter}</FilterText>
            </Between>
          </FilterButton>
        ) : null}
      </Search>
      <ScrollView
        // onScroll={({ nativeEvent }) => {
        //   if (adverts.length > 0 && total > 1 && page < total) {
        //     if (isCloseToBottom(nativeEvent)) {
        //       setPage(page + 1);
        //       getSearchResult(category, regionId);
        //     }
        //   }
        // }}
        scrollEventThrottle={400}
        contentContainerStyle={{ backgroundColor: "#F0F0F0" }}
      >
        {category > 0 || adverts.length ? (
          <ShowFoundAdverts />
        ) : (
          <SearchedCategoryResults />
        )}
      </ScrollView>
      <AddButton />
    </>
  );
};
const ResultLeft = styled.View`
  flex: 1;
`;
const BtnText = styled.Text`
  font-size: 14px;
  font-family: ${global.fonts.medium};
  color: white;
`;
const Btn = styled.View`
  height: 40px;
  padding: 0 10px;
  flex-direction: row;
  align-items: center;
  justify-content: center;
  border-radius: 10px;
  background: ${global.colors.orange};
`;
const Icon = styled.View`
  width: 20px;
  height: 20px;
  align-items: center;
  justify-content: center;
`;
const ResultTitle = styled.Text`
  font-family: ${global.fonts.regular};
  color: #333333;
  font-size: 14px;
`;
const ResultCategory = styled.Text`
  font-family: ${global.fonts.regular};
  color: #999999;
  font-size: 10px;
  margin-top: 5px;
`;
const Result = styled.View`
  flex-direction: row;
  border-radius: 10px;
  height: 60px;
  border-radius: 10px;
  shadow-color: #000;
  shadow-opacity: 0.12;
  shadow-offset: 2px 2px;
  elevation: 3;
  border-width: 1px;
  border-color: #efefef;
  margin: 0 10px;
  background: white;
  padding: 10px 15px;
`;
const FilterButton = styled.TouchableOpacity`
  background: ${global.colors.orange};
  height: 100%;
  width: 110px;
  align-items: center;
  justify-content: center;
`;
const FilterText = styled.Text`
  font-size: 14px;
  color: white;
  padding-left: 10px;
  font-family: ${global.fonts.regular};
`;
const Row = styled.View`
  flex-direction: row;
  align-items: center;
`;
const MapText = styled.Text`
  color: white;
  font-size: 14px;
  margin-left: 15px;
  font-family: ${global.fonts.medium};
`;
const Title = styled.Text`
  font-size: 18px;
  line-height: 22px;
  padding: 0 10px;
  font-family: ${global.fonts.medium};
  color: ${global.colors.secondary};
`;
const Wrap = styled.View`
  margin-top: 5px;
  background: ${global.colors.background};
  box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.12);
  padding: 10px 0;
`;
const BlackWrap = styled.View`
  background: ${global.colors.main};
  /* box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.12); */
  padding: 10px;
  justify-content: center;
`;
const PaddingHorizontal = styled.View`
  padding: 0 5px;
`;
const Between = styled.View`
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  width: ${({ full }) => (full ? "100%" : "auto")};
`;
const GoButton = styled.TouchableOpacity`
  flex-direction: row;
  align-items: center;
  justify-content: center;
  background: #efefef;
  border-radius: 10px;
  padding: 0 10px;
  height: 20px;
  margin-right: 10px;
`;
const ButtonText = styled.Text`
  color: ${global.colors.secondary};
  font-family: ${global.fonts.regular};
  font-size: 10px;
  margin-right: 5px;
`;
const Search = styled.View`
  flex-direction: row;
  background: white;
  height: 50px;
  align-items: center;
  padding-left: 10px;
  shadow-color: #000;
  shadow-opacity: 0.12;
  elevation: 3;
`;
const SearchInput = styled.TextInput`
  flex: 1;
  height: 50px;
  font-size: 18px;
  font-family: ${global.fonts.regular};
  margin: 0 10px;
`;
const ChoiceButton = styled.TouchableOpacity`
  background: #aeaeae;
  border-radius: 10px;
  height: 30px;
  flex: 1;
  max-width: ${global.strings.width / 2.1 + "px"};
  align-items: center;
  flex-direction: row;
  padding: 0 10px;
  justify-content: space-between;
`;
const ChoiceText = styled.Text`
  color: white;
  font-family: ${global.fonts.regular};
  font-size: 12px;
`;
export default SearchScreen;
