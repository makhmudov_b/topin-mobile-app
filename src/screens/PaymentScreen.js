import React, { useState, useEffect, useContext } from "react";
import {
  View,
  ScrollView,
  TextInput,
  Text,
  Image,
  TouchableOpacity,
  StatusBar,
} from "react-native";
import styled from "styled-components";
import global from "../../resources/global";
import TouchableScale from "react-native-touchable-scale";
import Context from "../components/Context";
import language from "../constants/language";
const generateButton = (type, disabled, title, method) => {
  return (
    <Flexer>
      <TouchableScale disabled={disabled} onPress={() => method()}>
        <BottomButton type={type}>
          <ButtonText>{title}</ButtonText>
        </BottomButton>
      </TouchableScale>
    </Flexer>
  );
};
const PaymentScreen = ({ navigation, route }) => {
  const { apiService, currentLang, user, token } = useContext(Context);
  const { price } = route.params;
  const [chosen, setChosen] = useState(-1);
  const makeChoice = (value) => {
    const setValue = value === chosen ? -1 : value;
    setChosen(setValue);
  };
  const generatePayment = (image, method, active) => {
    return (
      <TouchableScale
        style={{ marginBottom: 10, width: global.strings.width / 2.2 }}
        onPress={() => method()}
        activeScale={0.96}
      >
        <Promotion active={active === chosen ? 1 : 0}>
          <PaymentImage resizeMode={`contain`} source={image} />
        </Promotion>
      </TouchableScale>
    );
  };
  const getUrl = () => {
    if (!chosen) return;
    const url = chosen == 1 ? "uzcard" : "octo";
    const body = {
      price: price,
    };
    apiService
      .getResources("/payment/" + url + "?" + new URLSearchParams(body), token)
      .then((value) => {
        const url = value.data;
        navigation.navigate("Web", { url: url });
      });
  };
  return (
    <>
      <StatusBar backgroundColor={`white`} barStyle={`dark-content`} />
      <TopWrap>
        <Between>
          <View>
            <Balance>{language[currentLang].balance}:</Balance>
          </View>
          <View>
            <Amount>{user.balance.amount} UZS</Amount>
          </View>
        </Between>
      </TopWrap>
      <ScrollView
        contentContainerStyle={{
          backgroundColor: "#f5f5f5",
        }}
      >
        <Wrap>
          <Balance>{language[currentLang].paymentType}:</Balance>
          <View height={10} />
          <Between>
            {generatePayment(global.images.uzsPay, () => makeChoice(1), 1)}
            {generatePayment(global.images.usdPay, () => makeChoice(2), 2)}
          </Between>
        </Wrap>
      </ScrollView>
      <Wrap>
        <Row>
          {generateButton(
            chosen < 0,
            chosen < 0,
            language[currentLang].choose,
            () => getUrl()
          )}
          <View width={10} />
        </Row>
      </Wrap>
    </>
  );
};
const PaymentImage = styled.Image`
  width: 60%;
  height: 100px;
`;
const Promotion = styled.View`
  width: ${global.strings.width / 2.2 + "px"};
  align-items: center;
  justify-content: center;
  background: white;
  border-radius: 10px;
  shadow-color: #000;
  elevation: ${({ active }) => (active ? 10 : 3)};
  border-width: 2px;
  border-color: ${({ active }) => (active ? global.colors.orange : "white")};
`;
const Balance = styled.Text`
  font-size: 14px;
  color: #333333;
  font-family: ${global.fonts.medium};
  line-height: 18px;
`;
const Amount = styled.Text`
  font-size: 14px;
  color: #333333;
  font-family: ${global.fonts.regular};
  line-height: 18px;
`;
const TopWrap = styled.View`
  background: white;
  padding: 10px;
`;
const Flexer = styled.View`
  flex: 1;
`;
const ButtonText = styled.Text`
  font-size: 14px;
  color: white;
  font-family: ${global.fonts.regular};
`;
const BottomButton = styled.View`
  width: 100%;
  background: ${({ type }) => (type ? "#999999" : global.colors.orange)};
  border-radius: 10px;
  height: 40px;
  flex-direction: row;
  align-items: center;
  justify-content: center;
`;
const Row = styled.View`
  flex-direction: row;
  align-items: center;
`;
const Wrap = styled.View`
  margin: 5px 0 5px;
  background: white;
  padding: 10px;
  width: 100%;
  box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.12);
`;
const Between = styled.View`
  flex-direction: row;
  align-items: center;
  width: 100%;
  flex-wrap: wrap;
  justify-content: space-between;
`;
export default PaymentScreen;
