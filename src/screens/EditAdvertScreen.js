import React, { useState, useEffect, useContext } from "react";
import {
  View,
  ScrollView,
  TextInput,
  Text,
  Image,
  TouchableOpacity,
  StatusBar,
  Platform,
  KeyboardAvoidingView,
  Alert,
} from "react-native";
import global from "../../resources/global";
import Arrow from "../../assets/arrow-right.svg";
import Plus from "../../assets/plus-black.svg";
import Trash from "../../assets/trash.svg";
import TouchableScale from "react-native-touchable-scale";
import * as Permissions from "expo-permissions";
import * as ImagePicker from "expo-image-picker";
import styled from "styled-components";

import Context from "../components/Context";
import language from "../constants/language";
import LoadingScreen from "./LoadingScreen";
import SuccessScreen from "./SuccessScreen";

const EditAdvertScreen = ({ navigation, route }) => {
  const {
    apiService,
    token,
    phone,
    regions,
    categories,
    currentLang,
  } = useContext(Context);
  const { advertId } = route.params;
  const [name, setName] = useState("");
  const [price, setPrice] = useState("");
  const [childTypes, setChildTypes] = useState([]);
  const [typesChildren, setTypesChildren] = useState([]);
  const [chosenTypes, setChosenTypes] = useState([]);
  const [advert, setAdvert] = useState([]);
  const [category, setCategory] = useState(null);
  const [loading, setLoading] = useState(true);
  const [description, setDescription] = useState("");
  const [region, setRegion] = useState(null);
  const [address, setAddress] = useState("");
  const [coordinates, setCoordinate] = useState(null);
  const [mail, setMail] = useState("");
  const [camera, setCamera] = useState(false);
  const [images, setImages] = useState([]);
  const [succes, setSuccess] = useState(false);
  useEffect(() => {
    getPermission();
    getOwnAdvert();
  }, []);
  const getOwnAdvert = () => {
    apiService
      .getResources("/my/adverts/" + advertId, token)
      .then((value) => {
        const { data } = value;
        setAdvert(data);
        const getChosenType = data.advert_type.map((type) => {
          if (type.changeable == 1) {
            return { id: type.id, value: type.pivot.value };
          } else {
            return { id: type.id, value: type.pivot.value.id };
          }
        });
        setChosenTypes(getChosenType);
        setName(data.name);
        setCategory(data.category_id);
        setAddress(data.address_info);
        setRegion(data.category_id);
        setDescription(data.description ? data.description : "");
        if (data.lat && data.lat) {
          setCoordinate({
            longitude: Number(data.long),
            latitude: Number(data.lat),
          });
        }
        setPrice(data.price);
        setLoading(false);
      })
      .catch((e) => {
        Alert.alert("Что-то пошло не так!");
        navigation.goBack();
      });
  };
  const generateBlock = (title, value, method, type, disabled) => {
    return (
      <InputGroup>
        <InputTitle>{title} </InputTitle>
        <InputBlock
          placeholder={language[currentLang].addInfo}
          onChangeText={(text) => method(text)}
          editable={disabled ? false : true}
          // selectTextOnFocus={disabled ? false : true}
          keyboardType={type ? "numeric" : "default"}
          value={value}
        />
      </InputGroup>
    );
  };
  const generateChoice = (title, value, method) => {
    return (
      <InputGroup>
        <InputTitle>{title} </InputTitle>
        <View height={5} />
        <TouchableScale activeScale={0.96} onPress={() => method()}>
          <InputChoice>
            <Between>
              <InputChoiceText active={value ? 1 : 0}>
                {value ? value : language[currentLang].chooseList}
              </InputChoiceText>
              <Icon>
                <Arrow />
              </Icon>
            </Between>
          </InputChoice>
        </TouchableScale>
      </InputGroup>
    );
  };
  const getPermission = async () => {
    if (Platform === "ios") {
      const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL);
      if (status !== "granted") {
        Alert.alert("Нужно дать доступ для камеры");
      } else {
        setCamera(true);
      }
    } else {
      setCamera(true);
    }
  };
  const addImage = (value) => {
    if (
      images.length <
      10 +
        (advert.image.length
          ? advert.image[0].split("/").length > 5
            ? advert.image.length
            : 0
          : 0)
    ) {
      setImages([...images, value]);
    }
  };
  const generateButton = (type, disabled, title, method) => {
    return (
      <Flexer>
        <TouchableScale disabled={disabled} onPress={() => method()}>
          <BottomButton type={type}>
            <ButtonText>{title}</ButtonText>
          </BottomButton>
        </TouchableScale>
      </Flexer>
    );
  };
  const generateInfo = (left, right) => {
    return (
      <Between margin={5}>
        <Information color={1}>{left}</Information>
        <Information>{right}</Information>
      </Between>
    );
  };
  const goToCameraRoll = async () => {
    try {
      let result = await ImagePicker.launchImageLibraryAsync({
        allowsMultipleSelection: true,
        mediaTypes: ImagePicker.MediaTypeOptions.Images,
        allowsEditing: true,
        quality: 1,
        base64: true,
      });
      if (!result.cancelled) {
        addImage(result.uri);
      }
    } catch (E) {}
  };
  const pickImage = () => {
    if (camera) {
      goToCameraRoll();
    } else {
      getPermission();
    }
  };
  const findRegion = (id) => {
    const found = regions.find((cat) => cat.id == id);
    if (currentLang == 1) return found.name_ru;
    if (currentLang == 2) return found.name_uz;
    if (currentLang == 3) return found.name_kr;
  };
  const findCategory = (id) => {
    const category = categories.find((cat) => cat.id == id);
    if (currentLang == 1) return category.name_ru;
    if (currentLang == 2) return category.name_uz;
    if (currentLang == 3) return category.name_kr;
  };
  const setCategoryId = (id) => {
    getChildesByCategory(id);
    setCategory(id);
  };
  const getChildTypes = (id) => {
    apiService.getResources("/types/children/" + id).then((value) => {
      setTypesChildren(mergeDataById([...typesChildren, ...value.data]));
    });
  };
  const setChoice = (id, type) => {
    /// chosen_value = [id , chosen_type];
    const getChildren = [];
    childTypes.forEach((inner) => {
      getChildren.push(...inner.children);
    });
    typesChildren.forEach((inner) => {
      getChildren.push(...inner.children);
    });
    const mergedTypes = [...childTypes, ...typesChildren, ...getChildren];
    const chosenParent = mergedTypes.find(
      (inner) => inner.id == type.parent_id
    );
    if (id > 0) {
      let newData = [{ id: chosenParent.id, value: id }];
      if (chosenParent.changeable == 2) {
        getChildTypes(id);
      }
      const toRemove = getAllChildren(chosenParent);
      const merged = mergeDataById([
        ...newData,
        ...chosenTypes.filter((el) => !toRemove.includes(el.id)),
      ]);
      setChosenTypes(merged);
    } else {
      const toRemove = getAllChildren(chosenParent);
      const merged = chosenTypes
        .filter((choice) => choice.id !== chosenParent.id)
        .filter((el) => !toRemove.includes(el.id));
      setChosenTypes(merged);
    }
  };
  const mergeDataById = (data) => {
    return data.filter(function (a) {
      if (!this[a.id]) {
        this[a.id] = true;
        return true;
      }
    }, Object.create(null));
  };
  const getAllChildren = (data) => {
    let childrenIds = [];
    let pusher = data.children.map((child) => child.id);
    while (pusher.length) {
      childrenIds.push(...pusher);
      const children = [];
      typesChildren.forEach((child) => {
        pusher.forEach((item) => {
          if (parseInt(item) === parseInt(child.parent_id))
            children.push(child.id);
        });
      });
      pusher = children;
    }
    return childrenIds;
  };
  const findChosen = (id) => {
    const findChosenType = chosenTypes.filter((chosen) => chosen.id == id);
    if (findChosenType.length > 0) {
      const getChildren = [];
      childTypes.forEach((type) => {
        getChildren.push(...type.children);
      });
      typesChildren.forEach((type) => {
        getChildren.push(...type.children);
      });
      const findType = getChildren.find(
        (type) => type.id == findChosenType[0].value
      );
      if (currentLang == 1) return findType.name_ru;
      if (currentLang == 2) return findType.name_uz;
      if (currentLang == 3) return findType.name_kr;
    }
    return language[currentLang].chooseList;
  };
  const findText = (id) => {
    const findChosenType = chosenTypes.find((chosen) => chosen.id == id);
    if (findChosenType) return findChosenType.value;
    return "";
  };
  const getChildesByCategory = (categoryId) => {
    apiService
      .getResources(`/category/${categoryId}/types`)
      .then((value) => {
        setChildTypes(value.data);
        setTypesChildren([]);
        setChosenTypes([]);
      })
      .catch((e) => {});
  };
  const makeChoice = (choices) => {
    navigation.navigate("Choice", {
      choices,
      method: setChoice,
      headerTitle: language[currentLang].chooseList,
    });
  };
  const checkForRelevance = (inner) => {
    return chosenTypes.filter((choice) => choice.value == inner.parent_id)
      .length;
  };
  const setCoordinates = (data) => {
    setCoordinate(data);
  };
  const setText = (value, id) => {
    const data = chosenTypes.filter((chosen) => chosen.id != id);
    const newData = value.length ? { id: id, value: value } : {};
    setChosenTypes([newData, ...data]);
  };
  const updateAdvert = () => {
    if (
      !name.length &&
      !description.length &&
      !price.length &&
      !coordinates &&
      !chosenTypes.length &&
      !address.length &&
      !region &&
      !category
    ) {
      Alert.alert("Нужно заполнить всё!");
      return;
    }
    const advert_type = [];
    const type_id = [];
    // [id: type_id , value: value || child_id]
    chosenTypes.forEach((chosen) => {
      advert_type.push(chosen.value);
      type_id.push(chosen.id);
    });
    const body = {
      advert_type: advert_type,
      type_id: type_id,
      category_id: category,
      region_id: region,
      name: name,
      description: description,
      price: price,
      phone: phone,
      lat: coordinates.latitude.toFixed(6),
      long: coordinates.longitude.toFixed(6),
      files: images,
      address_info: address,
      id: advert.id,
    };
    if (!loading) setLoading(true);
    apiService
      .updateAdvert(token, body)
      .then((value) => {
        if (value) {
          setLoading(false);
          if (value) {
            setSuccess(true);
            setTimeout(() => navigation.replace("MyAdvert"), 2000);
          }
        }
      })
      .catch((e) => {
        Alert.alert("Что-то пошло не так!");
        if (loading) setLoading(false);
      });
  };
  const removeImage = (index) => {
    const removed = images.filter((img, iter) => iter != index);
    setImages(removed);
  };
  const removeAdvertImage = async ({ id, file_name }) => {
    const res = await apiService.getResources(
      "/remove/image/advert/" +
        id +
        "?" +
        new URLSearchParams({ file_name: file_name }),
      token
    );
    return res.statusCode == 200;
  };
  const deleteImage = async (key) => {
    const file = advert.image[key];
    const name = file.split("/")[6];
    const isDeleted = await removeAdvertImage({
      id: advert.id,
      file_name: name,
    });
    if (isDeleted) {
      const getAdvert = advert;
      getAdvert.image = getAdvert.image.filter((file, index) => index !== key);
      setAdvert(getAdvert);
    }
  };
  if (succes) return <SuccessScreen />;
  if (loading) return <LoadingScreen />;
  return (
    <>
      <StatusBar backgroundColor={"white"} barStyle={`dark-content`} />
      <KeyboardAvoidingView
        style={{ flex: 1 }}
        // behavior="padding"
        // keyboardVerticalOffset={150}
      >
        <ScrollView
          contentContainerStyle={{
            backgroundColor: "#f5f5f5",
            // flex: 1,
          }}
        >
          <Wrap>
            <Title>{language[currentLang].addRazdel}</Title>
            {generateBlock(language[currentLang].addName + ":", name, setName)}
            {generateChoice(
              language[currentLang].addCategory + ":",
              !category ? "" : findCategory(category),
              () =>
                navigation.navigate("CategoryChoice", {
                  headerTitle: language[currentLang].chooseList,
                  method: setCategoryId,
                })
            )}
          </Wrap>
          <Wrap>
            <Title>{language[currentLang].addDetails}</Title>
            {!childTypes.length && (
              <InputGroup>
                <InputTitle>{language[currentLang].toChange}</InputTitle>
              </InputGroup>
            )}
            {!childTypes.length &&
              advert.advert_type.map((info, key) => {
                return (
                  <React.Fragment key={key}>
                    {generateInfo(
                      currentLang == 1
                        ? info.name_ru
                        : currentLang == 2
                        ? info.name_uz
                        : info.name_kr,
                      info.changeable == 1
                        ? info.pivot.value
                        : currentLang == 1
                        ? info.pivot.value.name_ru
                        : currentLang == 2
                        ? info.pivot.value.name_uz
                        : info.pivot.value.name_kr
                    )}
                  </React.Fragment>
                );
              })}
            {generateBlock(
              language[currentLang].addDescr + ":",
              description,
              setDescription
            )}
            {generateBlock(
              language[currentLang].price + ":",
              price,
              setPrice,
              1
            )}
            {childTypes.map((inner, index) => {
              if (inner.children.length >= 1) {
                return (
                  <Between key={`${inner.id} ${index}`}>
                    {generateChoice(
                      currentLang == 1
                        ? inner.name_ru
                        : currentLang == 2
                        ? inner.name_uz
                        : inner.name_kr,
                      findChosen(inner.id),
                      () => makeChoice(inner.children)
                    )}
                  </Between>
                );
              } else {
                return (
                  <Between key={`${inner.id} ${index}`}>
                    {generateBlock(
                      currentLang == 1
                        ? inner.name_ru
                        : currentLang == 2
                        ? inner.name_uz
                        : inner.name_kr + ":",
                      findText(inner.id),
                      (value, id) => setText(value, inner.id),
                      1
                    )}
                  </Between>
                );
              }
            })}
            {typesChildren.map((inner, index) => {
              if (inner.children.length >= 1 && checkForRelevance(inner)) {
                return (
                  <Between key={`${index} ${inner.id}`}>
                    {generateChoice(
                      currentLang == 1
                        ? inner.name_ru
                        : currentLang == 2
                        ? inner.name_uz
                        : inner.name_kr,
                      findChosen(inner.id),
                      () => makeChoice(inner.children)
                    )}
                  </Between>
                );
              }
            })}
          </Wrap>
          <Wrap>
            <Title>{language[currentLang].address}</Title>
            {generateChoice(
              language[currentLang].region + ":",
              !region ? "" : findRegion(region),
              () =>
                navigation.navigate("RegionChoice", {
                  headerTitle: language[currentLang].chooseList,
                  method: setRegion,
                })
            )}
            {generateBlock(
              language[currentLang].addressInfo + ":",
              address,
              setAddress
            )}
            {generateChoice(
              language[currentLang].location,
              !coordinates
                ? language[currentLang].pinOnMap
                : language[currentLang].setted,
              () =>
                navigation.navigate("PinMap", {
                  method: setCoordinates,
                  coordinates,
                })
            )}
          </Wrap>
          <Wrap>
            <Between>
              <Title>{language[currentLang].photo}</Title>
              <Light>
                {images.length +
                  (advert.image.length
                    ? advert.image[0].split("/").length > 5
                      ? advert.image.length
                      : 0
                    : 0)}
                /10
              </Light>
            </Between>
            <PhotoWrap>
              {(advert.image.length
                ? advert.image[0].split("/").length > 5
                : 0) &&
                advert.image.map((img, index) => {
                  return (
                    <TouchableScale
                      style={[
                        { marginBottom: 10 },
                        index % 3 === 1 ? { marginHorizontal: 10 } : {},
                      ]}
                      key={index}
                      activeScale={0.96}
                      onPress={() => deleteImage(index)}
                    >
                      <Photo>
                        <PhotoImage
                          resizeMode={"contain"}
                          source={{ uri: img }}
                        />
                        <PhotoRemove>
                          <Trash style={{ width: 30, height: 30 }} />
                        </PhotoRemove>
                      </Photo>
                    </TouchableScale>
                  );
                })}
            </PhotoWrap>
            <PhotoWrap>
              {images.map((image, index) => {
                return (
                  <TouchableScale
                    style={[
                      { marginBottom: 10 },
                      index % 3 === 1 ? { marginHorizontal: 5 } : {},
                    ]}
                    key={index}
                    activeScale={0.96}
                    onPress={() => removeImage(index)}
                  >
                    <Photo>
                      <PhotoImage
                        resizeMode={"contain"}
                        source={{ uri: image }}
                      />
                      <PhotoRemove>
                        <Trash style={{ width: 30, height: 30 }} />
                      </PhotoRemove>
                    </Photo>
                  </TouchableScale>
                );
              })}
            </PhotoWrap>

            <View>
              {images.length <
                10 +
                  (advert.image.length
                    ? advert.image[0].split("/").length > 5
                      ? advert.image.length
                      : 0
                    : 0) && (
                <TouchableScale onPress={() => pickImage()} activeScale={0.95}>
                  <AddPhoto>
                    <Plus />
                    <AddPhotoText>
                      {language[currentLang].addPhoto}
                    </AddPhotoText>
                  </AddPhoto>
                </TouchableScale>
              )}
            </View>
          </Wrap>
          <Wrap>
            <Title>{language[currentLang].contactInfo} </Title>
            {/* {generateBlock(language[currentLang].email + ":", mail, setMail)} */}
            {generateBlock(
              language[currentLang].phone + ":",
              phone,
              () => {
                return;
              },
              0,
              true
            )}
          </Wrap>
          {/* <Wrap>
        <Between>
          <TouchableScale style={{ flex: 1 }} activeScale={0.96}>
            <SaveButton>Опубликовать</SaveButton>
          </TouchableScale>
        </Between>
      </Wrap> */}
          <Wrap>
            <Row>
              {/* {generateButton(1, false, "Предосмотр")} */}
              {/* <View width={10} /> */}
              {generateButton(0, false, language[currentLang].publish, () =>
                updateAdvert()
              )}
            </Row>
          </Wrap>
          <View height={15} />
        </ScrollView>
      </KeyboardAvoidingView>
    </>
  );
};
const PhotoRemove = styled.View`
  align-items: center;
  justify-content: center;
  background: blue;
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background: rgba(255, 120, 0, 0.5);
  border-radius: 10px;
  padding: 10px;
`;
const Row = styled.View`
  flex-direction: row;
  align-items: center;
`;
const Flexer = styled.View`
  flex: 1;
`;
const ButtonText = styled.Text`
  font-size: 14px;
  color: white;
  font-family: ${global.fonts.regular};
`;
const BottomButton = styled.View`
  width: 100%;
  background: ${({ type }) => (type ? "#999999" : global.colors.orange)};
  border-radius: 10px;
  height: 40px;
  flex-direction: row;
  align-items: center;
  justify-content: center;
`;
const Photo = styled.View`
  width: ${global.strings.width / 3.4 + "px"};
  align-items: center;
  justify-content: center;
  position: relative;
  margin-right: ${({ margin }) => (margin ? "10px" : "0")};
  margin-left: ${({ margin }) => (margin ? "10px" : "0")};
`;
const PhotoImage = styled.Image`
  width: 100%;
  height: 84px;
  border-radius: 10px;
`;
const AddPhotoText = styled.Text`
  color: #333333;
  font-size: 14px;
  font-family: ${global.fonts.regular};
  padding-left: 15px;
`;
const AddPhoto = styled.View`
  flex-direction: row;
  align-items: center;
  background: white;
  justify-content: center;
  border-radius: 10px;
  width: 100%;
  shadow-color: #000;
  shadow-opacity: 0.12;
  elevation: 3;
  height: 40px;
`;
const PhotoWrap = styled.View`
  flex-direction: row;
  width: 100%;
  align-items: center;
  /* justify-content: space-between; */
  padding-bottom: 10px;
  flex-wrap: wrap;
`;
const MapImage = styled.Image`
  width: 100%;
  margin-top: 10px;
  height: 170px;
  border-radius: 10px;
`;
const SaveButton = styled.Text`
  background: ${global.colors.orange};
  text-align: center;
  line-height: 40px;
  color: white;
  height: 40px;
  border-radius: 10px;
  margin-bottom: 5px;
  margin-top: 10px;
`;
const Title = styled.Text`
  color: #333333;
  font-size: 14px;
  font-family: ${global.fonts.medium};
  line-height: 22px;
  padding-bottom: 10px;
`;
const Light = styled.Text`
  color: #999999;
  font-size: 14px;
  font-family: ${global.fonts.regular};
  line-height: 22px;
  padding-bottom: 10px;
`;
const Icon = styled.View`
  width: 20px;
  height: 20px;
  align-items: center;
  justify-content: center;
`;
const InputChoice = styled.View`
  background: white;
  shadow-color: #000;
  shadow-opacity: 0.12;
  elevation: 3;
  width: 100%;
  height: 40px;
  border-radius: 10px;
  font-family: ${global.fonts.regular};
  padding: 10px 10px;
`;
const InputChoiceText = styled.Text`
  flex: 1;
  font-family: ${global.fonts.regular};
  font-size: 14px;
  color: ${({ active }) => (active ? "#333" : "#999999")};
`;
const InputBlock = styled.TextInput`
  margin-top: 5px;
  background: white;
  shadow-color: #000;
  shadow-opacity: 0.12;
  elevation: 3;
  width: 100%;
  height: 40px;
  border-radius: 10px;
  font-family: ${global.fonts.regular};
  padding: 10px 15px;
  min-height: ${({ multi }) => (multi ? multi + "px" : 0)};
  text-align-vertical: top;
  font-size: 14px;
`;
const InputTitle = styled.Text`
  color: #999999;
  font-family: ${global.fonts.regular};
  font-size: 14px;
`;
const InputGroup = styled.View`
  width: 100%;
  margin-bottom: 10px;
`;
const TypeInfoText = styled.Text`
  font-family: ${global.fonts.regular};
  font-size: 14px;
  color: ${({ type }) => type};
`;
const TypeInfo = styled.View`
  height: 40px;
  align-items: center;
  border-radius: 10px;
  /* box-shadow: 0px 0px 2px rgba(0, 0, 0, 0.12); */
  shadow-color: #000;
  shadow-opacity: 0.12;
  /* shadow-radius: 2; */
  elevation: 3;
  justify-content: center;
  background: ${({ type }) => type};
  margin-bottom: 10px;
`;
const Type = styled.View`
  flex: 1;
  padding-left: 10px;
`;
const LogoText = styled.Text`
  color: #ffffff;
  font-size: 16px;
  font-family: ${global.fonts.medium};
`;
const Logo = styled.View`
  align-items: center;
  justify-content: center;
  background: #999999;
  height: 90px;
  width: 90px;
  border-radius: 10px;
`;
const CoverImage = styled.Image`
  width: 100%;
  height: 170px;
  border-radius: 10px;
`;
const Wrap = styled.View`
  margin: 5px 0 5px;
  background: white;
  padding: 10px;
  /* flex: 1; */
  box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.12);
  /* align-items: center; */
`;
// const Title = styled.Text`
//   font-family: ${global.fonts.medium};
//   font-size: 24px;
//   line-height: 36px;
//   color: white;
//   padding-bottom: 20px;
// `;
const InputWrap = styled.View`
  background: white;
  border-radius: 10px;
  height: 40px;
  width: 100%;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
`;
const InputLeft = styled.View`
  height: 40px;
  width: 55px;
  border-right-width: 1px;
  border-right-color: #efefef;
  align-items: center;
  justify-content: center;
  /* border: solid #efefef; */
`;
const InfoText = styled.Text`
  color: #aeaeae;
  font-size: 14px;
  font-family: ${global.fonts.regular};
  text-align: center;
  margin: 20px 0;
`;
const OrangeText = styled.Text`
  color: ${global.colors.orange};
`;
const LoginInput = styled.TextInput`
  flex: 1;
  height: 40px;
  color: #333333;
  font-size: 14px;
  font-family: ${global.fonts.regular};
  padding: 0 10px;
`;
const PaddingHorizontal = styled.View`
  padding: 0 5px;
`;
const Information = styled.Text`
  font-size: 14px;
  color: ${({ color }) => (color ? "#333" : "#999")};
  font-family: ${global.fonts.regular};
`;
const Between = styled.View`
  flex-direction: row;
  align-items: center;
  width: 100%;
  justify-content: space-between;
  margin-top: ${({ margin }) => (margin ? margin + "px" : 0)};
`;

export default EditAdvertScreen;
