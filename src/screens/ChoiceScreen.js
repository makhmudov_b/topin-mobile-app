import React, { useState, useEffect, useContext } from "react";
import { View, ScrollView, Text, StatusBar } from "react-native";
import styled from "styled-components";
import global from "../../resources/global";
import Arrow from "../../assets/arrow-right.svg";
import TouchableScale from "react-native-touchable-scale";
import Context from "../components/Context";
import language from "../constants/language";
const ChoiceScreen = ({ navigation, route }) => {
  const { choices, method } = route.params;
  const { currentLang } = useContext(Context);
  const makeChoice = (id, type) => {
    method(id, type);
    navigation.goBack();
  };
  const generateButton = (name, method) => {
    return (
      <TouchableScale
        style={{ width: "100%", marginBottom: 15 }}
        activeScale={0.96}
        onPress={() => method()}
      >
        <AdvertButton>
          <Row>
            <Information color={1}>{name}</Information>
          </Row>
          <Row>
            <Icon left={5} right={5}>
              <Arrow width={20} height={20} />
            </Icon>
          </Row>
        </AdvertButton>
      </TouchableScale>
    );
  };
  return (
    <>
      <StatusBar barStyle={`dark-content`} backgroundColor={`white`} />
      <ScrollView
        contentContainerStyle={{
          backgroundColor: "#f5f5f5",
        }}
      >
        <Wrap>
          {choices.map((choice, key) => (
            <React.Fragment key={key}>
              {generateButton(
                currentLang == 1
                  ? choice.name_ru
                  : currentLang == 2
                  ? choice.name_uz
                  : choice.name_kr,
                (id) => makeChoice(choice.id, choice)
              )}
            </React.Fragment>
          ))}
        </Wrap>
      </ScrollView>
    </>
  );
};
const Row = styled.View`
  flex-direction: row;
  align-items: center;
`;
const AdvertButton = styled.View`
  flex-direction: row;
  justify-content: space-between;
  padding: 10px 15px;
  height: 40px;
  shadow-color: #000;
  shadow-opacity: 0.2;
  elevation: 3;
  shadow-radius: 4px;
  background: white;
  width: 100%;
  border-radius: 10px;
`;
const Information = styled.Text`
  font-size: 14px;
  color: ${({ color }) => (color ? "#333" : "#999")};
  font-family: ${global.fonts.regular};
`;
const Icon = styled.View`
  margin-left: ${({ left }) => left + "px"};
  margin-right: ${({ right }) => right + "px"};
  align-items: center;
  justify-content: center;
`;
const Wrap = styled.View`
  margin: 5px 0 5px;
  background: white;
  padding: 10px 10px 0;
  flex: 1;
  shadow-color: #000;
  shadow-opacity: 0.12;
  elevation: 3;
  align-items: center;
`;
const PaddingHorizontal = styled.View`
  padding: 0 5px;
`;
const Between = styled.View`
  flex-direction: row;
  align-items: center;
  width: 100%;
  justify-content: space-between;
  margin-top: ${({ margin }) => (margin ? margin + "px" : 0)};
`;
export default ChoiceScreen;
