import React, { useState, useEffect, useContext } from "react";
import { View, ScrollView, Text, Image, TouchableOpacity } from "react-native";
import styled from "styled-components";
import global from "../../resources/global";
import TouchableScale from "react-native-touchable-scale";
import Context from "../components/Context";

const CategoryScreen = ({ navigation }) => {
  const { apiService, token, phone, categories, currentLang } = useContext(
    Context
  );
  return (
    <ScrollView
      style={{ backgroundColor: "#F0F0F0" }}
      contentContainerStyle={{
        flexDirection: "row",
        flexWrap: "wrap",
        alignItems: "center",
        justifyContent: "space-between",
        paddingHorizontal: 10,
        paddingTop: 15,
      }}
    >
      {categories.map((category, key) => {
        if (!category.parent_id) {
          return (
            <TouchableScale
              onPress={() =>
                navigation.navigate("CategoryChoice", {
                  headerTitle:
                    currentLang == 1
                      ? category.name_ru
                      : currentLang == 2
                      ? category.name_uz
                      : category.name_kr,
                  categoryId: category.id,
                })
              }
              key={key}
              activeScale={0.95}
            >
              <Category>
                <CategoryTop color={category.color}>
                  <CategoryImage
                    resizeMode={"cover"}
                    source={{ uri: category.image }}
                  />
                </CategoryTop>
                <CategoryTitle>
                  {currentLang == 1 && category.name_ru}
                  {currentLang == 2 && category.name_uz}
                  {currentLang == 3 && category.name_kr}
                </CategoryTitle>
              </Category>
            </TouchableScale>
          );
        }
      })}
    </ScrollView>
  );
};
const categorySize = global.strings.width / 2.2 + "px";

const Category = styled.View`
  margin-bottom: 10px;
  width: ${categorySize};
`;
const CategoryTop = styled.View`
  width: ${categorySize};
  height: ${categorySize};
  background: ${({ color }) => "#" + color};
  align-items: flex-end;
  border-radius: 10px;
`;
const CategoryImage = styled.Image`
  height: ${categorySize};
  width: 115px;
`;

const CategoryTitle = styled.Text`
  font-size: 14px;
  line-height: 22px;
  height: 45px;
  font-family: ${global.fonts.regular};
  margin-top: 5px;
  text-align: center;
  width: ${categorySize};
`;
export default CategoryScreen;
