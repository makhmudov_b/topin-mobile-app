import React, { useContext, useEffect, useState } from "react";
import {
  AsyncStorage,
  View,
  Text,
  ActivityIndicator,
  StatusBar,
  StyleSheet,
  Platform,
} from "react-native";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import { createMaterialBottomTabNavigator } from "@react-navigation/material-bottom-tabs";
import global from "../resources/global";
import HomeScreen from "./screens/HomeScreen";
import Header from "./components/Header";
import HeaderLeft from "./components/HeaderLeft";
import HeaderAdd from "./components/HeaderAdd";
import HeaderFavorite from "./components/HeaderFavorite";
import HeaderClose from "./components/HeaderClose";
import Home from "../assets/home.svg";
import Category from "../assets/category.svg";
import Search from "../assets/search.svg";
import Profile from "../assets/profile.svg";
import CategoryScreen from "./screens/CategoryScreen";
import SearchScreen from "./screens/SearchScreen";
import ProfileScreen from "./screens/ProfileScreen";
import RegisterScreen from "./screens/RegisterScreen";
import SendMessageScreen from "./screens/SendMessageScreen";
import ConfirmScreen from "./screens/ConfirmScreen";
import TypeScreen from "./screens/TypeScreen";
import MyAdvertScreen from "./screens/MyAdvertScreen";
import FavoriteScreen from "./screens/FavoriteScreen";
import AddAdvertScreen from "./screens/AddAdvertScreen";
import ChoiceScreen from "./screens/ChoiceScreen";
import MultiChoiceScreen from "./screens/MultiChoiceScreen";
import MapScreen from "./screens/MapScreen";
import ShowAdvertScreen from "./screens/ShowAdvertScreen";
import UserScreen from "./screens/UserScreen";
import PromoteScreen from "./screens/PromoteScreen";
import BalanceScreen from "./screens/BalanceScreen";
import PaymentScreen from "./screens/PaymentScreen";
import TransactionScreen from "./screens/TransactionScreen";
import MessagesScreen from "./screens/MessagesScreen";
import MessageScreen from "./screens/MessageScreen";
import SettingScreen from "./screens/SettingScreen";
import Constants from "expo-constants";
import CategoryChoiceScreen from "./screens/CategoryChoiceScreen";
import RegionChoiceScreen from "./screens/RegionChoiceScreen";
import FilterScreen from "./screens/FilterScreen";
import Context from "./components/Context";
import language from "./constants/language";
import WebScreen from "./screens/WebScreen";
import EditUserScreen from "./screens/EditUserScreen";
import PinMapScreen from "./screens/PinMapScreen";
import EditAdvertScreen from "./screens/EditAdvertScreen";

const HeaderHeight =
  Platform.OS === "ios" ? 60 + Constants.statusBarHeight : 60;
const Stack = createStackNavigator();
const Tab = createMaterialBottomTabNavigator();
const ProfileAuth = createStackNavigator();
const AuthStack = () => {
  return (
    <ProfileAuth.Navigator headerMode={`none`}>
      <ProfileAuth.Screen name="Auth" component={SendMessageScreen} />
      <ProfileAuth.Screen name="Confirm" component={ConfirmScreen} />
      <ProfileAuth.Screen name="Type" component={TypeScreen} />
    </ProfileAuth.Navigator>
  );
};
const AppContainer = () => {
  // const [loading, setLoading] = useState(false);
  const { registered, token, currentLang } = useContext(Context);
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Home">
        <Stack.Screen
          name="Home"
          options={{
            headerTitle: () => <Header />,
            headerStyle: {
              backgroundColor: global.colors.main,
              height: HeaderHeight,
            },
            headerLeft: null,
            headerRight: null,
          }}
        >
          {(navigation) => {
            return (
              <Tab.Navigator
                barStyle={{
                  backgroundColor: global.colors.main,
                  // height: HeaderHeight,
                }}
                initialRouteName={`Main`}
                shifting={false}
                activeColor={global.colors.background}
                inactiveColor={`#ACACAC`}
                backBehavior={`none`}
              >
                <Tab.Screen
                  options={{
                    tabBarLabel: (
                      <Text style={styles.label}>
                        {language[currentLang].mainPage}
                      </Text>
                    ),
                    tabBarIcon: ({ color }) => (
                      <Home width={24} height={24} color={color} />
                    ),
                  }}
                  name="Main"
                  component={HomeScreen}
                />
                <Tab.Screen
                  options={{
                    tabBarLabel: (
                      <Text style={styles.label}>
                        {language[currentLang].category}
                      </Text>
                    ),
                    tabBarIcon: ({ color }) => (
                      <Category width={24} height={24} color={color} />
                    ),
                  }}
                  name="Category"
                  component={CategoryScreen}
                />
                <Tab.Screen
                  options={{
                    tabBarLabel: (
                      <Text style={styles.label}>
                        {language[currentLang].search}
                      </Text>
                    ),
                    tabBarIcon: ({ color }) => (
                      <Search width={24} height={24} color={color} />
                    ),
                  }}
                  name="Search"
                  component={SearchScreen}
                />
                <Tab.Screen
                  options={{
                    tabBarLabel: (
                      <Text style={styles.label}>
                        {language[currentLang].profile}
                      </Text>
                    ),
                    tabBarIcon: ({ color }) => (
                      <Profile width={24} height={24} color={color} />
                    ),
                  }}
                  name="Profile"
                  component={registered && token ? ProfileScreen : AuthStack}
                />
              </Tab.Navigator>
            );
          }}
        </Stack.Screen>
        <Stack.Screen
          name="Register"
          options={{
            headerTitle: language[currentLang].register,
            headerTitleStyle: styles.headerTitle,
            headerLeft: () => <HeaderLeft />,
            headerTitleAlign: "center",
            headerStyle: styles.headerCloseTitle,
          }}
          component={RegisterScreen}
        />
        <Stack.Screen
          name="EditUser"
          options={{
            headerTitle: language[currentLang].edit,
            headerTitleStyle: styles.headerTitle,
            headerLeft: () => <HeaderLeft />,
            headerTitleAlign: "center",
            headerStyle: styles.headerCloseTitle,
          }}
          component={EditUserScreen}
        />
        <Stack.Screen
          name="MyAdvert"
          options={{
            headerTitle: language[currentLang].adverts,
            headerTitleStyle: styles.headerTitle,
            headerLeft: () => <HeaderLeft />,
            headerRight: () => <HeaderAdd />,
            headerTitleAlign: "center",
            headerStyle: styles.headerCloseTitle,
          }}
          component={MyAdvertScreen}
        />
        <Stack.Screen
          name="Favorite"
          options={{
            headerTitle: language[currentLang].favorites,
            headerTitleStyle: styles.headerTitle,
            headerLeft: () => <HeaderLeft />,
            headerTitleAlign: "center",
            headerStyle: styles.headerCloseTitle,
          }}
          component={FavoriteScreen}
        />
        <Stack.Screen
          name="AddAdvert"
          options={{
            headerTitle: language[currentLang].adding,
            headerTitleStyle: styles.headerTitle,
            headerLeft: () => <HeaderLeft />,
            headerTitleAlign: "center",
            headerStyle: styles.headerCloseTitle,
          }}
          component={AddAdvertScreen}
        />
        <Stack.Screen
          name="EditAdvert"
          options={{
            headerTitle: language[currentLang].editing,
            headerTitleStyle: styles.headerTitle,
            headerLeft: () => <HeaderLeft />,
            headerTitleAlign: "center",
            headerStyle: styles.headerCloseTitle,
          }}
          component={EditAdvertScreen}
        />
        <Stack.Screen
          name="Choice"
          options={({ route }) => ({
            title: route.params.headerTitle,
            headerTitleStyle: styles.headerTitle,
            headerRight: () => <HeaderClose />,
            headerTitleAlign: "left",
            headerLeft: null,
            headerStyle: styles.headerCloseTitle,
          })}
          component={ChoiceScreen}
        />
        <Stack.Screen
          name="CategoryChoice"
          options={({ route }) => ({
            title: route.params.headerTitle,
            headerTitleStyle: styles.headerTitle,
            headerRight: () => <HeaderClose />,
            headerTitleAlign: "left",
            headerLeft: null,
            headerStyle: styles.headerCloseTitle,
          })}
          component={CategoryChoiceScreen}
        />
        <Stack.Screen
          name="RegionChoice"
          options={({ route }) => ({
            title: route.params.headerTitle,
            headerTitleStyle: styles.headerTitle,
            headerRight: () => <HeaderClose />,
            headerTitleAlign: "left",
            headerLeft: null,
            headerStyle: styles.headerCloseTitle,
          })}
          component={RegionChoiceScreen}
        />
        <Stack.Screen
          name="Filter"
          options={{
            title: language[currentLang].filter,
            headerTitleStyle: styles.headerTitle,
            headerRight: () => <HeaderClose />,
            headerTitleAlign: "left",
            headerLeft: null,
            headerStyle: styles.headerCloseTitle,
          }}
          component={FilterScreen}
        />
        <Stack.Screen
          name="MultiChoice"
          options={({ route }) => ({
            title: route.params.headerTitle,
            headerTitleStyle: styles.headerTitle,
            headerTitleStyle: styles.headerTitle,
            headerRight: () => <HeaderClose />,
            headerTitleAlign: "left",
            headerLeft: null,
            headerStyle: styles.headerCloseTitle,
          })}
          component={MultiChoiceScreen}
        />

        <Stack.Screen
          name="Map"
          options={{
            headerShown: false,
          }}
          component={MapScreen}
        />
        <Stack.Screen
          name="PinMap"
          options={{
            headerShown: false,
          }}
          component={PinMapScreen}
        />
        <Stack.Screen
          name="ShowAdvert"
          options={{
            headerLeft: () => <HeaderLeft />,
            // headerRight: () => <HeaderFavorite />,
            headerTitle: null,
            headerStyle: styles.headerHeight,
          }}
          component={ShowAdvertScreen}
        />
        <Stack.Screen
          name="User"
          options={{
            headerLeft: () => <HeaderLeft />,
            headerTitle: language[currentLang].profile,
            headerTitleStyle: styles.headerTitle,
            headerTitleAlign: "center",
            headerStyle: styles.headerHeight,
          }}
          component={UserScreen}
        />
        <Stack.Screen
          name="Promote"
          options={{
            headerLeft: () => <HeaderLeft />,
            headerTitle: language[currentLang].promotion,
            headerTitleStyle: styles.headerTitle,
            headerTitleAlign: "center",
            headerStyle: styles.headerHeight,
          }}
          component={PromoteScreen}
        />
        <Stack.Screen
          name="Balance"
          options={{
            headerLeft: () => <HeaderLeft />,
            headerTitle: language[currentLang].addBalance,
            headerTitleStyle: styles.headerTitle,
            headerTitleAlign: "center",
            headerStyle: styles.headerHeight,
          }}
          component={BalanceScreen}
        />
        <Stack.Screen
          name="Payment"
          options={{
            headerLeft: () => <HeaderLeft />,
            headerTitle: language[currentLang].chooseTypePay,
            headerTitleStyle: styles.headerTitle,
            headerTitleAlign: "center",
            headerStyle: styles.headerHeight,
          }}
          component={PaymentScreen}
        />
        <Stack.Screen
          name="Transaction"
          options={{
            headerLeft: () => <HeaderLeft />,
            headerTitle: language[currentLang].operations,
            headerTitleStyle: styles.headerTitle,
            headerTitleAlign: "center",
            headerStyle: styles.headerHeight,
          }}
          component={TransactionScreen}
        />
        <Stack.Screen
          name="Messages"
          options={{
            headerLeft: () => <HeaderLeft />,
            headerTitle: language[currentLang].messages,
            headerTitleStyle: styles.headerTitle,
            headerTitleAlign: "center",
            headerStyle: styles.headerHeight,
          }}
          component={MessagesScreen}
        />
        <Stack.Screen
          name="MessageShow"
          options={{
            headerLeft: () => <HeaderLeft />,
            headerTitle: language[currentLang].messages,
            headerTitleStyle: styles.headerTitle,
            headerTitleAlign: "center",
            headerStyle: styles.headerHeight,
          }}
          component={MessageScreen}
        />
        <Stack.Screen
          name="Setting"
          options={{
            headerLeft: () => <HeaderLeft />,
            headerTitle: language[currentLang].settings,
            headerTitleStyle: styles.headerTitle,
            headerTitleAlign: "center",
            headerStyle: styles.headerHeight,
          }}
          component={SettingScreen}
        />
        <Stack.Screen
          name="Web"
          options={{
            headerLeft: () => <HeaderLeft />,
            headerTitle: language[currentLang].payment,
            headerTitleStyle: styles.headerTitle,
            headerTitleAlign: "center",
            headerStyle: styles.headerHeight,
          }}
          component={WebScreen}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
};
const styles = StyleSheet.create({
  label: {
    fontFamily: global.fonts.regular,
    lineHeight: 22,
  },
  headerTitle: {
    color: "#333",
    fontSize: 24,
    fontFamily: global.fonts.medium,
  },
  headerCloseTitle: {
    backgroundColor: "white",
    height: HeaderHeight,
  },
  headerHeight: {
    height: HeaderHeight,
  },
});
export default AppContainer;
