const fonts = {
  light: "Rubik-Light",
  regular: "Rubik-Regular",
  medium: "Rubik-Medium",
  bold: "Rubik-Bold"
};

export default fonts;
