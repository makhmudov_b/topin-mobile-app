const colors = {
  main: "#5A5A5A",
  secondary: "#333333",
  orange: "#FF7800",
  background: "#FFFFFF",
};

export default colors;
