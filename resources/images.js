const images = {
  home: require("../assets/cover.png"),
  category: require("../assets/categ.png"),
  map: require("../assets/map.png"),
  uzsPay: require("../assets/uzs-pay.png"),
  usdPay: require("../assets/usd-pay.png"),
  octo: require("../assets/octo.png"),
  loading: require("../assets/categoryLoader.gif"),
  blackLoading: require("../assets/animation.gif"),
  notFound: require("../assets/not-found.gif"),
  // technical: require("../assets/technical.png"),
  // product: require("../assets/product-item.png"),
  // repair: require("../assets/repair.png"),
  // comment: require("../assets/comment.png")
};

export default images;
